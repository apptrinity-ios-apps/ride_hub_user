//  AppDelegate.swift
//  RideHubUser-Swift
//  Created by nagaraj  kumar on 10/09/18.
//  Copyright © 2018 Indobytes. All rights reserved.
import UIKit
import CoreData
import UserNotifications
import XMPPFramework
import GoogleMaps
import GooglePlaces
import CoreLocation
import Stripe
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate,XMPPStreamDelegate,XMPPRosterDelegate,XMPPRoomDelegate,UNUserNotificationCenterDelegate {
    var urlService = URLservices.sharedInstance
    var themes = Themes()
    var window: UIWindow?
    var xmppStream: XMPPStream?
    var xmppRoster: XMPPRoster?
    var xmppRosterStorage: XMPPRosterCoreDataStorage?
    var xmppJabberIdStr = String()
    var isXMPPDisConnected = Bool()
    var xmppRoom : XMPPRoom?
    var oldLoactionNext = Double()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey("AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw")
        GMSPlacesClient.provideAPIKey("AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw")
        Stripe.setDefaultPublishableKey("pk_test_0I3IGKx3mcL97rMJ0Dbt5pm9") //  for staging
       // Stripe.setDefaultPublishableKey("pk_live_ZHpP0LcJm5kfJ4SpnLx0rnUY") // for live
        registerForPushNotifications(application: application)
        
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings(completionHandler: { (settings) in
            if settings.authorizationStatus == .notDetermined {
                // Notification permission has not been asked yet, go for it!
                print("Notification permission has not been asked yet, go for it!")
            }
            if settings.authorizationStatus == .denied {
                // Notification permission was previously denied, go to settings & privacy to re-enable
                print(" Notification permission was previously denied, go to settings & privacy to re-enable")
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                    (granted, error) in
                    // add your own
                    UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
                    let alertController = UIAlertController(title: "Notification Alert", message: "Please enable notifications", preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            })
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    alertController.addAction(settingsAction)
                    DispatchQueue.main.async {
                        self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                    }
                }
            }
            if settings.authorizationStatus == .authorized {
                // Notification permission was already granted
                print("Notification permission was already granted")
            }
        })
         let checkTutorial = UserDefaults.standard.object(forKey: "checkTutorial")
             if checkTutorial is NSNull || checkTutorial == nil {
                let rootViewController = self.window!.rootViewController as! UINavigationController
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "LaunchPageVC") as! LaunchPageVC
                rootViewController.pushViewController(profileViewController, animated: true)
             }else{
                 let checkLogIn = UserDefaults.standard.object(forKey: "checkLogIn")
                    if checkLogIn is NSNull || checkLogIn == nil {
                        DispatchQueue.main.async {
                            if CLLocationManager.locationServicesEnabled() {
                                switch(CLLocationManager.authorizationStatus()) {
                                case .notDetermined, .restricted, .denied:
                                    print("No access")
                                    let rootViewController = self.window!.rootViewController as! UINavigationController
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "LocationServicesVC") as! LocationServicesVC
                                    profileViewController.fromHint = 2
                                    rootViewController.pushViewController(profileViewController, animated: true)
                                case .authorizedAlways, .authorizedWhenInUse:

                                    //  UserDefaults.standard.set("2", forKey: "checkfreeorGratis")
                                    print("Locations enabled")
                                    let rootViewController = self.window!.rootViewController as! UINavigationController
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                    rootViewController.pushViewController(profileViewController, animated: true)
                                }
                            } else {
                                let rootViewController = self.window!.rootViewController as! UINavigationController
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "LocationServicesVC") as! LocationServicesVC
                                profileViewController.fromHint = 2; rootViewController.pushViewController(profileViewController, animated: true)
                            }
                        }
                    } else{

                        DispatchQueue.main.async {
                            if CLLocationManager.locationServicesEnabled() {
                                switch(CLLocationManager.authorizationStatus()) {
                                case .notDetermined, .restricted, .denied:
                                    print("No access")

                                    let rootViewController = self.window!.rootViewController as! UINavigationController
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "LocationServicesVC") as! LocationServicesVC
                                   profileViewController.fromHint = 1
                                    rootViewController.pushViewController(profileViewController, animated: true)
                                case .authorizedAlways, .authorizedWhenInUse:

                                    //                    UserDefaults.standard.set("2", forKey: "checkfreeorGratis")
                                    print("Locations enabled")
                                    let rootViewController = self.window!.rootViewController as! UINavigationController
                                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                                    rootViewController.pushViewController(profileViewController, animated: true)
                                }
                            } else {
                                let rootViewController = self.window!.rootViewController as! UINavigationController
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let profileViewController = mainStoryboard.instantiateViewController(withIdentifier: "LocationServicesVC") as! LocationServicesVC
                                profileViewController.fromHint = 1; rootViewController.pushViewController(profileViewController, animated: true)
                            }
                        }
                    }
            }
        connectedToInternet()
        UIApplication.shared.registerForRemoteNotifications()
        return true
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
//            if themes.getUserId() != nil {
//            if xmppStream?.isDisconnected() {
//                self.disconnect()
//            }
//            if themes.hasAppDetails() {
//                self.connect()
//            }
//        }
//
//        let id = themes.getUserId() as Any
//        if id != nil {
//            disconnect()
//            if themes.hasAppDetails() {
//                self.connect()
//            }
//        }

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != UIUserNotificationType() {
            registerForPushNotifications(application: application)
        }
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        print("device token",token)
        let themes = Themes()
        themes.savedDeviceToken(token: token)
        
        //deviceToken1 = token
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("register failed")
        }
    func registerForPushNotifications(application: UIApplication) {
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                if (granted)
                {
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })                 }
                else{
                    //Do stuff if unsuccessful...
                    print("Don't Allow")
                    
                    
                }
            })
        }else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 8 support
        else if #available(iOS 8, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
            // iOS 7 support
        else {
            application.registerForRemoteNotifications(matching: [.badge, .sound, .alert])
        }
        
    }
    func GetAppInfo() {
        let parameter = ["id": themes.getUserId(),
                         "user_type": "user"]
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            print("log in parameters is \(parameter)")
            urlService.serviceCallPostMethodWithParams(url:getAppInfo, params: parameter as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    
                }else{
                let success = response["status"] as! String
                if(success == "1"){
                    var dict = response["response"]!["info"]  as! [String:AnyObject]
                    var rideDict = response["response"] as! [String:AnyObject]
                    if dict.count > 0 {
                        let objAppInfoRecs = AppInfoRecord()
                        objAppInfoRecs.serviceContactEmail = dict["site_contact_mail"] as! String
                        objAppInfoRecs.serviceNumber = dict["customer_service_number"] as! String
                        objAppInfoRecs.serviceSiteUrl = dict["site_url"] as! String
                        objAppInfoRecs.serviceXmppHost = dict["xmpp_host_name"] as! String
                        objAppInfoRecs.serviceXmppPort = dict["xmpp_host_url"] as! String
                        objAppInfoRecs.serviceFacebookAppId = dict["facebook_id"] as! String
                        objAppInfoRecs.serviceGooglePlusId = dict["google_plus_app_id"] as! String
                       // objAppInfoRecs.servicePhoneMaskingStatus = dict["phone_masking_status"] as! String
                    
                      //  objAppInfoRecs.hasPendingRide = rideDict["ongoing_ride"] as! String
                       // objAppInfoRecs.pendingRideID = rideDict["ongoing_ride_id"] as! String
                      //  objAppInfoRecs.hasPendingRating = rideDict["rating_pending"] as! String
                       // objAppInfoRecs.pendingRateId = rideDict["rating_pending_ride_id"] as! String
                        
                        Themes.saveAppDetails(objAppInfoRecs)
                        
                        self.connectToXmpp()
                        
                        
                        NotificationCenter.default.post(name: NSNotification.Name("NotifForAppInfo"), object: self, userInfo: nil)
                    }
                    
                    
                    
                }
                else{
                    
                    let alert = UIAlertController(title: "Ride Hub", message: "", preferredStyle: .alert)
                    let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alert.addAction(cancelButton)
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    
                }
            }
                
            }
        }else{
            
            let alert = UIAlertController(title: "Ride Hub", message: "", preferredStyle: .alert)
            let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(cancelButton)
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            
        }
        
        
        
        
        
        
        
    }
    
    
    
    
    
    
    func connectedToInternet() {
        
        let networkReachbilty = urlService.connectedToNetwork()
        if (networkReachbilty){
            
       print("Internet connected ")
            if themes.getUserId() == nil {
                        xmppState()
                    if isXMPPDisConnected {
                            if themes.hasAppDetails() {
                                connect()
                            }
                        } else {
                            print("XMPP not disconnected")
                        }
                    } else {
                        print("User not logged in")
                    }
            
            }
        else{
            print("No Internet  ")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NoInternet"), object: nil)

          // NotificationCenter.default.post(name: Notification.Name("NoInternet"), object: self, userInfo: nil)
        }
        
    }
    
func xmppState() {
    if XMPPStream.self == nil {
        isXMPPDisConnected = true
    } else {
        isXMPPDisConnected = (xmppStream?.isDisconnected)!
        print(String(format: "%hhd", isXMPPDisConnected as CVarArg))
    }
    
    
}
    
 func connectToXmpp() {
    if themes.getUserId() != "" {
        DDLog.add(DDTTYLogger.sharedInstance)
            if themes.hasAppDetails() {
                self.connect()
            }
        }
    }
    
    
func connect() -> Bool {
    
        setupStream()
        let appInfoDict = themes.appAllInfoDatas()
    xmppJabberIdStr = themes.checkNullValue(appInfoDict?["xmpp_host_url"]) as! String
    
    xmppStream?.hostName = xmppJabberIdStr
   // xmppStream?.hostPort =
    
    
    if xmppJabberIdStr.count > 0 {
            
        } else {
            NotificationCenter.default.post(name: NSNotification.Name("xmppNotConectNotif"), object: self, userInfo: nil)
        }
    let jabberID = "\(themes.getUserId())@\(xmppJabberIdStr)" // @messaging.dectar.com
        let myPassword = themes.getXmppUserCredentials()
    
    if !(xmppStream?.isDisconnected)! {
        
            return true
        }
        if jabberID == "" || myPassword == nil {
            
            return false
        }
     xmppStream?.myJID = XMPPJID.init(string: jabberID, resource: nil)
    // new One
     xmpp_plugin()
   // xmppStream?.setMyJID = XMPPJID.JID(with: jabberID) // old one
    //password = myPassword;
        
    let error: Error? = nil
    if !(((try? xmppStream!.connect(withTimeout: XMPPStreamTimeoutNone)) != nil)) {
        
        let alert = UIAlertController(title: "Error",
                                      message: "Can't connect to server \(error?.localizedDescription ?? "")",
                                      preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK",
                               style: .cancel,
                               handler: nil)
        alert.addAction(ok)
        self.window?.rootViewController?.present(alert, animated: true , completion: nil)
        
        return false
        }
    
    
     return true
    
    }
    func logoutroot() {
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let appDel = UIApplication.shared.delegate as? AppDelegate
        let rootView = sb.instantiateViewController(withIdentifier: "LoginVC") as? UINavigationController
        appDel?.window?.rootViewController = rootView
        
       
    }
    func goOffline() {
        let presence = XMPPPresence(type: "unavailable")
        xmppStream?.send(presence)
    }
    func goOnline() {
        let presence = XMPPPresence()
        xmppStream?.send(presence)
    }
    func disconnect() {
        isXMPPDisConnected = true
        xmpp_Unplug()
        goOffline()
        xmppStream?.disconnect()
    }
    func xmpp_Unplug() {
        let parameter = [
            "user_type": "user",
            "id": themes.getUserId(),
            "mode": "unavailable"
        ]
       xmppCheck(parameters: parameter)
    }
    func xmpp_plugin() {
        let parameter = [
            "user_type": "user",
            "id": themes.getUserId(),
            "mode": "available"
        ]
        xmppCheck(parameters: parameter)
    }
    func xmppCheck(parameters:[String:Any]){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:forXMPP, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    
                }else{
                let success = response["status"] as! String
                if(success == "1"){
                    self.xmppStream?.addDelegate(self, delegateQueue: DispatchQueue.main)
                    self.xmppRoster?.addDelegate(self, delegateQueue: DispatchQueue.main)
                    self.xmppRoom?.addDelegate(self, delegateQueue: DispatchQueue.main)
                }
                else{
                    let alert = UIAlertController(title: "Ride Hub", message: "", preferredStyle: .alert)
                    let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alert.addAction(cancelButton)
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                }
            }
                
            }
        }else{
            let alert = UIAlertController(title: "Ride Hub", message: "", preferredStyle: .alert)
            let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(cancelButton)
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
    func showAlertForSessionLogout(message:String){
        let alert = UIAlertController(title: "Ride Hub", message: message, preferredStyle: .alert)
        let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(cancelButton)
       self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
 func setupStream() {
        xmppStream = XMPPStream()
        xmppStream?.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppRoster?.addDelegate(self, delegateQueue: DispatchQueue.main)
    let appInfoDict = themes.appAllInfoDatas()
    let xmppHostStr = themes.checkNullValue(appInfoDict?["xmpp_host_name"]) as! String
    if xmppHostStr.count > 0 {
        xmppStream?.hostName = xmppHostStr
       
    } else {
        NotificationCenter.default.post(name: NSNotification.Name("xmppNotConectNotif"), object: self, userInfo: nil)
    }
    
    xmpp_plugin()
    
    let xmpp = XMPPReconnect()
    xmpp.activate(xmppStream!)
    xmpp.addDelegate(self, delegateQueue: DispatchQueue.main)
    xmppStream?.enableBackgroundingOnSocket = true
   
  
    //[xmppStream setHostPort:8070];
    
    
    
        
    }
    
    
    // MARK: - Xmpp Delegates
    
   @objc func xmppStreamDidConnect(_ sender: XMPPStream?) {
        
        isXMPPDisConnected = false
        let error: Error? = nil
    try? xmppStream?.authenticate(withPassword:themes.getXmppUserCredentials()!)
        if error == nil {
            xmpp_plugin()
        } else {
            isXMPPDisConnected = true
        }
        xmpp_plugin()
    }
 @objc func xmppStreamConnectDidTimeout(_ sender: XMPPStream?) {
        let alert = UIAlertController(title: "Time out", message: "Connection timeout please try again", preferredStyle: .alert)
        let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(cancelButton)
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
 @objc   func xmppStreamDidAuthenticate(_ sender: XMPPStream?) {
     goOnline()
    }
    @objc  func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
        let testXMLString = "\(message)"
//    let testXMLString = "\(message)"
////        // Parse the XML into a dictionary
      //  let parseError: Error? = nil
    _ = XMLParser()
    
        var xmlDictionary = try? XMLReader.dictionary(forXMLString: testXMLString)
    print("\(String(describing: xmlDictionary))")
    let message = xmlDictionary!["message"] as! NSDictionary
    let body = message["body"] as! NSDictionary
    let text = body["text"] as? String
   // var TextContent = xmlDictionary!["message"]?["body"]?["text"] as? String
    let xmppRecMsg = text?.replacingOccurrences(of: "+", with: " ").removingPercentEncoding
    let data: Data? = xmppRecMsg?.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
    var recMsgDict: Dictionary<String, Any>?
        if let aData = data {
            recMsgDict = try? JSONSerialization.jsonObject(with: aData, options: []) as! Dictionary<String, Any>
            let action = recMsgDict!["action"] as! String
            if(action == "ride_confirmed"){
                if(!cabArrivedHint){
                    NotificationCenter.default.post(name: NSNotification.Name("pushnotification"), object: recMsgDict)
                    let driverId = recMsgDict?["key1"] as! String
                    let driverName = recMsgDict?["key2"] as! String
                    let driverImage = recMsgDict?["key4"] as! String
                    themes.setDriverId(id: driverId)
                    themes.setDriverName(name: driverName)
                    themes.setDriverImage(image:driverImage)
                    cabArrivedHint = true
                }else{
                }
            }else if(action == "cab_arrived"){
                let imageDict = NSMutableDictionary()
                imageDict.setObject(themes.checkNullValue(recMsgDict?["key1"]) as! String, forKey: "rideID" as NSCopying)
                imageDict.setObject(themes.checkNullValue(recMsgDict?["message"]) as! String, forKey: "message" as NSCopying)
                imageDict.setObject(themes.checkNullValue(recMsgDict?["key3"]) as! String, forKey: "key3" as NSCopying)
                imageDict.setObject(themes.checkNullValue(recMsgDict?["key4"]) as! String, forKey: "key4" as NSCopying)
                NotificationCenter.default.post(name: NSNotification.Name("cab_arrived"), object: imageDict)
            }else if(action == "driver_loc"){
                
                let rideDict = NSMutableDictionary()
                rideDict.setObject(themes.checkNullValue(recMsgDict?["ride_id"]) as! String, forKey: "rideID" as NSCopying)
                rideDict.setObject(themes.checkNullValue(recMsgDict?["latitude"]) as! String, forKey: "driverLatitude" as NSCopying)
                rideDict.setObject(themes.checkNullValue(recMsgDict?["longitude"]) as! String, forKey: "driverLongitude" as NSCopying)
                
                NotificationCenter.default.post(name: NSNotification.Name("Updatedriver_loc"), object: rideDict)
                
            }else if(action == "ride_completed"){
                
                    let rideDict = NSMutableDictionary()
                    rideDict.setObject(self.themes.checkNullValue(recMsgDict?["key1"]) as! String, forKey: "rideID" as NSCopying)
                    rideDict.setObject(self.themes.checkNullValue(recMsgDict?["message"]) as! String, forKey: "message" as NSCopying)
                    //  NotificationCenter.default.post(name: NSNotification.Name("ride_completed"), object: rideDict)
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ride_completed"),object: rideDict))
                
            }else if(action == "payment_paid"){
             if(!ratingHint){
                    let rideDict = NSMutableDictionary()
                    rideDict.setObject(self.themes.checkNullValue(recMsgDict?["key1"]) as! String, forKey: "rideID" as NSCopying)
                    NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "payment_paid"),object: rideDict))
               
            //   NotificationCenter.default.post(name: NSNotification.Name("payment_paid"), object: rideDict)
               
                    ratingHint = true
            }else{

                    
                }
            }else if(action == "requesting_payment"){
                if(!requestPaymentHint){
                    let rideDict = NSMutableDictionary()
                   // rideDict.setObject(themes.checkNullValue(recMsgDict) as! String, forKey: "rideDetails" as NSCopying)
                    NotificationCenter.default.post(name: NSNotification.Name("waitingfor_payment"), object: recMsgDict)
                    requestPaymentHint = true
                }else{
                    
                    
                    
                    
                }
                
                
                
            }else if(action == "ride_cancelled"){
                
                
                
                
                
                
                
                
              
            }else if(action == "ads"){
                
            }else if(action == "trip_begin"){
                
                let rideDict = NSMutableDictionary()
                rideDict.setObject(themes.checkNullValue(recMsgDict?["key1"]) as! String, forKey: "rideID" as NSCopying)
                rideDict.setObject(themes.checkNullValue(recMsgDict?["message"]) as! String, forKey: "message" as NSCopying)
                rideDict.setObject(themes.checkNullValue(recMsgDict?["key3"]) as! String, forKey: "dropLatitude" as NSCopying)
                rideDict.setObject(themes.checkNullValue(recMsgDict?["key4"]) as! String, forKey: "dropLongitude" as NSCopying)
                 rideDict.setObject(themes.checkNullValue(recMsgDict?["key5"]) as! String, forKey: "driverLatitude" as NSCopying)
                 rideDict.setObject(themes.checkNullValue(recMsgDict?["key6"]) as! String, forKey: "driverLongitude" as NSCopying)
                rideDict.setObject(themes.checkNullValue(recMsgDict?["key7"]) as! String, forKey: "estimatedDropTime" as NSCopying)
                
                
                  rideDict.setObject(themes.checkNullValue(recMsgDict?["key9"]) as! String, forKey: "pickUpTime" as NSCopying)
                
                
                NotificationCenter.default.post(name: NSNotification.Name("Ride_start"), object: rideDict)
             
            }else if(action == "make_payment"){
                
                let rideDict = NSMutableDictionary()
                rideDict.setObject(self.themes.checkNullValue(recMsgDict?["key1"]) as! String, forKey: "rideID" as NSCopying)
                rideDict.setObject(self.themes.checkNullValue(recMsgDict?["message"]) as! String, forKey: "message" as NSCopying)
                //  NotificationCenter.default.post(name: NSNotification.Name("ride_completed"), object: rideDict)
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "ride_completed"),object: rideDict))
                
            }else if(action == "driver_loc"){
                
            }else {
                
            }
        }
    }
    
@objc    func xmppReconnect(_ sender: XMPPReconnect?, shouldAttemptAutoReconnect reachabilityFlags: SCNetworkReachabilityFlags) -> Bool {
        print("shouldAttemptAutoReconnect:\(reachabilityFlags)")
        return true
    }
    
@objc    func xmppReconnect(_ sender: XMPPReconnect?, didDetectAccidentalDisconnect connectionFlags: SCNetworkReachabilityFlags) {
        print("didDetectAccidentalDisconnect:\(connectionFlags)")
    }
    
    
 @objc   func xmppStream(_ sender: XMPPStream?, didReceiveError element: XMLElement?) {
        
        let elementName = element?.name
        let myJid = sender?.myJID as? Any
        let resultString = ("\(myJid ?? "")".components(separatedBy: "@"))[0]
        let userName = themes.getUserId().lowercased()
        if (elementName == "stream:error") || (elementName == "error") {
            let r_conflict: XMLElement? = element?.forName("conflict", xmlns: "urn:ietf:params:xml:ns:xmpp-streams")
            if r_conflict != nil {
                disconnect()
                if (resultString == userName) {
                    
                    let alert = UIAlertController(title: "Conflict", message: "Same User has been logged in into other device", preferredStyle: .alert)
                    let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                    alert.addAction(cancelButton)
                    self.window?.rootViewController?.present(alert, animated: true, completion: nil)
                } else {
                }
            }
        }
    }
    
 @objc   func xmppStream(_ sender: XMPPStream?, didReceive iq: XMPPIQ?) -> Bool {
        
        return false
        
    }
 @objc   func xmppStream(_ sender: XMPPStream?, didReceive presence: XMPPPresence?) {
        
    let jidUserTypeStr = presence?.type
    let jidUserStr = presence?.from?.user
        if (jidUserTypeStr == "available") {
            isXMPPDisConnected = false
        } else {
            isXMPPDisConnected = true
            disconnect()
        }
        
        print("presnece......User->\(jidUserStr ?? "")..UserPresence->\(jidUserTypeStr ?? "")")
        
        
        
    }
 @objc   func xmppRoster(_ sender: XMPPStream?, didReceiveRosterItem item: DDXMLElement?) {
        
        print("Did receive Roster item")
        
        
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "RideHubUser_Swift")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

