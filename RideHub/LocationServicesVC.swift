//
//  LocationServicesVC.swift
//  RideHubUser-Swift
//
//  Created by INDOBYTES on 01/02/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit
import CoreLocation
class LocationServicesVC: UIViewController,CLLocationManagerDelegate {
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var enableGPSView: UIView!
    var locManager = CLLocationManager()
    var fromHint = Int()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
         imageview.image = UIImage.gifImageWithName("Enable-GPS_Animation")
        enableGPSView.layer.cornerRadius = enableGPSView.frame.height / 2
        
        locManager.delegate = self
        
        locManager.startUpdatingHeading()
        
        locManager.requestAlwaysAuthorization()
        
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.startMonitoringSignificantLocationChanges()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         locManager.requestAlwaysAuthorization()
    }
    
    @IBAction func enableGPSAction(_ sender: Any) {
        
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                print("No access")
                locManager.requestAlwaysAuthorization()
//                let alertController = UIAlertController(title: "Your Location Services are Disabled",
//                                                        message: "Turn on your Location from the settings to help us locate you.",
//                                                        preferredStyle: .alert)
//                let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
//                    if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
//                        if #available(iOS 10.0, *) {
//                            UIApplication.shared.open(appSettings as URL)
//                        } else {
//                            // Fallback on earlier versions
//                        }
//                    }
//                }
//                alertController.addAction(settingsAction)
//
//                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
//                alertController.addAction(cancelAction)
//
//                present(alertController, animated: true, completion: nil)
                
            case .authorizedAlways, .authorizedWhenInUse:
                
                //                    UserDefaults.standard.set("2", forKey: "checkfreeorGratis")
                print("Locations enabled")
                if(fromHint == 1){
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        } else {
            let alertController = UIAlertController(title: "Your Location Services are Disabled",
                                                    message: "Turn on your Location from the settings to help us locate you",
                                                    preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default){ (alertAction) in
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(appSettings as URL)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
            alertController.addAction(settingsAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
