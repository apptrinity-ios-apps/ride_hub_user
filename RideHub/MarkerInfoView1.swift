//
//  MarkerInfoView1.swift
//  RideHub
//
//  Created by INDOBYTES on 04/12/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class MarkerInfoView1: UIView {
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dropMarkerBoderImageview: UIImageView!
    
    @IBOutlet weak var betweenLine: UIView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var pointimageview: UIImageView!
    
    
    @IBOutlet weak var locationBtn: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
