//
//  EditAccountTableViewCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 24/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class EditAccountTableViewCell: UITableViewCell {
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var profImgView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var phoneNumberView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var firstNameBtn: UIButton!
    @IBOutlet weak var lastNameBtn: UIButton!
    @IBOutlet weak var phoneNumberBtn: UIButton!
    @IBOutlet weak var emailBtn: UIButton!
    @IBOutlet weak var passwordBtn: UIButton!
    
    @IBOutlet weak var firstNameLbl: UILabel!
    @IBOutlet weak var lastNameLbl: UILabel!
    @IBOutlet weak var phoneNumberLbl: UILabel!
    @IBOutlet weak var emaiLbl: UILabel!
    @IBOutlet weak var passowrdField: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        profImgView.layer.cornerRadius = profImgView.frame.size.height/2        
        editBtn.layer.cornerRadius = editBtn.frame.size.height/2
        profileImage.layer.cornerRadius = profileImage.frame.size.height/2
        profileImage.layer.masksToBounds = true
        shadowView(view: profImgView)
        
        firstNameView.layer.cornerRadius = firstNameView.frame.size.height/2
        firstNameView.layer.borderWidth = 1
        self.firstNameView.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        lastNameView.layer.cornerRadius = lastNameView.frame.size.height/2
        lastNameView.layer.borderWidth = 1
        self.lastNameView.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        phoneNumberView.layer.cornerRadius = phoneNumberView.frame.size.height/2
        phoneNumberView.layer.borderWidth = 1
        self.phoneNumberView.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        emailView.layer.cornerRadius = emailView.frame.size.height/2
        emailView.layer.borderWidth = 1
        self.emailView.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        passwordView.layer.cornerRadius = passwordView.frame.size.height/2
        passwordView.layer.borderWidth = 1
        self.passwordView.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func shadowView(view:UIView){
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }
    
}
