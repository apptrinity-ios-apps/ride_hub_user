//
//  EditAccountVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 24/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit
import AssetsLibrary
import Photos
import Alamofire
class EditAccountVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    let themes = Themes()
    let urlservices = URLservices()
    var PopViewName = ""
    @IBOutlet weak var editAccountTableView: UITableView!
    @IBOutlet weak var dotLine: UIView!
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var saveBtn: UIButton!    
    @IBOutlet weak var popViewHeight: NSLayoutConstraint!
    @IBOutlet weak var hideView: UIView!
    @IBAction func poUpCloseAction(_ sender: Any) {
       popUpView.isHidden = true
        hideView.isHidden = true
    }
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        editAccountTableView.separatorColor = .clear
        editAccountTableView.delegate = self
        editAccountTableView.dataSource = self
        firstTextField.delegate = self
        secondTextField.delegate = self
        thirdTextField.delegate = self
        
        hideView.isHidden = true
        popUpView.isHidden = true
        popUpView.layer.cornerRadius = 15
        saveBtn.layer.cornerRadius = saveBtn.frame.size.height/2
        
        self.editAccountTableView.register(UINib(nibName: "EditAccountTableViewCell", bundle: nil), forCellReuseIdentifier: "EditAccountTableViewCell")
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(NoteToDriverVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(NoteToDriverVC.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        firstTextField.inputAccessoryView = toolBar
        secondTextField.inputAccessoryView = toolBar
        thirdTextField.inputAccessoryView = toolBar
        
        
        firstTextField.layer.cornerRadius = firstTextField.frame.size.height/2
        firstTextField.layer.borderWidth = 1
        self.firstTextField.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        // For Right side Padding
        firstTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: firstTextField.frame.height))
        firstTextField.leftViewMode = .always
        
        secondTextField.layer.cornerRadius = secondTextField.frame.size.height/2
        secondTextField.layer.borderWidth = 1
        self.secondTextField.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        // For Right side Padding
        secondTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: secondTextField.frame.height))
        secondTextField.leftViewMode = .always
        
        thirdTextField.layer.cornerRadius = thirdTextField.frame.size.height/2
        thirdTextField.layer.borderWidth = 1
        self.thirdTextField.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        // For Right side Padding
        thirdTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: thirdTextField.frame.height))
        thirdTextField.leftViewMode = .always

        // Do any additional setup after loading the view.
    }
    
    @objc func donePicker() {
        firstTextField.resignFirstResponder()
        secondTextField.resignFirstResponder()
        thirdTextField.resignFirstResponder()
    }
    
    @objc func cancelPicker() {
        firstTextField.text = ""
        firstTextField.resignFirstResponder()
        secondTextField.text = ""
        secondTextField.resignFirstResponder()
        thirdTextField.text = ""
        thirdTextField.resignFirstResponder()
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditAccountTableViewCell", for: indexPath) as! EditAccountTableViewCell
        cell.selectionStyle = .none
       let image1 = themes.checkNullValue(self.themes.getUserDp()) as! String
        if(image1 == ""){
            cell.profileImage.image = UIImage(named: "user_default")
        }else{
            let image = self.themes.getUserDp()
            let fullimageUrl = URL(string: image)
            cell.profileImage.load(url: fullimageUrl!)
        }

        cell.firstNameLbl.text = self.themes.getUserName()
        cell.phoneNumberLbl.text = self.themes.getmobileNumber()
        cell.emaiLbl.text = self.themes.getuserMail()
        cell.passowrdField.text = self.themes.getPassword()
        cell.editBtn.addTarget(self, action: #selector(editBtnClicked(button :)), for: .touchUpInside)
        cell.firstNameBtn.addTarget(self, action: #selector(firstNameBtnClicked(button :)), for: .touchUpInside)
        cell.lastNameBtn.addTarget(self, action: #selector(lastNameBtnClicked(button :)), for: .touchUpInside)
        cell.phoneNumberBtn.addTarget(self, action: #selector(phoneNumberBtnClicked(button :)), for: .touchUpInside)
        cell.emailBtn.addTarget(self, action: #selector(emailBtnClicked(button :)), for: .touchUpInside)
        cell.passwordBtn.addTarget(self, action: #selector(passwordBtnClicked(button :)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 461
    }
    
    @objc func editBtnClicked(button : UIButton) {

        let actionSheet = UIActionSheet(title: "Choose Option", delegate: self, cancelButtonTitle: "Cancel", destructiveButtonTitle: nil, otherButtonTitles: "Camera","Gallery")
        actionSheet.show(in: self.view)
        
    }
    
    func actionSheet(_ actionSheet: UIActionSheet, clickedButtonAt buttonIndex: Int)
    {
        switch (buttonIndex){
        case 1:
            cameraClicked()
        case 2:
            galleryClicked()
        default:
            print("Default")
        }
    }
    
    func cameraClicked(){
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized
        {
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerController.SourceType.camera
            image.allowsEditing = true
            self.present(image, animated: true)
        }
        else
        {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    let image = UIImagePickerController()
                    image.delegate = self
                    image.sourceType = UIImagePickerController.SourceType.camera
                    image.allowsEditing = false
                    
                    self.present(image, animated: true)
                }
                else
                {
                    print("User Rejected")
                    let alertController = UIAlertController(title: "This app does not have access to your camera.",
                                                            message: "Turn on your Camera from the settings to help Access your camera",
                                                            preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
                       if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(appSettings as URL)
                            } else {
                                // Fallback on earlier versions
                            }
                        }
                    }
                    alertController.addAction(settingsAction)
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            });
        }
    }
    
    func galleryClicked(){
        let status = PHPhotoLibrary.authorizationStatus()
        if (status == PHAuthorizationStatus.authorized) {
            // Access has been granted.
            let image = UIImagePickerController()
            image.delegate = self
            image.sourceType = UIImagePickerController.SourceType.photoLibrary
            image.allowsEditing = true
            
            self.present(image, animated: true)
        }
        else if (status == PHAuthorizationStatus.denied) {
            // Access has been denied.
            print("alert")
            let alertController = UIAlertController(title: "This app does not have access to your Photos.",
                                                    message: "Turn on your Photos from the settings to help Access your Gallery",
                                                    preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (alertAction) in
                if let appSettings = URL(string: UIApplicationOpenSettingsURLString) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(appSettings as URL)
                    } else {
                        // Fallback on earlier versions
                    }
                }
            }
            alertController.addAction(settingsAction)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else if (status == PHAuthorizationStatus.notDetermined) {
            // Access has not been determined.
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                if (newStatus == PHAuthorizationStatus.authorized) {
                    
                }
                else {
                }
            })
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image1 = info[UIImagePickerControllerEditedImage] as? UIImage
        {
            let cropimage = resizeImage(image: image1, targetSize: CGSize(width: 400.0, height: 400.0))
            
            let imageData:NSData = UIImageJPEGRepresentation(cropimage, 0.50)! as NSData //UIImagePNGRepresentation(img)
            let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
            if let asset = info[UIImagePickerControllerPHAsset] as? PHAsset {
                let assetResources = PHAssetResource.assetResources(for: asset)
                let indexPath = IndexPath(row: 0, section: 0)
//                let cell = self.editAccountTableView?.cellForRow(at: indexPath) as? EditAccountTableViewCell
                
               
                //  let cropimage = resizeImage(image: image1, targetSize: CGSize(width: 400.0, height: 400.0))
                
              //  print(cropimage.size)
                
                
                
                self.dismiss(animated: true, completion: nil)
                print("stauscode data converting")
                
                //  print(imgString)
                let photname = info["UIImagePickerControllerImageURL"] as! URL
                //   let photo = "\(photname)"
                
                
                
                let result = PHAsset.fetchAssets(withALAssetURLs: [photname], options: nil)
                let asset = result.firstObject
                // print(asset?.value(forKey: "filename"))
                 let id = themes.getUserId()
               EditAccountApi(user_id: id, name: "", phoneNumber: "", password: "", photoName: assetResources.first!.originalFilename, photoStr: imgString)
                
                print(assetResources.first!.originalFilename)
            }else{
                
                let indexPath = IndexPath(row: 0, section: 0)
                let cell = self.editAccountTableView?.cellForRow(at: indexPath) as? EditAccountTableViewCell
                let id = themes.getUserId()
                cell?.profileImage.image = image1
                 EditAccountApi(user_id: id, name: "", phoneNumber: "", password: "", photoName: "camera.jpg", photoStr: imgString)
                
                
                
            }
            
            dismiss(animated: true, completion: nil)
        }else if let img = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            
            let imageData:NSData = UIImageJPEGRepresentation(img, 0.50)! as NSData //UIImagePNGRepresentation(img)
            let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
            if let asset = info[UIImagePickerControllerPHAsset] as? PHAsset {
                let assetResources = PHAssetResource.assetResources(for: asset)
                let indexPath = IndexPath(row: 0, section: 0)
                let cell = self.editAccountTableView?.cellForRow(at: indexPath) as? EditAccountTableViewCell
             
                cell?.profileImage.image = img
               // let cropimage = resizeImage(image: img, targetSize: CGSize(width: 400.0, height: 400.0))
                //print(cropimage.size)
                
            }
            
            dismiss(animated: true, completion: nil)
        }
        
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    func ValidatedTextField()-> Bool {
        
        let oldPass = themes.getPassword()
        
        if (firstTextField.text == ""){
            self.themes.showAlert(title: "Alert", message: "Current Password is Mandatory", sender: self)
            // self.userNameField.becomeFirstResponder()
            return false
        }
        else if (secondTextField.text == "") {
            self.themes.showAlert(title: "Alert", message: "NewPassword is Mandatory", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        } else if (thirdTextField.text == "") {
            self.themes.showAlert(title: "Alert", message: "Please Re-Type the NewPassword", sender: self)
            // self.passwordField.becomeFirstResponder()
            return false
        }
        
        
        if (firstTextField.text == oldPass) {
            if (secondTextField.text == thirdTextField.text) {
                return true
            } else {
                self.themes.showAlert(title: "Alert", message: "Current password mismatched", sender: self)
            }
            
        }
        if (secondTextField.text == thirdTextField.text ){
            return true
        } else {
            self.themes.showAlert(title: "Alert", message: "Password Mismatched", sender: self)
            return false
        }
        return true
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        popUpView.isHidden = true
        hideView.isHidden = true
        
        let id = themes.getUserId()
        if (PopViewName == "name") {
            
            EditAccountApi(user_id: id, name: firstTextField.text!, phoneNumber: "", password: "", photoName: "", photoStr: "")
            
        } else if (PopViewName == "phoneNumber") {
            
           EditAccountApi(user_id: id, name: "", phoneNumber: firstTextField.text!, password: "", photoName: "", photoStr: "")
            
        } else if (PopViewName == "password") {
            
            if ValidatedTextField() == true {
            
           EditAccountApi(user_id: id, name: "", phoneNumber: "", password: thirdTextField.text!, photoName: "", photoStr: "")
            } else {
                
            }
            
        }
       

    }
    
    @objc func firstNameBtnClicked(button : UIButton) {
        
        PopViewName = "name"
        popUpView.isHidden = false
        hideView.isHidden = false
        secondTextField.isHidden = true
        thirdTextField.isHidden = true
        popViewHeight.constant = 205
        firstTextField.attributedPlaceholder = NSAttributedString(string: "First Name",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        firstTextField.text = self.themes.getUserName()

    }
    
    @objc func lastNameBtnClicked(button : UIButton) {
        popUpView.isHidden = false
        hideView.isHidden = false
        secondTextField.isHidden = true
        thirdTextField.isHidden = true
        popViewHeight.constant = 205
        firstTextField.attributedPlaceholder = NSAttributedString(string: "Last Name",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        

        
    }
    
    @objc func phoneNumberBtnClicked(button : UIButton) {
        PopViewName = "phoneNumber"
        popUpView.isHidden = false
        hideView.isHidden = false
        secondTextField.isHidden = true
        thirdTextField.isHidden = true
        popViewHeight.constant = 205
        firstTextField.attributedPlaceholder = NSAttributedString(string: "Phone Number",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        firstTextField.text = self.themes.getmobileNumber()
        
//        firstTextField.keyboardType = UIKeyboardType.numberPad
    }
    
    @objc func emailBtnClicked(button : UIButton) {
        
        popUpView.isHidden = false
        hideView.isHidden = false
        secondTextField.isHidden = true
        thirdTextField.isHidden = true
        popViewHeight.constant = 205
        firstTextField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        firstTextField.text = self.themes.getuserMail()

        
    }
    
    @objc func passwordBtnClicked(button : UIButton) {
        PopViewName = "password"
        popUpView.isHidden = false
        hideView.isHidden = false
        popViewHeight.constant = 325
        secondTextField.isHidden = false
        thirdTextField.isHidden = false
        firstTextField.attributedPlaceholder = NSAttributedString(string: "Old Password",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        secondTextField.attributedPlaceholder = NSAttributedString(string: "New Password",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])

        thirdTextField.attributedPlaceholder = NSAttributedString(string: "Confirm Password",
                                                                  attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(28/255.0), green: CGFloat(37/255.0), blue: CGFloat(42/255.0), alpha: CGFloat(1.0) )])
        
        firstTextField.text = self.themes.getPassword()
        

    }
    
    
    func EditAccountApi(user_id : String, name : String, phoneNumber : String, password : String,photoName:String,photoStr:String) {

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = [ "user_id": user_id , "name": name, "phone": phoneNumber, "password":password, "image":photoStr,
                               "image_name":photoName] as [String : Any]
            
            print("log in parameters is \(parameters)")
             urlService.serviceCallPostMethodWithParams(url:editAccount, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let data = response["data"]

                let name = data!["user_name"] as! String
                let password = data!["password"] as! String
                let phone_number = data!["phone_number"] as! String
                
                let image = data!["user_image"] as! String
                
                
                self.themes.saveuserDP(image)
                self.themes.saveUserName(name)
                self.themes.savePassword(password: password)
                self.themes.saveMobileNumber(phone_number)

                 self.editAccountTableView.reloadData()

                    self.themes.hideActivityIndicator(uiView: self.view)

            }
            
             self.themes.hideActivityIndicator(uiView: self.view)
//            Alamofire.request("https://ridehub.co/uploaded/userprofiles/app/profile-edit", method: .post, parameters: parameters,encoding:
//                JSONEncoding.default, headers: nil).responseJSON {
//                    response in
//                    switch response.result {
//                    case .success:
//                        self.themes.hideActivityIndicator(uiView: self.view)
//                        print(response)
//                        break
//
//                    case .failure(let error):
//                        print(error)
//                        self.themes.hideActivityIndicator(uiView: self.view)
//                    }
//            }
//
            
            
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        firstTextField.resignFirstResponder();
        secondTextField.resignFirstResponder();
        thirdTextField.resignFirstResponder();
        return true;
    }

}
