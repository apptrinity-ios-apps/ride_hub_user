//
//  HelpChoosePaymentVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 24/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class HelpChoosePaymentVC: UIViewController {
    
    @IBOutlet weak var dotLine: UIView!
    
    @IBOutlet weak var btnsView: UIView!
 
    @IBOutlet weak var nextBtn: UIButton!
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
//        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
//        
//        self.navigationController?.pushViewController(Vc, animated: true)
//        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnsView.layer.cornerRadius = 20
        nextBtn.layer.cornerRadius = nextBtn.frame.size.height/2
        btnsView.layer.borderWidth = 1
        btnsView.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        
        nextBtn.layer.masksToBounds = false
        nextBtn.layer.shadowColor = UIColor.lightGray.cgColor
        nextBtn.layer.shadowOpacity = 0.5
        nextBtn.layer.shadowOffset = CGSize(width: 2, height: 2)
        nextBtn.layer.shadowRadius = 1


        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        
        // Do any additional setup after loading the view.
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
}
