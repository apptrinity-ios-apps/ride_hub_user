//
//  HelpSubscriptionsVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 24/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class HelpSubscriptionsVC: UIViewController {
    
    @IBOutlet weak var dotLine: UIView!
    
    @IBAction func backAction(_ sender: Any) {
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpChoosePaymentVC") as! HelpChoosePaymentVC
        
        self.navigationController?.pushViewController(Vc, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)

        // Do any additional setup after loading the view.
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }


}
