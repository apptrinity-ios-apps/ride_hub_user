//
//  AllTopicsTableCell.swift
//  RideHub
//
//  Created by INDOBYTES on 30/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class AllTopicsTableCell: UITableViewCell {

    
    @IBOutlet weak var allTopicsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
