//
//  HelpLastTripVC.swift
//  RideHub
//
//  Created by INDOBYTES on 30/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class HelpLastTripVC: UIViewController,UITableViewDelegate,UITableViewDataSource,ratingEditDelegate {
    func ratingEdit() {
        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "EditRatingVC") as! EditRatingVC
        rideVc.rideID = rideId
        self.navigationController?.pushViewController(rideVc, animated: true)
    }
    
    
    
    @IBOutlet weak var dotLine: UIView!
    
    @IBAction func backAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    var helpCategoryList = Array<AnyObject>()
    let themes = Themes()
    var detailRide = Array<AnyObject>()
    var rideId = String()
    @IBOutlet weak var helpTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        helpTableView.separatorColor = UIColor.clear
        helpTableView.delegate = self
        helpTableView.dataSource = self
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        self.helpTableView.register(UINib(nibName: "LastTripTableCell", bundle: nil), forCellReuseIdentifier: "LastTripTableCell")
        self.helpTableView.register(UINib(nibName: "NeedHelpTableCell", bundle: nil), forCellReuseIdentifier: "NeedHelpTableCell")
        self.helpTableView.register(UINib(nibName: "HeaderAllTopicsTableCell", bundle: nil), forCellReuseIdentifier: "HeaderAllTopicsTableCell")
        self.helpTableView.register(UINib(nibName: "AllTopicsTableCell", bundle: nil), forCellReuseIdentifier: "AllTopicsTableCell")
        self.helpTableView.register(UINib(nibName: "HelpDetailsTableCell", bundle: nil), forCellReuseIdentifier: "HelpDetailsTableCell")
        latestRideApi()
         helpListApi()
        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let header = tableView.dequeueReusableCell(withIdentifier: "HeaderAllTopicsTableCell") as! HeaderAllTopicsTableCell
            
            return header
        } else {
            return UITableViewCell()
        }
    }
    func helpListApi() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
            let parameters = [:] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:helpCategory, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    //  let response =  response["response"] as AnyObject
                    self.helpCategoryList = response["data"] as! [AnyObject]
                    self.helpTableView.reloadData()
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func latestRideApi() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId()] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:latestRide, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    let response1 =  response["response"] as AnyObject
                    self.detailRide = response1["rides"] as! [AnyObject]
                    
                    
                    self.helpTableView.reloadData()
                    
                    
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        } else {
            return self.helpCategoryList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "LastTripTableCell", for: indexPath) as! LastTripTableCell
                
                if(self.detailRide.count > 0){
                    let obj = self.detailRide[0]
                    
                    
                    let date = obj["ride_date"] as! String
                    let time = obj["ride_time"] as! String
                    let carName = obj["cab_type"] as! String
                    rideId = obj["ride_id"] as! String
                    
                    //let vehicle_no = obj["vehicle_no"] as! String
                    cell.dateLabel.text =  date + ", " + time
                    cell.carBrandLabel.text =  carName
                    cell.amountLabel.text = (themes.checkNullValue(obj["total_fare"] as AnyObject) as! String)
                    let fullimageUrl = themes.checkNullValue(obj["image"] as AnyObject) as! String
                    if(fullimageUrl == ""){
                        cell.mapImageView.image = UIImage(named: "defaultmap")
                    }else{
                        
                        let url = URL(string: mapsBaseUrl + fullimageUrl)
                        cell.mapImageView.load(url: url!)
                    }
                    
                }else{
                    
                }
               
                cell.selectionStyle = .none
                cell.detailBtn.addTarget(self, action: #selector(detailBtnClicked(button:)), for: .touchUpInside)
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "NeedHelpTableCell", for: indexPath) as! NeedHelpTableCell
                cell.delegate = self
                cell.selectionStyle = .none
                
                return cell
            } else {
                return UITableViewCell()
            }
            
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HelpDetailsTableCell", for: indexPath) as! HelpDetailsTableCell
            let obj = self.helpCategoryList[indexPath.row]
            cell.helpLabel.text = (obj["category"] as! String)
            cell.selectionStyle = .none
            return cell
        } else {
            return UITableViewCell()
        }
    }
     @objc func detailBtnClicked(button: UIButton){
        
        
        
        
        
        retrieveMyRideslist(id: rideId)
        
    }
    func retrieveMyRideslist(id:String){
        themes.showActivityIndicator(uiView: self.view)
        let parameters = [ "user_id": themes.getUserId(), "ride_id": id] as [String : Any]
        print("parameters is \(parameters)")
        urlService.serviceCallPostMethodWithParams(url:viewRide, params: parameters as Dictionary<String, Any>) { response in
            //            print(response)
            let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
            if(isDead == "Yes"){
                self.themes.hideActivityIndicator(uiView: self.view)
                self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(rideVc, animated: true)
            }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let some =  response["response"] as! [String : AnyObject]
                //            print(some)
                let details = some["details"] as! [String : AnyObject]
                //            print(self.totalRideArr)
                let ridevc = self.storyboard?.instantiateViewController(withIdentifier: "TripDetailsVC") as! TripDetailsVC
                ridevc.detail = details
                self.navigationController?.pushViewController(ridevc, animated: true)
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 236
            } else if indexPath.row == 1 {
                return 190
            } else {
                return 0
            }
        } else if indexPath.section == 1 {
            return 44
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else if section == 1{
            return 44
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(indexPath.section == 1){
            let helpSubViewController = self.storyboard?.instantiateViewController(withIdentifier: "HelpSubViewController") as! HelpSubViewController
            let obj = helpCategoryList[indexPath.row]
            helpSubViewController.question = (obj["category"] as! String)
            helpSubViewController.categoryId = obj["id"] as! String
            self.navigationController?.pushViewController(helpSubViewController, animated: true)
        }
    
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }


}
