//
//  LastTripTableCell.swift
//  RideHub
//
//  Created by INDOBYTES on 30/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class LastTripTableCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var detailBtn: UIButton!
    @IBOutlet weak var carBrandLabel: UILabel!
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var mapImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
