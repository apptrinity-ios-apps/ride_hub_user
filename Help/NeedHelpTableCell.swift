//
//  NeedHelpTableCell.swift
//  RideHub
//
//  Created by INDOBYTES on 30/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

protocol ratingEditDelegate{
    func ratingEdit()
    
    
    
}




class NeedHelpTableCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    

     var delegate: ratingEditDelegate!
    @IBOutlet weak var needHelpCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        needHelpCollectionView.delegate = self
        needHelpCollectionView.dataSource = self
        
        self.needHelpCollectionView.register(UINib(nibName: "RatingEditCollectionCell", bundle: nil), forCellWithReuseIdentifier: "RatingEditCollectionCell")
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RatingEditCollectionCell", for: indexPath) as! RatingEditCollectionCell
        cell.editRatingBtn.addTarget(self, action: #selector(editRatingBtnClicked(button:)), for: .touchUpInside)
        cell.starRatingBtn.addTarget(self, action: #selector(starRatingBtnClicked(button:)), for: .touchUpInside)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //        let screenSize = UIScreen.main.bounds
        //        let screenWidth = screenSize.width
        return CGSize(width: 138 , height: 108)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        
        return 0
        
    }
    
    
    
    
    @objc func editRatingBtnClicked(button : UIButton){
        delegate.ratingEdit()
    }
    
    @objc func starRatingBtnClicked(button : UIButton){
        
    }
    
}
