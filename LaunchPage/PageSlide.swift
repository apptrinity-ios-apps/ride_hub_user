//
//  PageSlide.swift
//  RideHub
//
//  Created by INDOBYTES on 05/11/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import Foundation
import UIKit


class PageSlide : UIView {
    @IBOutlet weak var dotLine: UIView!
    
    @IBOutlet weak var headLabel: UILabel!
    
    @IBOutlet weak var descLabel: UILabel!

    @IBOutlet weak var imageView: UIImageView!
    
}

