//
//  LaunchPageVC.swift
//  RideHub
//
//  Created by INDOBYTES on 05/11/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

var urlService = URLservices.sharedInstance

class LaunchPageVC: UIViewController,UIScrollViewDelegate {
    
    var pageNumbr = Int()
    
    @IBOutlet var scroollView: UIScrollView!{
        didSet{
            scroollView.delegate = self
        }
    }

    
    @IBOutlet var skipBtn: UIButton!
    @IBOutlet var nextBtn: UIButton!
    @IBOutlet var pageControl: UIPageControl!
    
    @IBAction func skipAction(_ sender: Any) {
        
        
        UserDefaults.standard.set(true, forKey: "checkTutorial")
        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginMainVC") as! LoginMainVC
        self.navigationController?.pushViewController(rideVc, animated: true)
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
         UserDefaults.standard.set(true, forKey: "checkTutorial")
        
        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginMainVC") as! LoginMainVC
        self.navigationController?.pushViewController(rideVc, animated: true)
        
        
        
    }
    
    
    @IBAction func pageControlAction(_ sender: Any) {
        
        
    }
    
    
     var slides:[PageSlide] = [];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        scroollView.delegate = self
        pageControl.numberOfPages = slides.count
        pageControl.currentPage = 0
        view.bringSubview(toFront: pageControl)
        
        
        
        nextBtn.isHidden = true
        

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    func createSlides() -> [PageSlide] {
        
        let slide1:PageSlide = Bundle.main.loadNibNamed("PageSlide", owner: self, options: nil)?.first as! PageSlide
        slide1.imageView.image = UIImage(named: "page1")
        slide1.headLabel.text = "Request a Ride"
         slide1.descLabel.text = "Request a ride get picked up by a nearby community driver."
        
        
       drawDottedLine(start: CGPoint(x: slide1.dotLine.bounds.minX, y: slide1.dotLine.bounds.minY), end: CGPoint(x: slide1.dotLine.bounds.maxX, y: slide1.dotLine.bounds.minY), view: slide1.dotLine)
        
        let slide2:PageSlide = Bundle.main.loadNibNamed("PageSlide", owner: self, options: nil)?.first as! PageSlide
        slide2.imageView.image = UIImage(named: "page2")
        slide2.headLabel.text = "Choose Vehicle"
         slide2.descLabel.text = "Users have the liberty to choose the type of vehicle as per their need."
         drawDottedLine(start: CGPoint(x: slide2.dotLine.bounds.minX, y: slide2.dotLine.bounds.minY), end: CGPoint(x: slide2.dotLine.bounds.maxX, y: slide2.dotLine.bounds.minY), view: slide2.dotLine)
        let slide3:PageSlide = Bundle.main.loadNibNamed("PageSlide", owner: self, options: nil)?.first as! PageSlide
        slide3.imageView.image = UIImage(named: "page3")
        slide3.headLabel.text = "Live Ride Tracking"
         slide3.descLabel.text = "Know your driver in advance and be able to view current location in real time on the map"
         drawDottedLine(start: CGPoint(x: slide3.dotLine.bounds.minX, y: slide3.dotLine.bounds.minY), end: CGPoint(x: slide3.dotLine.bounds.maxX, y: slide3.dotLine.bounds.minY), view: slide3.dotLine)
        let slide4:PageSlide = Bundle.main.loadNibNamed("PageSlide", owner: self, options: nil)?.first as! PageSlide
        slide4.imageView.image = UIImage(named: "page4")
        slide4.headLabel.text = "Trip Sharing"
        slide4.descLabel.text = "Passengers can share their ride details with family and friends for safety reasons."
         drawDottedLine(start: CGPoint(x: slide4.dotLine.bounds.minX, y: slide4.dotLine.bounds.minY), end: CGPoint(x: slide4.dotLine.bounds.maxX, y: slide4.dotLine.bounds.minY), view: slide4.dotLine)
        
        return [slide1, slide2, slide3, slide4]
    }
    
    
    func setupSlideScrollView(slides : [PageSlide]) {
        scroollView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        scroollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: view.frame.height)
        scroollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scroollView.addSubview(slides[i])
        }
    }
    
    /*
     * default function called when view is scolled. In order to enable callback
     * when scrollview is scrolled, the below code needs to be called:
     * slideScrollView.delegate = self or
     */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        pageNumbr = pageControl.currentPage
        
        if (pageNumbr == 3) {
            nextBtn.isHidden = false
        } else {
            nextBtn.isHidden = true
        }
        
        if scrollView.contentOffset.y > 0 || scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        }
    }
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        //        shapeLayer.strokeColor = (UIColor.init(red: 6, green: 152, blue: 212, alpha: 1) as! CGColor)
        //        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineDashPattern =  [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    

}
