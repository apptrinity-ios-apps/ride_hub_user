//
//  Constants.swift
//  4Print
//  Created by S s Vali on 3/14/18.
//  Copyright © 2018 com.Indobytes. All rights reserved.

import UIKit


//var AppbaseUrl =  "https://54.169.136.144/v4/"
var AppbaseUrl =  "https://ridehub.co/v4/"
var mapsBaseUrl = "https://ridehub.co/"
var ImagebaseUrl =  "https://ridehub.co/images/users/"
var kXmppHostName =  "54.169.136.144"
let GoogleServerKey = "AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw"
var uberClient_ID = "NGFkBy0ojytzrLaTNuO8RK6QNCJyD0PK"
var uber_Clientsecret_key = "CkYFMstM_AhYx5u7UFyQpTWuqV86QQTKno_Gak8N"
var mailInvoice = "app/mail-invoice"
var UberLoginTokenUrl = "https://login.uber.com/oauth/v2/token"
var UberAppbaseUrl = "https://sandbox-api.uber.com"
var UberCodeStr = ""
var UberAcessToken = ""
var Uber_rider_Details = "https://api.uber.com/v1.2/me"
var uberDefaultAccessToken = "JA.VUNmGAAAAAAAEgASAAAABwAIAAwAAAAAAAAAEgAAAAAAAAG8AAAAFAAAAAAADgAQAAQAAAAIAAwAAAAOAAAAkAAAABwAAAAEAAAAEAAAAJw64-wJyDwMIquW_OsmZX9sAAAAV2ZgLu_6PA0KoucriTlykBYxgwIwZMvmvMkNRVmZ7GiGUjot1VRTDNRfLY163r2DkzND82v1J1NUm7i9Uo6U2LiJHti_eBSVG_0YNTdl413hFIkQ1nDK8s6WrB4miGDlAnWs0qEciE_EUzYpDAAAAErDO_0iCrNEHZ_4-iQAAABiMGQ4NTgwMy0zOGEwLTQyYjMtODA2ZS03YTRjZjhlMTk2ZWU"
let  optionslist = "app/review/options-list"
var getUberReceipt = "https://api.uber.com/v1.2/requests/"
var getUberRidesHistory = "https://api.uber.com/v1.2/history"
var globalRideId = ""
let rideCanel = "app/cancel-ride"
let cancelReason = "app/cancellation-reason"
let userLocationUpdate = "app/set-user-geo"
let kAppInfo = "UserAppInfokey"
let applogin = "app/login"
let deleteRide = "app/delete-ride"
let carsDetails_Taxi = "app/get-map-view"
let paymentList = "app/payment-list"
let PaymentByCash = "app/payment/by-cash"
let PaymentByWallet = "app/payment/by-wallet"
let detectiontionAuto = "app/payment/by-auto-detect"
let paymentGetWay = "app/payment/by-gateway"
let paymentByCashMethod = "app/payment/by-cash"
let forXMPP = "api/xmpp-status"
let myRide = "app/my-rides"
let viewRide = "app/view-ride"
let appRegister = "app/check-user"
let OTP = "app/register"
let getRouteFromGoogle = "https://maps.googleapis.com/maps/api/directions/json"
let getAppInfo = "api/v3/get-app-info"
let Map_and_Taxi_Selection = "app/get-map-view"
let confirmRide = "app/book-ride"
let tipsAdding = "app/apply-tips"
let referPointsApi = "app/payment/by-points"
let imgBaseUrl = ""
let imgordersBaseUrl = ""
let logOut = "app/logout"
let RateSubmit = "app/review/submit"
let savedCards = "mobile/user/get_saved_stripe_cards"
let btnFont = "System"
let paymentWithSavedCard = "app/payment/by-auto-detect"
let refer_Points = "app/user/points-list"
let inviteCode = "app/get-invites"
let editAccount = "app/profile-edit"
let forGotPassword = "app/reset-password"
let recentRideList = "app/recent-rides"
let addFavoriteLocation = "app/addFavoriteLocations"
let getFavoriteLocation = "app/getFavoriteLocations"
let helpCategory = "app/help-category"
let helpSubCategory = "app/help-sub-category"
let helpQuestionList = "app/help-questions-list"
let StripePayService = "StripePayService"
let paymentUrl = "stripePayment"
let walletPayment = "mobile/wallet-recharge/stripe-process"
let latestRide = "app/user/get-latest-ride-list"
var globalPickUpLatitude = 0.0
var globalPickUpLongitude = 0.0
var globalDropLatitude = 0.0
var globalDropLongitude = 0.0
var cabArrivedHint = false
var requestPaymentHint = false
var ratingHint = false
 var cabType = ""
var getWalletMoney = "app/get-money-page"
var driverNote = String()
var driverDetails = NSDictionary()

// Hexa ColorCode
let textFieldColor = UIColor(red: 22/255.0, green: 48/255.0, blue: 61/255.0, alpha: 0.7)
let DARKBLUECOLOR = UIColor(red: 4/255.0, green: 68/255.0, blue: 126/255.0, alpha: 1.0)
let BlackColor = UIColor(red: 67/255.0, green: 67/255.0, blue: 67/255.0, alpha: 1.0)
let GREENCOLOR = UIColor(red: 21/255.0, green: 196/255.0, blue: 54/255.0, alpha: 1.0)
let YELLOWCOLOR = UIColor(red: 253/255.0, green: 183/255.0, blue: 20/255.0, alpha: 1.0)
let REDCOLOR = UIColor(red: 255/255.0, green: 74/255.0, blue: 74/255.0, alpha: 1.0)
let BLUECOLOR = UIColor(red: 63/255.0, green: 144/255.0, blue: 195/255.0, alpha: 1.0)
let AMOUNTCOLOR = UIColor(red: 32/255.0, green: 133/255.0, blue: 197/255.0, alpha: 1.0)
func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
    let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
    let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
    let blue = CGFloat(rgbValue & 0xFF)/256.0
    return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
}
