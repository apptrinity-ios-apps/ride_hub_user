import Foundation
import UIKit
import Alamofire
import AlamofireImage
import SystemConfiguration
class URLservices: UIViewController {
   // var userID:UserDefaults!
    //SingleTon
    var ShowDeadAlert = Bool()
    var themes = Themes()
    //let appDelegate = UIApplication.shared.delegate as! AppDelegate
static let sharedInstance : URLservices = {
        let instance = URLservices()
        return instance
}()
override func viewDidLoad() {
        super.viewDidLoad()
}
   
func serviceCallGetMethod(url:String,completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl =  AppbaseUrl + urlString
          Alamofire.request(totalUrl,method: .get,  encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
                // check for errors
                guard response.result.error == nil else {
                    // got an error in getting the data, need to handle it
                    return
                }
                guard let json = response.result.value as? [String: Any] else {
                    return
                }
                //print(json)
                completionHandler(json as [String : AnyObject])
        }
    }
    // Calling PostMethod With Parameters
func serviceCallPostMethodWithParams(url:String, params:Dictionary<String,Any> , completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl =  AppbaseUrl + urlString
        let params: [String: Any] = params
        // Converting Dictinary to Array formmat and Than Converting to String Formmat
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        let param = data.joined(separator: "&")
        var request = URLRequest(url: try!  totalUrl.asURL())
        request.httpMethod = "POST"
        request.setValue("ios", forHTTPHeaderField: "Apptype")
         request.setValue(themes.getUserId(), forHTTPHeaderField: "Userid")
         request.setValue(themes.getDeviceToken(), forHTTPHeaderField: "Apptoken")
//         request.setValue("a498f1e1b936c6823de5b7c7b650352da79d30c86935ef6a75ee2f50f854cfc7", forHTTPHeaderField: "Apptoken")
        request.httpBody = param.data(using: .utf8)
        Alamofire.request(request).responseJSON { response in
                guard response.result.error == nil else {
                    print("error calling POST on /todos/1")
                    print(response.result.error!)
//                    guard let json = response.result.error as? [String: Any]
//                        else {
//                            let JJson = response.result.error as? [String: Any]
//                            var deadStr = JJson!["is_dead"] as! String
//                            if deadStr.count > 0 {
//                                if self.ShowDeadAlert == false {
//                                    self.ShowDeadAlert = true
////                                     self.appDelegate.showAlertForSessionLogout(message: "Your Session has been Logged out...\nKindly Login again")
//                                    let sessionManager = Alamofire.SessionManager.default
//                                    sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in dataTasks.forEach { $0.cancel() }
//                                        uploadTasks.forEach { $0.cancel() }
//                                        downloadTasks.forEach { $0.cancel() }
//                                    }
//
//                                }
//                            }
//                            return
//
//
//                    }
                    return
                }
                // make sure we got some JSON since that's what we expect
                guard let json = response.result.value as? [String: Any]
                    else {
                        print("didn't get todo object as JSON from API")
                        return
                }
                print(json)
            let deadStr = self.themes.checkNullValue(json["is_dead"]) as! String
            //if(deadStr == ""){
                completionHandler(json as [String : AnyObject])
//            }else{
//                if self.ShowDeadAlert == false {
//                    self.ShowDeadAlert = true
//
//                    self.themes.isDeadFunction()
//                     DispatchQueue.main.async {
//                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
//                    }
                    
//                    DispatchQueue.main.async {
//
//                        self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
//
                    
                        
//                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//                        self.navigationController?.pushViewController(rideVc, animated: true)
                  //  }
                    
                    
//
//                    print("djfngidugnidufngidfg")
//                    let AlertMesg = UIAlertController(title: "Message", message: " Your Session has been Logged out...\nKindly Login again", preferredStyle: UIAlertControllerStyle.alert)
//                    AlertMesg.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
//                        self.themes.hideActivityIndicator(uiView: self.view)
//                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//                        self.navigationController?.pushViewController(rideVc, animated: true)
//                    }))
//                    self.present(AlertMesg, animated: true, completion: nil)
//
//
                    
                    
                    
                    //self.showAlertForSessionLogout(message: "Your Session has been Logged out...\nKindly Login again")
                   
//                    let sessionManager = Alamofire.SessionManager.default
//                    sessionManager.session.getTasksWithCompletionHandler { dataTasks, uploadTasks, downloadTasks in dataTasks.forEach { $0.cancel() }
//                        uploadTasks.forEach { $0.cancel() }
//                        downloadTasks.forEach { $0.cancel() }
//                    }
                    
               // }
               return
            }
            
            
            
       // }
    }
    
    func serviceCallPostMethodWithParamsWithOutConversion(url:String, params:Dictionary<String,Any> , completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl =  AppbaseUrl + urlString
        let params: [String: Any] = params
        // Converting Dictinary to Array formmat and Than Converting to String Formmat
        Alamofire.request(totalUrl,method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
            
            guard response.result.error == nil else {
                print("error calling POST on /todos/1")
                print(response.result.error!)
                
                //  self.themes.hideActivityIndicator(uiView: self.view)
                
                return
            }
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any]
                else {
                    print("didn't get todo object as JSON from API")
                    // print("Error: \(String(describing: response.result.error))")
                    return
            }
            print(json)
            completionHandler(json as [String : AnyObject])
        }
    }
    
//    func showAlertForSessionLogout(message:String){
//        let alert = UIAlertController(title: "Ride Hub", message: message, preferredStyle: .alert)
//        let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
//        alert.addAction(cancelButton)
//        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
//    }
    
    
    
    
    
    // NetworkReachabilityManager
func connectedToNetwork()  -> Bool {
    
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return isReachable && !needsConnection
    }
    
    func Uber_ServiceCallPostMethodWithParams(url:String, params:Dictionary<String,Any> , completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl =  urlString
        let params: [String: Any] = params
        // Converting Dictinary to Array formmat and Than Converting to String Formmat
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        let param = data.joined(separator: "&")
        var request = URLRequest(url: try!  totalUrl.asURL())
        request.httpMethod = "POST"
        request.httpBody = param.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { response in
            guard response.result.error == nil else {
                print("error calling POST on /todos/1")
                print(response.result.error!)
                guard let json = response.result.error as? [String: Any]
                    else {
                        return
                }
                return
            }
            // make sure we got some JSON since that's what we expect
            guard let json = response.result.value as? [String: Any]
                else {
                    print("didn't get todo object as JSON from API")
                    return
            }
            print(json)
            completionHandler(json as [String : AnyObject])
        }
    }
    
    
    
    func UberServiceCallGetMethod(url:String,completionHandler:@escaping (_ success: [String : AnyObject]) -> ())
    {
        let urlString: String = url
        let totalUrl = urlString
        Alamofire.request(totalUrl,method: .get,  encoding: JSONEncoding.default).responseJSON { (response:DataResponse<Any>) in
            // check for errors
            guard response.result.error == nil else {
                // got an error in getting the data, need to handle it
                return
            }
            guard let json = response.result.value as? [String: Any] else {
                return
            }
            completionHandler(json as [String : AnyObject])
        }
    }
    

func download_Image(urlString: String,type:String,completion: @escaping (_ success: UIImage) -> ()) {
        var totalUrl = String()
            totalUrl =  imgBaseUrl + urlString
        let imgVW = UIImageView()
        Alamofire.request(totalUrl).responseImage { response in
            if let image1 = response.result.value {
                imgVW.image = image1
            }else{
                
            }
            completion(imgVW.image!)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}



extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}







