//
//  Themes.swift
//  FourPrint
//
//  Created by nagaraj  kumar on 07/02/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class Themes: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    var container: UIView = UIView()
    var loadingView: UIView = UIView()
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    // Mark: ActivityIndicator
    // Showing customized activity indicator
    func showActivityIndicator(uiView: UIView) {
        
        
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColorFromHex(rgbValue: 0x00000, alpha: 0.5)
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.clear
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        

        
        let jeremyGif = UIImage.gifImageWithName("loading_location_final")
        let imageView = UIImageView(image: jeremyGif)
        imageView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
       
        imageView.layer.cornerRadius = imageView.frame.height / 2
        imageView.clipsToBounds = true
        loadingView.addSubview(imageView)
        
       container.addSubview(loadingView)
        uiView.addSubview(container)
      // activityIndicator.startAnimating()
    }
    
    // Mark: Hidding activity indicator
    func hideActivityIndicator(uiView: UIView) {
        activityIndicator.stopAnimating()
        container.removeFromSuperview()
       // uiView.removeFromSuperview()
    }
    

    
    func shadowForView(view: UIView){
        
        
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowOpacity = 1.0
        view.layer.shadowRadius = 3.0
        view.layer.masksToBounds = false
    }
    
    func setDriverId(id: String){
        UserDefaults.standard.set(id, forKey: "driverID")
    }
    func setDriverName(name: String){
        UserDefaults.standard.set(name, forKey: "driverName")
    }
    func setDriverImage(image: String){
        UserDefaults.standard.set(image, forKey: "driverImage")
    }
    
    func getDriverId() -> String{
        let driverID = UserDefaults.standard.object(forKey: "driverID")
        if driverID == nil {
            
            return ""
            
        } else {
            
            return driverID as! String
        }
    }
    func getDriverImage() -> String{
        let driverImage = UserDefaults.standard.object(forKey: "driverImage")
        if driverImage == nil {
            
            return ""
            
        } else {
            
            return driverImage as! String
        }
    }
    
    func getDriverName() -> String{
        let driverName = UserDefaults.standard.object(forKey: "driverName")
        if driverName == nil {
            
            return ""
            
        } else {
            
            return driverName as! String
        }
    }
    
    
    
    func setUserId(id: String){
        UserDefaults.standard.set(id, forKey: "userID")
    }
    func setUserName(name: String){
        UserDefaults.standard.set(name, forKey: "userName")
    }
    
    func getUsername() -> String{
        let name = UserDefaults.standard.object(forKey: "userName")
        if name == nil {
            
            return ""
            
        } else {
            
            return name as! String
        }
    }

    
    func removeUserId(id: String){
        let userDefault  = UserDefaults.standard
        userDefault.removeObject(forKey: "userID")
        let keyValue = userDefault.string(forKey: "userID")
        print("Key Value after remove \(String(describing: keyValue))")
        
    }
 func saveUserName(_ userName: String?) {
        UserDefaults.standard.set(userName, forKey: "Username")
        UserDefaults.standard.synchronize()
        
    }
    
  
    func isDeadFunction(){
        print("dead dead")
        
        DispatchQueue.main.async {
//            let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            self.navigationController?.pushViewController(rideVc, animated: true)
        }
        
        
        
    }
    
    func getUserName() -> String{
        let Username = UserDefaults.standard.object(forKey: "Username")
        if Username == nil {
            
            return ""
            
        } else {
            
            return Username as! String
        }
    }

    func saveUberCode(_ Currency: String?) {
        UserDefaults.standard.set(Currency, forKey: "ubercode")
        UserDefaults.standard.synchronize()
    }
    
    func getUberCode() -> String? {
        return UserDefaults.standard.object(forKey: "ubercode") as? String
        
    }
    func saveUberAccessToken(_ Currency: String?) {
        UserDefaults.standard.set(Currency, forKey: "accessToken")
        UserDefaults.standard.synchronize()
    }
    
    func getUberAccessToken() -> String? {
        return UserDefaults.standard.object(forKey: "accessToken") as? String
        
    }
    
  func saveUserID(_ userName: String?) {
        UserDefaults.standard.set(userName, forKey: "userID")
        UserDefaults.standard.synchronize()
    }
  func getUserId() -> String{
        let userID = UserDefaults.standard.object(forKey: "userID")
        if userID == nil {
            
            return ""
            
        } else {
            
            return userID as! String
        }
    }
  func saveCouponDetails(_ CouponDetails: String?) {
        UserDefaults.standard.set(CouponDetails, forKey: "CouponDetails")
        UserDefaults.standard.synchronize()
    }
    
  func getCouponDetails() -> String? {
        return UserDefaults.standard.object(forKey: "CouponDetails") as? String
    }
    
  func saveCountryCode(_ CountryCode: String?) {
        UserDefaults.standard.set(CountryCode, forKey: "CountryCode")
        UserDefaults.standard.synchronize()
    }
    
  func getCountryCode() -> String? {
        return UserDefaults.standard.object(forKey: "CountryCode") as? String
        
    }
    
 func saveMobileNumber(_ mobileNumber: String?) {
        UserDefaults.standard.set(mobileNumber, forKey: "mobilenumber")
        UserDefaults.standard.synchronize()
}
    
 func getmobileNumber() -> String? {
        return UserDefaults.standard.object(forKey: "mobilenumber") as? String
    
    }
    
    func saveuserDP(_ Displaypicture: String?) {
        UserDefaults.standard.set(Displaypicture, forKey: "UserDP")
        UserDefaults.standard.synchronize()
        
    }
    func getUserDp() -> String{
        let UserDP = UserDefaults.standard.object(forKey: "UserDP")
        if UserDP == nil {
            return ""
        } else {
            
            return UserDP as! String
        }
    }
    
    
    func savedDeviceToken(token : String){
        UserDefaults.standard.set(token, forKey: "deviceToken")
    }
    
    func savePassword(password : String){
        UserDefaults.standard.set(password, forKey: "savePassword")
    }
    
    func getPassword() -> String {
        return UserDefaults.standard.object(forKey: "savePassword") as! String
    }

    func getDeviceToken() -> String {
        return UserDefaults.standard.object(forKey: "deviceToken") as! String  
    }
    
   func saveCategoryString(_ CategoryString: String?) {
        UserDefaults.standard.set(CategoryString, forKey: "categorystring")
        UserDefaults.standard.synchronize()
    }
    
   func saveSubCategoryString(_ SubCategoryString: String?) {
        UserDefaults.standard.set(SubCategoryString, forKey: "subcategorystring")
        UserDefaults.standard.synchronize()
    }
    
   func getCategoryString() -> String? {
        return UserDefaults.standard.object(forKey: "categorystring") as? String
    }
    
   func getSubCategoryString() -> String? {
        return UserDefaults.standard.object(forKey: "subcategorystring") as? String
    
    }
    
  func saveuserEmail(_ userMail: String?) {
        UserDefaults.standard.set(userMail, forKey: "useremail")
        UserDefaults.standard.synchronize()
        
    }
    
   
    
    func getuserMail() -> String{
        let useremail = UserDefaults.standard.object(forKey: "useremail")
        if useremail == nil {
            return ""
        } else {
            
            return useremail as! String
        }
    }
    
    func saveWallet(_ Amount: String?) {
        UserDefaults.standard.set(Amount, forKey: "walletamount")
        UserDefaults.standard.synchronize()
    }
    
   func getWallet() -> String? {
        return UserDefaults.standard.object(forKey: "walletamount") as? String
        
    }
    
  func saveCurrency(_ Currency: String?) {
        UserDefaults.standard.set(Currency, forKey: "currency")
        UserDefaults.standard.synchronize()
    }
    
  func getCurrency() -> String? {
        return UserDefaults.standard.object(forKey: "currency") as? String
        
    }
    
    func saveFullWallet(_ FullWallet: String?) {
        UserDefaults.standard.set(FullWallet, forKey: "FullWallet")
        
    }
    
    func saveXmppUserCredentials(_ str: String?) {
        
        UserDefaults.standard.set(str, forKey: "xmppusercredentialsdectar")
        UserDefaults.standard.synchronize()
        
    }
    
    func ClearUserInfo(){
        
        saveXmppUserCredentials("")
        saveUserID("")
        saveuserDP("")
        saveCategoryString("")
        saveSubCategoryString("")
        saveuserEmail("")
        saveUserName("")
        saveMobileNumber("")
        saveWallet("")
        saveCurrency("")
        saveCountryCode("")
        UserDefaults.standard.set(nil, forKey: "checkLogIn")
       
        
    }
    
    
    
    
  func getXmppUserCredentials() -> String? {
    return UserDefaults.standard.object(forKey: "xmppusercredentialsdectar") as? String
    }
    
    func showAlert(title:String , message:String,sender:UIViewController)
    {
        
         DispatchQueue.main.async {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK",
                               style: .cancel, handler: nil)
        alert.addAction(ok)
        sender.present(alert, animated: true , completion: nil)
        }
    }
    
    
    
func hasAppDetails() -> Bool {
        
        if !UserDefaults.standard.bool(forKey: "HasLaunchedOnce") {
            UserDefaults.standard.set(true, forKey: "HasLaunchedOnce")
            UserDefaults.standard.synchronize()
            return true
            //1234changed
        } else {
            
            if UserDefaults.standard.dictionaryRepresentation().keys.contains(kAppInfo) {
                let data = UserDefaults.standard.object(forKey: kAppInfo) as? Data
                var myDictionary: [AnyHashable : Any]? = nil
                if let aData = data {
                    myDictionary = NSKeyedUnarchiver.unarchiveObject(with: aData) as? [AnyHashable : Any]
                }
                if myDictionary != nil {
                    return true
                }
            }
        }
        return false
    }

    func clearCache(){
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        
        if let cookies = HTTPCookieStorage.shared.cookies {
            for cookie in cookies {
                HTTPCookieStorage.shared.deleteCookie(cookie)
            }
        }
        
    }
    
 func appAllInfoDatas() -> [AnyHashable : Any]? {
    
    if self.getUserId() != nil {
            let data = UserDefaults.standard.object(forKey: kAppInfo) as? Data
            var myDictionary: [AnyHashable : Any]? = nil
            if let aData = data {
                myDictionary = NSKeyedUnarchiver.unarchiveObject(with: aData) as? [AnyHashable : Any]
            }
            return myDictionary
        }
        return nil
    }
    
    
   
//    func checkNullValue(value : AnyObject?) -> String? {
//        if value is NSNull || value == nil {
//            return ""
//        } else {
//            return value as? String
//        }
//    }
    
    func checkNullValue(_ value: Any?) -> Any? {
        var value = value
        if (value is NSNull) || value == nil {
            value = ""
        }
        var str: String? = nil
        if let aValue = value {
            str = "\(aValue)"
        }
        return str
    }
    
    
    func setNormalFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Regular", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    func setMediumFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Medium", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    func setBoldFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Bold", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    func setSemiBoldNormFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-SemiBold", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    class func saveAppDetails(_ appInforec: AppInfoRecord?) {
        
        let data = NSKeyedArchiver.archivedData(withRootObject: self.records(toAppDict: appInforec))
        UserDefaults.standard.set(data, forKey: kAppInfo)
    }
    
    class func records(toAppDict appInforec: AppInfoRecord?) -> [AnyHashable : Any]? {
        var dict: [AnyHashable : Any] = [:]
        dict["site_contact_mail"] = appInforec?.serviceContactEmail
        dict["customer_service_number"] = appInforec?.serviceNumber
        dict["site_url"] = appInforec?.serviceSiteUrl
        dict["xmpp_host_url"] = appInforec?.serviceXmppHost
        dict["xmpp_host_name"] = appInforec?.serviceXmppPort
        dict["facebook_id"] = appInforec?.serviceFacebookAppId
        dict["google_plus_app_id"] = appInforec?.serviceGooglePlusId
        dict["phone_masking_status"] = appInforec?.servicePhoneMaskingStatus
        dict["ongoing_ride"] = appInforec?.hasPendingRide
        dict["ongoing_ride_id"] = appInforec?.pendingRideID
        dict["rating_pending"] = appInforec?.hasPendingRating
        dict["rating_pending_ride_id"] = appInforec?.pendingRateId
        return dict
    }
    
    class func findCurrencySymbol(byCode _currencyCode: String?) -> String? {
        var themes = Themes()
        var _currencyCode = _currencyCode
        _currencyCode = themes.checkNullValue(_currencyCode) as! String
        let fmtr = NumberFormatter()
        let locale: NSLocale? = self.findLocale(byCurrencyCode: _currencyCode)
        var currencySymbol: String
        if locale != nil {
            if let locale = locale {
                fmtr.locale = locale as Locale
            }
        }
        fmtr.numberStyle = .currency
        currencySymbol = fmtr.currencySymbol
        
        if currencySymbol.count > 1 {
            currencySymbol = (currencySymbol as? NSString)?.substring(to: 1) ?? ""
        }
        return currencySymbol
    }
    
    class func findLocale(byCurrencyCode _currencyCode: String?) -> NSLocale? {
        let locales = NSLocale.availableLocaleIdentifiers
        var locale: NSLocale? = nil
        
        for localeId: String in locales {
            locale = NSLocale(localeIdentifier: localeId)
            let code = locale?.object(forKey: .currencyCode) as? String
            if (code == _currencyCode) {
                break
            } else {
                locale = nil
            }
        }
        return locale
    }
    
  
}


extension UILabel {
    
    func setNormalFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Regular", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    func setMediumFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Medium", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    func setBoldFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-Bold", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    func setSemiBoldNormFontFor(_ label: UILabel? , size:Int) -> UILabel? {
        if let aSize = UIFont(name: "Poppins-SemiBold", size: CGFloat(size)) {
            label?.font = aSize
        }
        return label
    }
    
}

extension UIButton {
    
    func setNormalFontFor(_ btn: UIButton? , size:Int) -> UIButton? {
        if let aSize = UIFont(name: "Poppins-Regular", size: CGFloat(size)) {
            btn?.titleLabel?.font = aSize
        }
        return btn
    }
    func setMediumFontFor(_ btn: UIButton? , size:Int) -> UIButton? {
        if let aSize = UIFont(name: "Poppins-Medium", size: CGFloat(size)) {
            btn?.titleLabel?.font = aSize
        }
        return btn
    }
    func setBoldFontFor(_ btn: UIButton? , size:Int) -> UIButton? {
        if let aSize = UIFont(name: "Poppins-Bold", size: CGFloat(size)) {
            btn?.titleLabel?.font = aSize
        }
        return btn
    }
    func setSemiBoldNormFontFor(_ btn: UIButton? , size:Int) -> UIButton? {
        if let aSize = UIFont(name: "Poppins-SemiBold", size: CGFloat(size)) {
            btn?.titleLabel?.font = aSize
        }
        return btn
    }
    
    
    
}
extension UITextField {
  
    func setNormalFontFor(_ textField: UITextField? , size:Int) -> UITextField? {
        if let aSize = UIFont(name: "Poppins-Regular", size: CGFloat(size)) {
            textField?.font = aSize
        }
        return textField
    }
    func setMediumFontFor(_ textField: UITextField? , size:Int) -> UITextField? {
        if let aSize = UIFont(name: "Poppins-Medium", size: CGFloat(size)) {
            textField?.font = aSize
        }
        return textField
    }
    func setBoldFontFor(_ textField: UITextField? , size:Int) -> UITextField? {
        if let aSize = UIFont(name: "Poppins-Bold", size: CGFloat(size)) {
            textField?.font = aSize
        }
        return textField
    }
    func setSemiBoldNormFontFor(_ textField: UITextField? , size:Int) -> UITextField? {
        if let aSize = UIFont(name: "Poppins-SemiBold", size: CGFloat(size)) {
            textField?.font = aSize
        }
        return textField
    }
    
    
    
}

extension UITextView {
    
func setNormalFontFor(_ UITextView: UITextView?, size:Int) -> UITextView? {
    UITextView?.font = UIFont(name: "Arial Rounded MT Bold",size: CGFloat(size))
    return UITextView
 }
}



extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIView {
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat, roundedRect: CGRect) {
        let path = UIBezierPath(roundedRect: roundedRect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
 }
    
}






