//
//  AppInfoRecord.swift
//  RideHubUser-Swift
//
//  Created by nagaraj  kumar on 06/02/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit

class AppInfoRecord: NSObject {

    var serviceContactEmail = ""
    var serviceNumber = ""
    var serviceSiteUrl = ""
    var serviceXmppHost = ""
    var serviceXmppPort = ""
    var serviceFacebookAppId = ""
    var serviceGooglePlusId = ""
    var servicePhoneMaskingStatus = ""
    var hasPendingRide = ""
    var pendingRideID = ""
    var hasPendingRating = ""
    var pendingRateId = ""
   
}
