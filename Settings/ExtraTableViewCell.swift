//
//  ExtraTableViewCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 31/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class ExtraTableViewCell: UITableViewCell {
    
    @IBOutlet weak var headLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
