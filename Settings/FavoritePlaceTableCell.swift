//
//  FavoritePlaceTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 31/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class FavoritePlaceTableCell: UITableViewCell {
    
    @IBOutlet weak var rightArrowImg: UIImageView!
    
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var placeImage: UIImageView!
    
    @IBOutlet weak var placeNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
