//
//  SettingsViewController.swift
//  NewRideHub
//
//  Created by INDOBYTES on 31/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,LocateOnTheMap,UISearchBarDelegate,GMSAutocompleteFetcherDelegate,GMSMapViewDelegate {
    func didAutocomplete(with predictions: [GMSAutocompletePrediction]) {
        print(predictions)
    }
    
    func didFailAutocompleteWithError(_ error: Error) {
        print(error)
    }
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        print(title)
      
        
        if(selectedIndex == 1){
            homeAddress = title
            UserDefaults.standard.set(title, forKey: "homeAddress")
            
            addFavoriteListAPI(type: "home", lat: lat, long: lon, address: title)
            
        }else if(selectedIndex == 2){
              addFavoriteListAPI(type: "work", lat: lat, long: lon, address: title)
            workAddress = title
             UserDefaults.standard.set(title, forKey: "officeAddress")
        }
        settingsTableView.reloadData()
    }
    let themes = Themes()
    let urlservices = URLservices()
    
     var resultsArray = [String]()
    
    var extraArray = ["Safety","Privacy","Security"]
    var extraDetailsArray = ["Control your safety settings,including Ride Check notifications","Manage the data you share with us","Control your account security with 2-step verification and more"]

    var selectedIndex = Int()
    @IBOutlet weak var dotLine: UIView!
    
    @IBOutlet weak var settingsTableView: UITableView!
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var homeAddress = String()
    var workAddress = String()
     var searchResultController: SearchResultsController!
     var gmsFetcher: GMSAutocompleteFetcher!
    override func viewDidLoad() {
        super.viewDidLoad()
        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        gmsFetcher = GMSAutocompleteFetcher()
        gmsFetcher.delegate = self
        settingsTableView.delegate = self
        settingsTableView.dataSource = self
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
    
        self.settingsTableView.register(UINib(nibName: "UserProfileTableCell", bundle: nil), forCellReuseIdentifier: "UserProfileTableCell")
        self.settingsTableView.register(UINib(nibName: "FavorHeaderTableCell", bundle: nil), forCellReuseIdentifier: "FavorHeaderTableCell")
        self.settingsTableView.register(UINib(nibName: "FavoritePlaceTableCell", bundle: nil), forCellReuseIdentifier: "FavoritePlaceTableCell")
        self.settingsTableView.register(UINib(nibName: "FamilyHeaderTableCell", bundle: nil), forCellReuseIdentifier: "FamilyHeaderTableCell")
        self.settingsTableView.register(UINib(nibName: "VisaTableViewCell", bundle: nil), forCellReuseIdentifier: "VisaTableViewCell")
        self.settingsTableView.register(UINib(nibName: "ExtraTableViewCell", bundle: nil), forCellReuseIdentifier: "ExtraTableViewCell")
        self.settingsTableView.register(UINib(nibName: "SignOutTableViewCell", bundle: nil), forCellReuseIdentifier: "SignOutTableViewCell")

        // Do any additional setup after loading the view.
    }
    func addFavoriteListAPI(type:String,lat:Double,long:Double,address:String) {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
            let userID =  themes.getUserId()
            let parameters = ["user_id" : userID,"latitude":lat,"longitude":long,"title":type,"address":address] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:addFavoriteLocation, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        //let result = response["response"] as! String
                        print(response)
                        
                        
                        
                    }
                    else{
                        self.themes.hideActivityIndicator(uiView: self.view)
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 9
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let header = tableView.dequeueReusableCell(withIdentifier: "FavorHeaderTableCell") as!
            FavorHeaderTableCell
            header.headerLabel.text = "Favorites"
            header.arrowImage.isHidden = true
            header.headerBtn.isHidden = true
            return header
            
        } else if section == 2{
            let header = tableView.dequeueReusableCell(withIdentifier: "FavorHeaderTableCell") as!
            FavorHeaderTableCell
            header.headerLabel.text = "Trusted Contacts"
            header.arrowImage.isHidden = false
            return header
            
        } else if section == 3 {
            let header = tableView.dequeueReusableCell(withIdentifier: "FamilyHeaderTableCell") as!
            FamilyHeaderTableCell
            
            return header
        } else if section == 4 {
            let header = tableView.dequeueReusableCell(withIdentifier: "FavorHeaderTableCell") as!
            FavorHeaderTableCell
            header.headerLabel.text = "Rewards"
            header.arrowImage.isHidden = true
            header.headerBtn.isHidden = true
            
            return header
        } else{
            let header = tableView.dequeueReusableCell(withIdentifier: "FavorHeaderTableCell") as!
            FavorHeaderTableCell            
            header.headerLabel.isHidden = true
            header.arrowImage.isHidden = true
            header.headerBtn.isHidden = true
            return header
        }
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 3
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "UserProfileTableCell", for: indexPath) as! UserProfileTableCell
                cell.selectionStyle = .none
                cell.profileEditBtn.addTarget(self, action: #selector(profileEditBtnClicked(button:)), for: .touchUpInside)
                
                return cell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritePlaceTableCell", for: indexPath) as! FavoritePlaceTableCell
                
                let homeAddress = themes.checkNullValue(UserDefaults.standard.object(forKey: "homeAddress")) as! String
               
                
                
               
                
                    
                    if(homeAddress == ""){
                       
                    }else{
                        cell.addressLabel.text = homeAddress
                    }
                
               // cell.addressLabel.text = homeAddress
                cell.selectionStyle = .none
                cell.placeNameLbl.text = "Add Home"
                cell.placeImage.image = UIImage(named : "home_icon")
                return cell
            } else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritePlaceTableCell", for: indexPath) as! FavoritePlaceTableCell
                
                
             
                let officeAddress = themes.checkNullValue(UserDefaults.standard.object(forKey: "officeAddress")) as! String
                
                
                
                
                    
                    
                    if(officeAddress == ""){
                       
                    }else{
                        cell.addressLabel.text = officeAddress
                    }
                    
                    
                
                
                
                
                
                cell.selectionStyle = .none
                cell.placeNameLbl.text = "Add Work"
                cell.placeImage.image = UIImage(named : "work_icon")
                return cell
            } else if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SignOutTableViewCell", for: indexPath) as! SignOutTableViewCell
                cell.selectionStyle = .none
                
                cell.btnNameLbl.text = "More Saved Place"
                cell.btnNameLbl.textColor = UIColor.init(red: 82/255, green: 162/255, blue: 51/255, alpha: 1.0)
                return cell
            }else {
               return UITableViewCell()
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritePlaceTableCell", for: indexPath) as! FavoritePlaceTableCell
                cell.selectionStyle = .none
                cell.placeNameLbl.text = "Manage Trusted Contacts"
                cell.rightArrowImg.isHidden = true
                cell.placeImage.image = UIImage(named: "contact")
                return cell
            } else {
               return UITableViewCell()
            }
        } else if indexPath.section == 3 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExtraTableViewCell", for: indexPath) as! ExtraTableViewCell
                cell.selectionStyle = .none
                cell.headLabel.text = "Set up your family"
                cell.descriptionLabel.text = "Pay for your loved ones and get trip notifications"
                return cell
            } else {
              return UITableViewCell()
            }
        } else if indexPath.section == 4 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "VisaTableViewCell", for: indexPath) as! VisaTableViewCell
                cell.selectionStyle = .none
                
                return cell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.section == 5 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExtraTableViewCell", for: indexPath) as! ExtraTableViewCell
                cell.selectionStyle = .none
                cell.headLabel.text = "Safety"
                cell.descriptionLabel.text = "Control your safety settings,including Ride Check notifications"
                return cell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.section == 6 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExtraTableViewCell", for: indexPath) as! ExtraTableViewCell
                cell.selectionStyle = .none
                cell.headLabel.text = "Privacy"
                cell.descriptionLabel.text = "Manage the data you share with us"
                return cell
            } else {
                return UITableViewCell()
            }
        } else if indexPath.section == 7 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ExtraTableViewCell", for: indexPath) as! ExtraTableViewCell
                cell.selectionStyle = .none
                cell.headLabel.text = "Security"
                cell.descriptionLabel.text = "Control your account security with 2-step verification and more"
                return cell
            } else {
                return UITableViewCell()
            }
        } else {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "SignOutTableViewCell", for: indexPath) as! SignOutTableViewCell
                cell.selectionStyle = .none
                cell.btnNameLbl.text = "Sign Out"
                cell.btnNameLbl.textColor = UIColor.init(red: 236/255, green: 114/255, blue: 114/255, alpha: 1.0)
                
                 cell.signOutBtn.addTarget(self, action: #selector(signOutBtnClicked(button:)), for: .touchUpInside)
                return cell
            } else {
                return UITableViewCell()
            }
        }
    }
    @objc func signOutBtnClicked(button: UIButton){
        let AlertMesg = UIAlertController(title: "Alert", message: " Are you Sure You want to logout", preferredStyle: UIAlertController.Style.alert)
        AlertMesg.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            self.LogOut()
            //            print("Handle Ok logic here")
        }))
        AlertMesg.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            //            print("Handle Cancel Logic here")
        }))
        present(AlertMesg, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
           return 0
        } else if section == 1 {
           return 50
        } else if section == 2 {
           return 44
        } else if section == 3 {
           return 81
        } else {
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 1){
            if(indexPath.row == 0){
                let searchController = UISearchController(searchResultsController: searchResultController)
                searchController.searchBar.delegate = self
                self.present(searchController, animated: true, completion: nil)
                
                selectedIndex = 1
                
                
            }else if(indexPath.row == 1){
                selectedIndex = 2
                let searchController = UISearchController(searchResultsController: searchResultController)
                searchController.searchBar.delegate = self
                self.present(searchController, animated: true, completion: nil)
            }
        }
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        let placeClient = GMSPlacesClient()
        
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil)  {(results, error: Error?) -> Void in
            // NSError myerr = Error;
            print("Error @%",Error.self)
            
            self.resultsArray.removeAll()
            if results == nil {
                return
            }
            
            for result in results! {
                if let result = result as? GMSAutocompletePrediction {
                    self.resultsArray.append(result.attributedFullText.string)
                }
            }
            self.searchResultController.reloadDataWithArray(self.resultsArray)
            
        }
        //        self.resultsArray.removeAll()
        //        gmsFetcher?.sourceTextHasChanged(searchText)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 116
            } else {
                return 0
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                return UITableViewAutomaticDimension
            } else if indexPath.row == 1 {
                return UITableViewAutomaticDimension
            } else {
                return 44
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                return 44
            } else {
                return 0
            }
        } else if indexPath.section == 3 {
            if indexPath.row == 0 {
                return 72
            } else {
                return 0
            }
        } else if indexPath.section == 4{
            if indexPath.row == 0 {
                return 51
            } else {
                return 0
            }
        }else if indexPath.section == 8 {
            if indexPath.row == 0 {
                return 44
            } else {
                return 0
            }
        } else {
            if indexPath.row == 0 {
                return 78
            } else {
                return 0
            }
        }
    }
   
    func LogOut() {
        
        let networkRechability = urlservices.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId(),
                              "device":"IOS"] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print("log out parameters is \(parameters)")
            urlservices.serviceCallPostMethodWithParams(url:logOut, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        let result = response["response"] as! String
                        print(result)
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        
                        popOverVC.fromScreen = ""
                        //                    popOverVC.ImageStr = ""
                        popOverVC.headingMesg = "Alert"
                        popOverVC.subMesg = "You are Logout Successfully"
                        
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
//                        self.themes.showAlert(title: "Alert", message: "You are Logout Successfully" , sender: self)
                        
                        let appdelegate = UIApplication.shared.delegate as? AppDelegate
                        appdelegate?.disconnect()
                        
                        self.themes.ClearUserInfo()
                        //                    let domain = Bundle.main.bundleIdentifier!
                        //                    UserDefaults.standard.removePersistentDomain(forName: domain)
                        //                    UserDefaults.standard.synchronize()
                        
                        // print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
                        let login = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC")as! LoginVC
                        self.navigationController?.pushViewController(login, animated: true)
                    }
                    else{
                        
                        let result = response["response"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    @objc func profileEditBtnClicked(button : UIButton) {
        
        let editVc = storyboard?.instantiateViewController(withIdentifier: "EditAccountVC") as! EditAccountVC
        
        self.navigationController?.pushViewController(editVc, animated: true)
        
    }

}
