//
//  SignOutTableViewCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 01/11/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class SignOutTableViewCell: UITableViewCell {

    @IBOutlet weak var btnNameLbl: UILabel!
    
    @IBOutlet weak var signOutBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
