//
//  FavorHeaderTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 01/11/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class FavorHeaderTableCell: UITableViewCell {
    
    
    
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    @IBOutlet weak var headerBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
