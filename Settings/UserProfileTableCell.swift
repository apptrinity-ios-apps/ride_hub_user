//
//  UserProfileTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 31/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class UserProfileTableCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var NumberLabel: UILabel!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var emailVeriftBtn: UIButton!
    
    @IBOutlet weak var profileEditBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
