//
//  ShareTripCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 23/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class ShareTripCell: UITableViewCell {
    
    
    @IBOutlet weak var dotLine1: UIView!
    
    @IBOutlet weak var dotLine2: UIView!
    
    @IBOutlet weak var btnBackView: UIView!
    
    @IBOutlet weak var laterBtn: UIButton!
    
    @IBOutlet weak var nextBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.nextBtn.layer.masksToBounds = true
        nextBtn.roundedButton()

        btnBackView.layer.cornerRadius = btnBackView.frame.size.height/2
                
        drawDottedLine(start: CGPoint(x: dotLine1.bounds.minX, y: dotLine1.bounds.maxY), end: CGPoint(x: dotLine1.bounds.maxX, y: dotLine1.bounds.minY), view: dotLine1)
        drawDottedLine(start: CGPoint(x: dotLine2.bounds.minX, y: dotLine2.bounds.maxY), end: CGPoint(x: dotLine2.bounds.maxX, y: dotLine2.bounds.minY), view: dotLine2)
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
  
}
extension UIButton{
    func roundedButton(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.bottomRight , .topRight , .bottomLeft],
                                     cornerRadii: CGSize(width: 20, height: 50))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}

