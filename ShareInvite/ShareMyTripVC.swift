//
//  ShareMyTripVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 23/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class ShareMyTripVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var dotLine: UIView!
    @IBOutlet weak var shareMyTripTableView: UITableView!
    
    

    @IBAction func closeAction(_ sender: Any) {
        
       self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shareMyTripTableView.delegate = self
        shareMyTripTableView.dataSource = self
        
        self.shareMyTripTableView.register(UINib(nibName: "ShareTripCell", bundle: nil), forCellReuseIdentifier: "ShareTripCell")
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShareTripCell", for: indexPath) as! ShareTripCell
        
        cell.laterBtn.addTarget(self, action: #selector(laterBtnClicked(button:)), for: .touchUpInside)
        cell.nextBtn.addTarget(self, action: #selector(nextBtnClicked(button:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 652
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    @objc func laterBtnClicked(button : UIButton) {
//        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePaymentVC") as! ChangePaymentVC
//        
//        self.navigationController?.pushViewController(Vc, animated: true)
        
    }
    
    @objc func nextBtnClicked(button : UIButton) {
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SaftyVC") as! SaftyVC
        
        self.navigationController?.pushViewController(Vc, animated: true)
        
    }

}
