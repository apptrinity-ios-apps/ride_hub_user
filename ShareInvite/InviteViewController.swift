//
//  InviteViewController.swift
//  RideHub
//
//  Created by INDOBYTES on 22/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class InviteViewController: UIViewController {
    
    @IBOutlet weak var inviteCodeField: UITextField!
    @IBOutlet weak var contentlabel: UILabel!
    
    @IBOutlet weak var dotLineView: UIView!
    @IBOutlet weak var inviteFriendsBtn: UIButton!
    
    @IBAction func closeAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func inviteWorkAction(_ sender: Any) {
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "FreeRidesVC") as! FreeRidesVC
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    @IBAction func inviteAction(_ sender: Any) {
        let text = "I have an RideHub coupon worth $ \(referMoney) for you.Sign up with my code \(referCode) to ride free.Enjoy! https://itunes.apple.com/us/app/ridehubuser/id1361522193"
        // set up activity view controller
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
  
    }
    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var referCode = String()
    var referMoney = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        inviteFriendsBtn.layer.cornerRadius = inviteFriendsBtn.frame.size.height/2
        
        inviteCodeField.layer.cornerRadius = inviteCodeField.frame.size.height/2
        inviteCodeField.layer.borderWidth = 1
        self.inviteCodeField.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        
        inviteCodeField.attributedPlaceholder = NSAttributedString(string: "jhonsome3245",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(34/255.0), green: CGFloat(43/255.0), blue: CGFloat(69/255.0), alpha: CGFloat(1.0) )])
        
        drawDottedLine(start: CGPoint(x: dotLineView.bounds.minX, y: dotLineView.bounds.maxY), end: CGPoint(x: dotLineView.bounds.maxX, y: dotLineView.bounds.minY), view: dotLineView)

        // Do any additional setup after loading the view.
        
        inviteCodeApi()
    }
    
    func inviteCodeApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = [ "user_id":themes.getUserId()] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:inviteCode, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    if(success == "1"){
                        print("response is \(response)")
                        let response = response["response"] as? [String:AnyObject]
                        let details = response!["details"] as? [String:AnyObject]
                        
                        self.inviteCodeField.text = details!["referral_code"] as? String
                        self.referCode = (details!["referral_code"] as? String)!
                        self.referMoney = (details!["your_earn_amount"] as? Int)!
                        
                        
                        
                        let main_string = "Share the Ride Hub love and give friends free rides to try Ride worth up to $ \(self.referMoney) each!"
                        let string_to_color = "$ \(self.referMoney)"
                        
                        let range = (main_string as NSString).range(of: string_to_color)
                        
                        let attribute = NSMutableAttributedString.init(string: main_string)
                        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red , range: range)
                        
                        
                        //self.contentLabel = UITextField.init(frame:CGRect(x:10 , y:20 ,width:100 , height:100))
                        self.contentlabel.attributedText = attribute
                        self.contentlabel.font = self.contentlabel.font?.withSize(15)
                        
                    }
                    else{
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [7, 3] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    

}
