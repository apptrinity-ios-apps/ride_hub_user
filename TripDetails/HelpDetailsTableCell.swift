//
//  HelpDetailsTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 29/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class HelpDetailsTableCell: UITableViewCell {

    
    @IBOutlet weak var helpLabel: UILabel!
    
    @IBOutlet weak var helpRightArrow: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
       
        
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
