//
//  ReceiptDetailsTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 29/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class ReceiptDetailsTableCell: UITableViewCell {

    
    @IBOutlet weak var receiptLabel: UILabel!
    
    @IBOutlet weak var receiptAmountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
