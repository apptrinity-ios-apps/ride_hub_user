//
//  HelpSubViewController.swift
//  RideHub
//
//  Created by INDOBYTES on 13/12/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class HelpSubViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var helpTableView: UITableView!
    var question = String()
    var categoryId = String()
    let themes = Themes()
    var helpSubCategoryList = Array<AnyObject>()
    override func viewDidLoad() {
        super.viewDidLoad()
     questionLabel.text = question
        // Do any additional setup after loading the view.
        self.helpTableView.register(UINib(nibName: "HelpDetailsTableCell", bundle: nil), forCellReuseIdentifier: "HelpDetailsTableCell")
        
       helpSubListApi()
        helpTableView.separatorColor = UIColor.clear

    }
    func helpSubListApi() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
            let parameters = ["category_id":categoryId] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:helpSubCategory, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    //  let response =  response["response"] as AnyObject
                    self.helpSubCategoryList = response["data"] as! [AnyObject]
                    
                    
                    self.helpTableView.reloadData()
                    
                    
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.helpSubCategoryList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpDetailsTableCell", for: indexPath) as! HelpDetailsTableCell
        let obj = self.helpSubCategoryList[indexPath.row]
        cell.helpLabel.text = (obj["sub_category"] as! String)
        cell.selectionStyle = .none
        return cell

    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let helpSubViewController = self.storyboard?.instantiateViewController(withIdentifier: "HelpSubmitViewController") as! HelpSubmitViewController
        let obj = self.helpSubCategoryList[indexPath.row]
        helpSubViewController.question = (obj["sub_category"] as! String)
        helpSubViewController.category_id1 = obj["category"] as! String
        helpSubViewController.subCategory_id1 = obj["sub_category_id"] as! String
        self.navigationController?.pushViewController(helpSubViewController, animated: true)
    }
    @IBAction func closeAction(_ sender: Any) {
        
        
        self.navigationController?.popViewController(animated: true)
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
