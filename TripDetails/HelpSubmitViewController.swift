//
//  HelpSubmitViewController.swift
//  RideHub
//
//  Created by INDOBYTES on 13/12/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit
class HelpSubmitViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var category_id1 = String()
    var subCategory_id1 = String()
    var themes = Themes()
    var helpQuestionListArray = Array<AnyObject>()
   var question = String()
    @IBOutlet weak var questionlabel: UILabel!
    @IBOutlet weak var helpSubmitTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
   helpSubListApi()
        // Do any additional setup after loading the view.
        questionlabel.text = question
        self.helpSubmitTableView.register(UINib(nibName: "HelpSubmitTableViewCell", bundle: nil), forCellReuseIdentifier: "HelpSubmitTableViewCell")
        helpSubmitTableView.separatorColor = UIColor.clear
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpQuestionListArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HelpSubmitTableViewCell", for: indexPath) as! HelpSubmitTableViewCell
        let obj = self.helpQuestionListArray[indexPath.row]
        cell.questionLabel.text = (obj["question"] as! String)
        cell.answerLabel.text = (obj["answer"] as! String)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func helpSubListApi() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
            let parameters = ["category_id":category_id1,"sub_category_id":subCategory_id1] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:helpQuestionList, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    //  let response =  response["response"] as AnyObject
                    self.helpQuestionListArray = response["data"] as! [AnyObject]
                    
                    
                    self.helpSubmitTableView.reloadData()
                    
                    
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
