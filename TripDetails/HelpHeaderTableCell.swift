//
//  HelpHeaderTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 29/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class HelpHeaderTableCell: UITableViewCell {
    
    
    @IBOutlet weak var helpBtn: UIButton!
    
    @IBOutlet weak var helpView: UIView!
    
    
    @IBOutlet weak var receiptBtn: UIButton!
    
    @IBOutlet weak var receiptView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
