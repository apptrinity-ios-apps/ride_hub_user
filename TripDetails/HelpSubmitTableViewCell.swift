//
//  HelpSubmitTableViewCell.swift
//  RideHub
//
//  Created by INDOBYTES on 13/12/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class HelpSubmitTableViewCell: UITableViewCell {

    @IBOutlet weak var answerLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
