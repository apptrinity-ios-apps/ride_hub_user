//
//  RatingEditCollectionCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 29/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class RatingEditCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var starRatingBtn: UIButton!
    
    @IBOutlet weak var editRatingBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shadowView(view: backView)
        // Initialization code
    }
    
    func shadowView(view:UIView){
        view.layer.cornerRadius = 15
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }

}
