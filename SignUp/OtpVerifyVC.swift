//
//  OtpVerifyVC.swift
//  RideHubUser-Swift
//
//  Created by INDOBYTES on 04/02/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit
class OtpVerifyVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var numberTextfield: UITextField!
    @IBOutlet weak var secondView: UIView!
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var otpTextfield: UITextField!
    var otp = String()
    var urlService = URLservices.sharedInstance
    var themes = Themes()
    var mobileNumber = String()
    var name = String()
    var email = String()
    var password = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        numberTextfield.text = mobileNumber
        
       
        
        numberTextfield.attributedPlaceholder = NSAttributedString(string: "Mobile Number",
                                                             attributes: [NSAttributedStringKey.foregroundColor: textFieldColor])
        numberTextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: numberTextfield.frame.height))
        firstView.layer.cornerRadius = firstView.frame.height/2
        numberTextfield.leftViewMode = .always
        //For Right side padding
        numberTextfield.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: numberTextfield.frame.height))
        numberTextfield.rightViewMode = .always
        
        otpTextfield.attributedPlaceholder = NSAttributedString(string: "Enter Otp",
                                                             attributes: [NSAttributedStringKey.foregroundColor: textFieldColor])
         secondView.layer.cornerRadius = secondView.frame.height/2
        otpTextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: otpTextfield.frame.height))
        otpTextfield.leftViewMode = .always
        //For Right side padding
        otpTextfield.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: otpTextfield.frame.height))
        otpTextfield.rightViewMode = .always
    }

    @IBAction func confirmAction(_ sender: Any) {
        
        if(otpTextfield.text == otp){
            verifyOtp()
        }else{
             self.themes.showAlert(title: "Alert ☹️", message: "Please enter valid otp", sender: self)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {    self.navigationController?.popViewController(animated: true)
    }
    
    func verifyOtp(){
        
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static

            let parameters = [ "user_name":name ,"email":email , "password":password ,"country_code":"+91","phone_number":mobileNumber,"referal_code":"","deviceToken":themes.getDeviceToken() ,"gcm_id":""] as [String : Any]
            
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:OTP, params: parameters as Dictionary<String, Any>) { response in
                print(response)
 
                let success = response["status"] as! String
                if(success == "1"){
                    
                    self.themes.hideActivityIndicator(uiView: self.view)

                  let user_id = response["user_id"] as! String
                       let UserImage = response["user_image"] as! String
                       let CategoryString = response["category"] as! String
                       // let SubCategoryString = response["subcategory"] as! String
                       let userEmail = response["email"] as! String
                       let UserName = response["user_name"] as! String
                       let UserPhoneNumber = response["phone_number"] as! String
                       let AmountWallet = response["wallet_amount"] as! String
                       let currency = response["currency"] as! String
                       let countryCode = response["country_code"] as! String
                     let sec_key = response["sec_key"] as! String
                    self.themes.saveXmppUserCredentials(sec_key)
                    self.themes.saveUserID(user_id)
                    self.themes.saveuserDP(UserImage)
                     self.themes.saveCategoryString(CategoryString)
                   // self.themes.saveSubCategoryString(SubCategoryString)
                    self.themes.saveuserEmail(userEmail)
                    self.themes.saveUserName(UserName)
                    self.themes.saveMobileNumber(UserPhoneNumber)
                    self.themes.saveCurrency(currency)
                    self.themes.saveWallet(AmountWallet)
                    self.themes.saveCountryCode(countryCode)
                    
                   let appDelagate = UIApplication.shared.delegate as? AppDelegate
                    appDelagate?.connectToXmpp()

                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(vc, animated: true)
  
                }
                else{
                    
                    let result = response["message"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
