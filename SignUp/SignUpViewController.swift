//  SignUpViewController.swift
//  RideHubUser-Swift
//  Created by S s Vali on 1/31/19.
//  Copyright © 2019 Indobytes. All rights reserved.
import UIKit
import CountryPicker
class SignUpViewController: UIViewController,UITextFieldDelegate,CountryPickerDelegate{
    @IBOutlet var referalTextField: UITextField!
    @IBOutlet weak var numberCodeView: UIView!
    @IBOutlet weak var checkInImageview: UIImageView!
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var mailField: UITextField!
    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var referalCodeView: UIView!
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var picker: CountryPicker!
    
    @IBOutlet weak var eyeBtn: UIButton!
    @IBOutlet weak var eyeImage: UIImageView!
    
    
    var eyeIconClick = true
    var checkHint = Bool()
    var urlService = URLservices.sharedInstance
    var themes = Themes()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameView.layer.cornerRadius = nameView.frame.height/2
//        self.nameView.alpha = 0.25
        self.emailView.layer.cornerRadius = nameView.frame.height/2
//        self.emailView.alpha = 0.25
        self.numberView.layer.cornerRadius = nameView.frame.height/2
//        self.numberView.alpha = 0.25
        self.referalCodeView.layer.cornerRadius = referalCodeView.frame.height/2
//        self.referalCodeView.alpha = 0.25
        self.passwordView.layer.cornerRadius = nameView.frame.height/2
         self.checkInImageview.layer.cornerRadius = checkInImageview.frame.height/2
        self.numberCodeView.layer.cornerRadius = 15
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        //init Picker
//        picker.displayOnlyCountriesWithCodes = ["DK", "SE", "NO", "DE"] //display only
//        picker.exeptCountriesWithCodes = ["RU"] //exept country
//        let theme = CountryViewTheme(countryCodeTextColor: .white, countryNameTextColor: .white, rowBackgroundColor: .black, showFlagsBorder: false)        //optional for UIPickerView theme changes
//        picker.theme = theme //optional for UIPickerView theme changes
//        picker.countryPickerDelegate = self
//        picker.showPhoneNumbers = true
//        picker.setCountry(code!)

//        self.passwordView.alpha = 0.25
        self.nameField.delegate = self
        self.mailField.delegate = self
        self.numberField.delegate = self
        self.passwordField.delegate = self
        
//        nameField.layer.borderWidth = 1
//        nameField.layer.borderColor = UIColor.lightGray.cgColor
     //   self.themes.shadowForView(view: nameView)
        self.nameField.layer.cornerRadius = nameView.frame.height/2
//        self.nameField.backgroundColor = UIColor.clear
        nameField.attributedPlaceholder = NSAttributedString(string: "Full Name",
        attributes: [NSAttributedStringKey.foregroundColor: textFieldColor])
        nameField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: nameField.frame.height))
        nameField.leftViewMode = .always
        //For Right side padding
        nameField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: nameField.frame.height))
        nameField.rightViewMode = .always
        self.mailField.layer.cornerRadius = 20

        mailField.attributedPlaceholder = NSAttributedString(string: "Email ID",
        attributes: [NSAttributedStringKey.foregroundColor: textFieldColor])
        mailField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: mailField.frame.height))
        mailField.leftViewMode = .always
        //For Right side padding
        mailField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: mailField.frame.height))
        mailField.rightViewMode = .always
        self.numberField.layer.cornerRadius = 20
//        self.numberField.backgroundColor = UIColor.clear
//        numberField.layer.borderWidth = 1
//        numberField.layer.borderColor = UIColor.lightGray.cgColor
    //    self.themes.shadowForView(view: numberView)
        numberField.attributedPlaceholder = NSAttributedString(string: "Mobile Number",
        attributes: [NSAttributedStringKey.foregroundColor: textFieldColor])
        numberField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: numberField.frame.height))
        numberField.leftViewMode = .always
        //For Right side padding
        numberField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: numberField.frame.height))
        numberField.rightViewMode = .always
        self.passwordField.layer.cornerRadius = 20
//        self.passwordField.backgroundColor = UIColor.clear
//        passwordField.layer.borderWidth = 1
//        passwordField.layer.borderColor = UIColor.lightGray.cgColor
        //self.themes.shadowForView(view: passwordView)
        passwordField.attributedPlaceholder = NSAttributedString(string: "Password",
        attributes: [NSAttributedStringKey.foregroundColor: textFieldColor])
        passwordField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: passwordField.frame.height))
        passwordField.leftViewMode = .always
        //For Right side padding
        passwordField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: passwordField.frame.height))
        passwordField.rightViewMode = .always
        
        self.referalTextField.layer.cornerRadius = 20
        referalTextField.attributedPlaceholder = NSAttributedString(string: "Referral Code",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: textFieldColor])
        referalTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: passwordField.frame.height))
        referalTextField.leftViewMode = .always
        //For Right side padding
        referalTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: passwordField.frame.height))
        referalTextField.rightViewMode = .always
        
        
        
        self.signUpButton.layer.cornerRadius = 20
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SignUpViewController.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tap)
        
        
        // Do any additional setup after loading the view.
    }
    
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //pick up anythink
        print(phoneCode) 
    }
     override func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            //
            self.view.frame.origin.y = 150
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y -= keyboardHeight
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func eyeBtnAction(_ sender: Any) {
        if(eyeIconClick == true) {
            passwordField.isSecureTextEntry = false
            eyeImage.image = UIImage(named: "eyeOpen")
        } else {
            passwordField.isSecureTextEntry = true
            eyeImage.image = UIImage(named: "eyeClosed")
        }
        
        eyeIconClick = !eyeIconClick
        
        
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    @IBAction func checkMarkAction(_ sender: Any) {
        if(checkHint == false){
            checkInImageview.image = UIImage(named: "radioSelect")
            checkHint = true
        }else{
            checkInImageview.image = UIImage(named: "radioUnselect")
            checkHint = false
        }
    }
    func signUP(){

        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            let parameters = [ "user_name":nameField.text ?? "","email":mailField.text ?? "", "password":passwordField.text ?? "","country_code":"+91","phone_number":numberField.text ?? "","referal_code":"" ?? "","deviceToken":themes.getDeviceToken() ,"gcm_id":""] as [String : Any]
            
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:appRegister, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    let otp = response["otp"] as! String
                    let phone_number = response["phone_number"] as! String
                    let email = response["email"] as! String

                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "OtpVerifyVC") as! OtpVerifyVC
                    vc.otp = otp
                    vc.name = self.nameField.text!
                    vc.mobileNumber = phone_number
                    vc.password = self.passwordField.text!
                    vc.email = email
                    self.navigationController?.pushViewController(vc, animated: true)

                }
                else{
                    
                    let result = response["message"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @IBAction func signUpAction(_ sender: Any) {
        if(ValidatedTextField()){
            
            if(checkHint){
                signUP()
            }else{
                self.themes.showAlert(title: "Alert", message: "Please accept Terms and Conditions", sender: self)
            }
        }
    }
    func ValidatedTextField()-> Bool {
        if (nameField.text == ""){
            self.themes.showAlert(title: "Alert", message: "Name is Mandatory", sender: self)
            self.nameField.becomeFirstResponder()
            return false
        }
        else if (mailField.text == "") {
            self.themes.showAlert(title: "Alert", message: "Email is Mandatory", sender: self)
            self.mailField.becomeFirstResponder()
            return false
        }
        else if (numberField.text == "") {
            self.themes.showAlert(title: "Alert", message: "Mobile Number is Mandatory", sender: self)
            self.numberField.becomeFirstResponder()
            return false
        }
        else if (passwordField.text == "") {
            self.themes.showAlert(title: "Alert", message: "Password is Mandatory", sender: self)
            self.passwordField.becomeFirstResponder()
            return false
        }
        return true
    }
    @IBAction func countryCodeAction(_ sender: Any) {
        
    }
    @IBAction func signInAction(_ sender: UIButton) {
         self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
