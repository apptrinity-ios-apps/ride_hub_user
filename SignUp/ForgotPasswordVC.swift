//
//  ForgotPasswordVC.swift
//  RideHub
//
//  Created by INDOBYTES on 21/11/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    var urlService = URLservices.sharedInstance
    var themes = Themes()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
         self.emailView.layer.cornerRadius = emailView.frame.height/2
        
        
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Enter your email",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: textFieldColor])
        emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: emailTextField.frame.height))
        emailTextField.leftViewMode = .always
        //For Right side padding
        emailTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: emailTextField.frame.height))
        emailTextField.rightViewMode = .always
  
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if(!(isValidEmail(testStr: emailTextField.text!))){
            
            themes.showAlert(title: "Alert", message: "Please Enter Valid Email", sender: self)
            
            //textField.resignFirstResponder()
            //emailTFT.becomeFirstResponder()
        }
        else{
             textField.resignFirstResponder()
           // emailTextField.becomeFirstResponder()
        }
        
       
        return false
    }
    
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    @IBAction func confirmAction(_ sender: Any) {
        
        if(emailTextField.text == ""){
            themes.showAlert(title: "Alert", message: "Please enter email.", sender: self)
        }else{
            
            if(!(isValidEmail(testStr: emailTextField.text!))){
                
                themes.showAlert(title: "Alert", message: "Please Enter Valid Email", sender: self)
                
               // textField.resignFirstResponder()
                //emailTFT.becomeFirstResponder()
            }
            else{
                
                forGotPasswordApi()
            }
            
           
        }
        
        
    }
    func forGotPasswordApi(){
        
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            let parameters = ["email":emailTextField.text!] as [String : Any]
            
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:forGotPassword, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    if(success == "1"){
                       self.themes.showAlert(title: "Message", message: "Your password is sent to Mobile Number.", sender: self)
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                       
                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    }
                    else{
                        
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
