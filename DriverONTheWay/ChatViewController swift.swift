//  ChatViewController.swift
//  RideHubUser-Swift
//  Created by INDOBYTES on 09/04/19.
//  Copyright © 2019 Indobytes. All rights reserved.
import UIKit
import WebKit
class ChatViewController: UIViewController,WKNavigationDelegate,UIScrollViewDelegate{
    let themes = Themes()
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var webV: WKWebView!
   // var webV = WKWebView()
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let userName = themes.getUserName()
        let userId = themes.getUserId()
        let userimage = themes.getUserDp()
        let userMail = themes.getuserMail()
        let driverId = themes.getDriverId()
        let driverImage = themes.getDriverImage()
        let driverName = themes.getDriverName()
        nameLbl.text = themes.getDriverName()
        let js = "<script>\n" +

            "(function(t,a,l,k,j,s){\n" +
            "s=a.createElement('script');s.async=1;s.src=\"https://cdn.talkjs.com/talk.js\";a.head.appendChild(s)\n" +
            ";k=t.Promise;t.Talk={v:1,ready:{then:function(f){if(k)return new k(function(r,e){l.push([f,r,e])});l\n" +
            ".push([f])},catch:function(){return k&&new k()},c:l}};})(window,document,[]);\n" +
            "</script>\n" +
            "\n" +
            "<!-- container element in which TalkJS will display a chat UI -->\n" +
            "<div id=\"talkjs-container\" style=\"width: 100%; margin: auto; height: 100%\"><i>Naveen chat...</i></div>\n" +
            "\n" +
            "<!-- TalkJS initialization code, which we'll customize in the next steps -->\n" +
            "<script>\n" +
            "Talk.ready.then(function() {\n" +
            "    var me = new Talk.User({\n" +
            "        id: \"\(userId)\",\n" +
            "        name: \"\(userName)\",\n" +
            "        role: \"user\",\n" +
            "        email: \"\(userMail)\",\n" +
            "        photoUrl: \"https://demo.talkjs.com/img/sebastian.jpg\",\n" +
            "        welcomeMessage: \"Hey, how can I help?\"\n" +
            "    });\n" +
            "    window.talkSession = new Talk.Session({\n" +
            "        appId: \"M5iybBK9\",\n" +
            "        me: me\n" +
            "    });\n" +
            "    var other = new Talk.User({\n" +
            "        id: \"\(driverId)\",\n" +
            "        name: \"\(driverName)\",\n" +
            "        role: \"user\",\n" +
            "        email: \"alice@example.com\",\n" +
            "        photoUrl: \"\(driverImage)\",\n" +
            "        welcomeMessage: \"Hey there! How are you? :-)\"\n" +
            "    });\n" +
            "\n" +
            "    var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other))\n" +
            "    conversation.setParticipant(me);\n" +
            "    conversation.setParticipant(other);\n" +
            "    var inbox = talkSession.createInbox({selected: conversation});\n" +
            "    inbox.mount(document.getElementById(\"talkjs-container\"));\n" +
            "});\n" +
        "</script>\n"
       
  
      
        loadHTMLContent(js)


        
     
        // Do any additional setup after loading the view.
    }
    
    func loadHTMLContent(_ htmlContent: String) {
        let htmlStart = "<HTML><HEAD><meta name=\"viewport\" content=\"width=device-width, maximum-scale=1.0,user-scalable=no\"></HEAD><BODY>"
        let htmlEnd = "</BODY></HTML>"
        let htmlString = "\(htmlStart)\(htmlContent)\(htmlEnd)"
        webV.loadHTMLString(htmlString, baseURL: Bundle.main.bundleURL)
        
    }
    
   
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {

      //  webView.frame.size.height = 1
        webView.frame.size = webView.scrollView.contentSize
      
//        webView.frame.size.height = 1
//        webView.frame.size = webView.scrollView.contentSize
//
//
//
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
