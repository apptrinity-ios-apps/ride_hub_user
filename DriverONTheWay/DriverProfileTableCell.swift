//
//  DriverProfileTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class DriverProfileTableCell: UITableViewCell {
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var feedBackHeight: NSLayoutConstraint!
    @IBOutlet weak var tripwasHeight: NSLayoutConstraint!
    @IBOutlet weak var driverImgView: UIImageView!
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var vehNumberLbl: UILabel!
    @IBOutlet weak var vehModelLbl: UILabel!
    @IBOutlet weak var vehTypeLbl: UILabel!
    @IBOutlet weak var vehImgVIew: UIImageView!    
    @IBOutlet weak var dotLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       driverImgView.layer.cornerRadius = driverImgView.frame.height/2
        driverImgView.clipsToBounds = true
       vehTypeLbl.layer.cornerRadius = vehTypeLbl.frame.size.height/2
        vehTypeLbl.layer.masksToBounds = true
        
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
//        shapeLayer.strokeColor = (UIColor.init(red: 6, green: 152, blue: 212, alpha: 1) as! CGColor)
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
//        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.lineWidth = 1

        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
}
