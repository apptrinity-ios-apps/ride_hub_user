//
//  PopUpSafetyTableCell.swift
//  RideHub
//
//  Created by INDOBYTES on 29/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class PopUpSafetyTableCell: UITableViewCell {
    
    @IBOutlet var safetyImage: UIImageView!
    
    @IBOutlet var safetyDetailLbl: UILabel!
    
    @IBOutlet var safetyHeadLbl: UILabel!
    
    @IBOutlet var backView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        shadowView(view: backView)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func shadowView(view:UIView){
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }
    
}
