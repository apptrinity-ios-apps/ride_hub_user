//
//  DriverOnWayVc.swift
//  NewRideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import Polyline
import AVFoundation
class DriverOnWayVc: UIViewController,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate {
    @IBOutlet var popUpViewHeight: NSLayoutConstraint!
    @IBOutlet var popUpHeadingHeight: NSLayoutConstraint!
    @IBOutlet var popUpView: UIView!
    @IBOutlet var popUpTableView: UITableView!
    @IBAction func popCloseAction(_ sender: Any) {
        driverOnWayView.isHidden = false
        self.popUpView.isHidden = true
    }
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var headingLabel: UILabel!
    @IBOutlet var dragView: UIView!
    @IBOutlet var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var driverOnWayView: UIView!
    @IBOutlet weak var driverOnWayTableView: UITableView!
    @IBOutlet var statusHeight: NSLayoutConstraint!
    @IBOutlet var driverTimeLabel: UILabel!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var leftBtn: UIButton!
    @IBAction func closeAction(_ sender: Any) {
        driverOnWayTableView.setContentOffset(.zero, animated: true)
        statusHeight.constant = 50
        statusLabel.isHidden = false
        driverTimeLabel.isHidden = false
        driverOnWayTableView.isScrollEnabled = false
        tableHeight.constant = 313
        downHint = true
        blueView.isHidden = true
        headingLabel.isHidden = true
        closeBtn.isHidden = true
    UIView.animate(withDuration: 0.5, animations: {
    self.view.layoutIfNeeded()
    })
    }
    @IBAction func leftAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    var estimatedPickUpTime = String()
    var estimatedDropTime = String()
    var driverRating = String()
    var catagoryType = String()
    var driverLoactionHint = Bool()
     var Record_Driver = DriverRecord()
    var CheckStatus_Notify = Bool()
    var trackDetail = [String : AnyObject]()
    var rideStatus = String()
    var vehicleFare = String()
    @IBOutlet var closeBtn: UIButton!
    var pickUpPrimary = String()
    var dropPrimary = String()
     var wayPointLatLon = Array<AnyObject>()
    var OldLocationNext = CLLocationCoordinate2D()
    var pickUpAddress = String()
    var dropAddress = String()
    var oldLocation = CLLocationCoordinate2D()
     var stepsCoords:[CLLocationCoordinate2D] = []
    var themes = Themes()
    var defaultPolyline = GMSPolyline()
     var pathDrawn = GMSMutablePath()
    var downHint = Bool()
    var driverMarker = GMSMarker()
     var pickUpMarker = GMSMarker()
     var dropMarker = GMSMarker()
    var driverPolyline = GMSPolyline()
    var driverName = String()
    var carName = String()
    var carNumber = String()
    var driverImage = String()
    var driverLatitude = Double()
    var driverLongitude = Double()
    var mobileNumber = String()
    var rideID = String()
    var eta = String()
    var buttonTag = ""
    var EditImageArray = ["cross","plus_icon","edit","edit"]
    var EditArray = ["Cancel Ride","Add Stop","Edit Drop-off","Edit Pickup"]
    var safetyImageArray = ["safety_icon","share_icon","911_icon"]
    var safetyArray = ["Safety Center","Share My Trip","911 Assistance"]

    var pickUpLatitude = Double()
    var pickUpLongitude = Double()
    var dropLatitude = Double()
    var dropLongitude = Double()
    var rideCompleteHint = Bool()
    
  //  func wasDragged(_ sender: UITapGestureRecognizer) {
   
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let tapOnScreen: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "CheckTheTime")
//        tapOnScreen.cancelsTouchesInView = false
//        view.addGestureRecognizer(tapOnScreen)
        
           //gestureRecognizer.cancelsTouchesInView = false
       estimatedDropTime = "-"
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(wasDragged(gestureRecognizer:)));       driverOnWayView.addGestureRecognizer(gesture)
        gesture.delegate = self
        
        
        self.popUpView.isHidden = true
        headingLabel.text = "WE FOUND YOU A DRIVER"
        driverTimeLabel.text = "Driver will pickup you in \(eta)"
        
        blueView.isHidden = true
        headingLabel.isHidden = true
        closeBtn.isHidden = true
        driverOnWayTableView.separatorColor = UIColor.clear
        driverOnWayView.layer.cornerRadius = 15
//        driverOnWayTableView.layer.cornerRadius = 15
        driverOnWayTableView.delegate = self
        driverOnWayTableView.dataSource = self
        
        dragView.layer.cornerRadius = dragView.frame.height / 2
      //  driverOnWayTableView.separatorColor = .none
//
           driverOnWayTableView.layer.masksToBounds = true
//        driverOnWayView.layer.shadowColor = UIColor.black.cgColor
//        driverOnWayView.layer.shadowOpacity = 0.5
//        driverOnWayView.layer.shadowOffset = CGSize(width: -1, height: 1)
//        driverOnWayView.layer.shadowRadius = 1
//        driverOnWayView.layer.shouldRasterize = true
        //driverOnWayView.animShow()
        
        shadowView(view: driverOnWayView)
       // shadowView(view: driverOnWayTableView)
        let driverPosition = CLLocationCoordinate2DMake(driverLatitude, driverLongitude)
        // let markerImage = UIImage(named: "Mapcar")!.withRenderingMode(.alwaysTemplate)
        //creating a marker view
        driverMarker.appearAnimation = GMSMarkerAnimation.none
        
        
        
        let markerIcon = UIImage(named: "trackCar") /// 16th
        driverMarker.icon = markerIcon
         mapView.camera = GMSCameraPosition(target: driverPosition, zoom: 15, bearing: 0, viewingAngle: 0)
        
        //        let camera = GMSCameraPosition.camera(withLatitude: driverLatitude, longitude: driverLongitude, zoom: 13)
        //        self.mapView.camera = camera
        self.driverMarker.position = driverPosition
        self.driverMarker.map = self.mapView
        
        popUpTableView.delegate = self
        popUpTableView.dataSource = self
        popUpTableView.separatorColor = .clear
        shadowView(view: popUpView)
        self.driverOnWayTableView.register(UINib(nibName: "DriverProfileTableCell", bundle: nil), forCellReuseIdentifier: "DriverProfileTableCell")
        self.driverOnWayTableView.register(UINib(nibName: "DriverContactCell", bundle: nil), forCellReuseIdentifier: "DriverContactCell")
        self.driverOnWayTableView.register(UINib(nibName: "TripTableCell", bundle: nil), forCellReuseIdentifier: "TripTableCell")
        self.driverOnWayTableView.register(UINib(nibName: "FareCancelCell", bundle: nil), forCellReuseIdentifier: "FareCancelCell")
        self.popUpTableView.register(UINib(nibName: "PopUpEditTableCell", bundle: nil), forCellReuseIdentifier: "PopUpEditTableCell")
        self.popUpTableView.register(UINib(nibName: "PopUpSafetyTableCell", bundle: nil), forCellReuseIdentifier: "PopUpSafetyTableCell")
        if(CheckStatus_Notify == true){
            setDataToView()
            optionslistApi()
        }
    }
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        
        let screesize = UIScreen.main.bounds.height
        if (screesize == 568) {
            
            let translation = gestureRecognizer.translation(in: self.view)
            if gestureRecognizer.state == UIGestureRecognizer.State.began  {
                
                print(gestureRecognizer.view!.center.y)
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            if gestureRecognizer.state == UIGestureRecognizer.State.changed {
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    if (gestureRecognizer.view!.center.y >= 640){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 640)
                    }else if(gestureRecognizer.view!.center.y <= 150){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 150)
                        
                    }else{
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    }
                    
                }
                else {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 313)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            
        } else if (screesize == 667) {
            
            let translation = gestureRecognizer.translation(in: self.view)
            if gestureRecognizer.state == UIGestureRecognizer.State.began  {
                
                print(gestureRecognizer.view!.center.y)
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            if gestureRecognizer.state == UIGestureRecognizer.State.changed {
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    if (gestureRecognizer.view!.center.y >= 735){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 735)
                    }else if(gestureRecognizer.view!.center.y <= 250){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 250)
                        
                    }else{
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    }
                    
                }
                else {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 313)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            
        } else if (screesize == 736) {
            
            let translation = gestureRecognizer.translation(in: self.view)
            if gestureRecognizer.state == UIGestureRecognizer.State.began  {
                
                print(gestureRecognizer.view!.center.y)
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            if gestureRecognizer.state == UIGestureRecognizer.State.changed {
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    if (gestureRecognizer.view!.center.y >= 804){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 804)
                    }else if(gestureRecognizer.view!.center.y <= 318){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 318)
                        
                    }else{
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    }
                    
                }
                else {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 313)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            
        } else if (screesize == 812) {
            
            let translation = gestureRecognizer.translation(in: self.view)
            if gestureRecognizer.state == UIGestureRecognizer.State.began  {
                
                print(gestureRecognizer.view!.center.y)
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            if gestureRecognizer.state == UIGestureRecognizer.State.changed {
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    if (gestureRecognizer.view!.center.y >= 880){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 880)
                    }else if(gestureRecognizer.view!.center.y <= 394){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 394)
                        
                    }else{
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    }
                    
                }
                else {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 313)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            
        } else if (screesize == 896) {
            
            let translation = gestureRecognizer.translation(in: self.view)
            if gestureRecognizer.state == UIGestureRecognizer.State.began  {
                
                print(gestureRecognizer.view!.center.y)
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            if gestureRecognizer.state == UIGestureRecognizer.State.changed {
                if(gestureRecognizer.view!.center.y < 1000) {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    if (gestureRecognizer.view!.center.y >= 957){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 957)
                    }else if(gestureRecognizer.view!.center.y <= 460){
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 460)
                        
                    }else{
                        gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: gestureRecognizer.view!.center.y + translation.y)
                    }
                    
                }
                else {
                    gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x, y: 313)
                }
                gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
            }
            
            
        }
        
        
    }
    func setDataToView(){
        if(rideStatus == "Onride"){
            
            statusLabel.text = "YOUR TRIP HAS BEGUN"
            driverTimeLabel.text = "Estimated Time 25 mins"
            headingLabel.text = "YOUR TRIP HAS BEGUN"
            let cabName = trackDetail["cab_type"] as! String
            let vehicle_number = trackDetail["vehicle_number"] as! String
            rideID = trackDetail["ride_id"] as! String
           
            carName = cabName
            carNumber = vehicle_number
           // driverImage =  driverImage
            
            let pickUp = trackDetail["pickup"] as! [String : AnyObject]
            let drop = trackDetail["drop"] as! [String : AnyObject]
            let picklatLong = pickUp["latlong"] as! [String : AnyObject]
            let droplatLong = drop["latlong"] as! [String : AnyObject]
            let pickUpLat = picklatLong["lat"] as! NSNumber
            let pickUpLon = picklatLong["lon"] as! NSNumber
            let dropLat = droplatLong["lat"] as! NSNumber
            let dropLon = droplatLong["lon"] as! NSNumber
            pickUpLatitude = Double(truncating: pickUpLat)
            pickUpLongitude = Double(truncating: pickUpLon)
            dropLatitude = Double(truncating: dropLat)
            dropLongitude = Double(truncating: dropLon)
            getAddressFromLatLon(pdblLatitude: String(pickUpLat.stringValue), withLongitude: String(pickUpLon.stringValue), hint: "PICK UP")
            getAddressFromLatLon(pdblLatitude: String(dropLat.stringValue), withLongitude: String(dropLon.stringValue), hint: "DROP")
            drawPolyLine(pickUpLat: Double(truncating: pickUpLat), pickUpLon: Double(truncating: pickUpLon), dropLat: Double(truncating: dropLat), dropLon: Double(truncating: dropLon), hint: "defalut")
            self.pickUpMarker = PlaceMarker(latitude: pickUpLatitude, longitude: pickUpLongitude, distance: 0.0, placeName: "pickUp")
            self.pickUpMarker.map = self.mapView
            self.dropMarker = PlaceMarker(latitude: dropLatitude, longitude: dropLongitude, distance: 0.0, placeName: "Drop")
            self.dropMarker.map = self.mapView
            
            self.driverOnWayTableView.reloadData()
        }
    }
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String,hint:String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                    print(pm.subLocality)
                    print(pm.thoroughfare)
                    print(pm.postalCode)
                    print(pm.subThoroughfare)
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    print(addressString)
                    if(hint == "PICK UP"){
                        self.pickUpAddress = addressString
                        self.pickUpPrimary = pm.subLocality!
                    }else{
                        self.dropPrimary = pm.subLocality!
                       self.dropAddress = addressString
                    }
                }
        })
    }
    func optionslistApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["optionsFor" : "driver",
                              "ride_id" : rideID] as [String : Any]
            
            print("Ride Review parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:optionslist, params: parameters as Dictionary<String, Any>) { response in
                print("option list response is \(response)")
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    if(success == "1"){
                        
                        let image = response["driver_image"] as! String
                       
                        self.driverImage = "https://ridehub.co/" + image
                        self.driverName = response["driver_name"] as! String
                     
                        self.driverOnWayTableView.reloadData()
                    }
                    else{
                        
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.cabArraived(_:)), name: NSNotification.Name("cab_arrived"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tripBegin(_:)), name: NSNotification.Name("Ride_start"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.rideCompleted(_:)), name: NSNotification.Name(rawValue: "ride_completed"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.paymentPaid(_:)), name: NSNotification.Name(rawValue: "payment_paid"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.paymentBegin(_:)), name: NSNotification.Name("waitingfor_payment"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.driverLocationUpdate(_:)), name: NSNotification.Name("Updatedriver_loc"), object: nil)
        let appdelegete = UIApplication.shared.delegate as! AppDelegate
        appdelegete.xmpp_plugin()
        appdelegete.connectToXmpp()

    }
    @objc  func rideCompleted(_ notification: Notification?) {
        headingLabel.text = "Ride Completed"
        statusLabel.text = "Ride Completed"
        
        if rideCompleteHint == false {            
            rideCompleteHint = true
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        
        //        popOverVC.fromScreen = "Payment"
        popOverVC.headingMesg = "Message"
        popOverVC.subMesg = "Ride Completed Successfully."
        
        self.addChildViewController(popOverVC)
        popOverVC.view.center = self.view.center
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
        
//        self.themes.showAlert(title: "Message", message: "Ride Completed Successfully" , sender: self)
        
        let speechSynthesizer = AVSpeechSynthesizer()
        // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
        let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: "YOUR DESTINATION IS ARRIVED.")
        //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
        speechUtterance.rate =  0.5
        // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        // Line 5. Pass in the urrerance to the synthesizer to actually speak.
        speechSynthesizer.speak(speechUtterance)
        driverOnWayTableView.reloadData()
    }
    @objc func paymentPaid(_ notification: Notification?){
        let ratingVC = storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
        let object = notification!.object as! NSDictionary
        let rideId = object["rideID"] as! String
        ratingVC.rideID = rideId
        self.navigationController?.pushViewController(ratingVC, animated: true)
    }
    @objc func cabArraived(_ notification: Notification?){
//        let notification1 = UILocalNotification()
//        //notification1.fireDate = "11-22-2020"
//        notification1.alertBody = "Cab Arrivied!"
//        notification1.alertAction = "open"
//        notification1.hasAction = true
//        //notification1.userInfo = ["UUID": "reminderID" ]
//        UIApplication.shared.scheduleLocalNotification(notification1)
        statusLabel.text = "YOUR DRIVER HAS ARRIVED"
        driverTimeLabel.text = "Please be there within 5 minutes"
        let speechSynthesizer = AVSpeechSynthesizer()
        // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
        let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: "Your cab is arrived")
        //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
        speechUtterance.rate =  0.5
        // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        // Line 5. Pass in the urrerance to the synthesizer to actually speak.
        speechSynthesizer.speak(speechUtterance)
        headingLabel.text = "YOUR DRIVER HAS ARRIVED"
        let object = notification!.object as! NSDictionary
        driverLatitude = Double(object["key3"] as! String)!
        driverLongitude = Double(object["key4"] as! String)!
        let driverPosition = CLLocationCoordinate2DMake(driverLatitude, driverLongitude)
        self.driverMarker.position = driverPosition
        driverMarker.appearAnimation = GMSMarkerAnimation.none
        let markerIcon = UIImage(named: "trackCar") /// 16th
        driverMarker.icon = markerIcon
        driverMarker.map = self.mapView
    }
    @objc func tripBegin(_ notification: Notification?){
        let speechSynthesizer = AVSpeechSynthesizer()
        // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
        let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: "YOUR TRIP HAS BEGIN")
        //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
        speechUtterance.rate =  0.5
        // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        // Line 5. Pass in the urrerance to the synthesizer to actually speak.
        speechSynthesizer.speak(speechUtterance)
        statusLabel.text = "YOUR TRIP HAS BEGUN"
        driverTimeLabel.text = "Estimated Time 25 mins"
        headingLabel.text = "YOUR TRIP HAS BEGUN"
        let object = notification!.object as! NSDictionary
        let dropLatitude = Double(object["dropLatitude"] as! String)!
        let dropLongitude = Double(object["dropLongitude"] as! String)!
        let driverLatitude = Double(object["driverLatitude"] as! String)!
        let driverLongitude = Double(object["driverLongitude"] as! String)!
        let estimateDrop = Double(object["estimatedDropTime"] as! String)!
        let pickUpTime = object["pickUpTime"] as! String
   
        let dropTime = Int(estimateDrop)

//        if(driverLoactionHint == false){
//            self.pickUpMarker.map = nil
//            self.dropMarker.map = nil
//            self.pickUpMarker = PlaceMarker(latitude: pickUpLatitude, longitude: pickUpLongitude, distance: 0.0, placeName: "pickUp")
//            self.pickUpMarker.map = self.mapView
//
//            self.dropMarker = PlaceMarker(latitude: dropLatitude, longitude: dropLongitude, distance: 0.0, placeName: "drop")
//            dropMarker.map = self.mapView
//            driverLoactionHint = true
//        }
        estimatedDropTime = String(dropTime) + " mins"
        estimatedPickUpTime = pickUpTime
        drawPolyLine(pickUpLat: pickUpLatitude, pickUpLon: pickUpLongitude, dropLat: dropLatitude, dropLon: dropLongitude, hint: "default")
        
        driverOnWayTableView.reloadData()
    }
    @objc func driverLocationUpdate(_ notification: Notification?){
        if(statusLabel.text == "YOUR TRIP HAS BEGUN"){
            var newLocation = CLLocationCoordinate2D()
            let object = notification!.object as! NSDictionary
            let rideId = object["rideID"] as! String
            let driverLatitude = object["driverLatitude"] as! String
            let driverLongitude = object["driverLongitude"] as! String
            if(OldLocationNext.latitude == 0.0 || OldLocationNext.longitude == 0.0){
                OldLocationNext.latitude = Double(driverLatitude)!
                OldLocationNext.longitude =  Double(driverLongitude)!
            }else{
                OldLocationNext.latitude = oldLocation.latitude
                OldLocationNext.longitude = oldLocation.longitude
            }
            // mapView.camera = GMSCameraPosition(target: newLocation, zoom: 1.0, bearing: 0, viewingAngle: 100)
            newLocation.latitude = Double(driverLatitude)!
            newLocation.longitude = Double(driverLongitude)!
            let oLocation = CLLocationCoordinate2D(latitude: OldLocationNext.latitude, longitude: OldLocationNext.longitude)
            let nLocation = CLLocationCoordinate2D(latitude: newLocation.latitude, longitude: newLocation.longitude)
            
            wayPointLatLon.removeAll()
            var waypoint = String()
            waypoint = "\(newLocation.latitude),\(newLocation.longitude)"
            
     
            
            if(driverLoactionHint == false){
                self.pickUpMarker.map = nil
                self.dropMarker.map = nil
                self.pickUpMarker = PlaceMarker(latitude: pickUpLatitude, longitude: pickUpLongitude, distance: 0.0, placeName: "pickUp")
                self.pickUpMarker.map = self.mapView
                
                self.dropMarker = PlaceMarker(latitude: dropLatitude, longitude: dropLongitude, distance: 0.0, placeName: "drop")
                dropMarker.map = self.mapView
                driverLoactionHint = true
            }
            

            drawDirectionPathwayPoint(userlat: String(pickUpMarker.position.latitude), userlng: String(pickUpMarker.position.longitude), droplat: String(dropMarker.position.latitude), droplon: String(dropMarker.position.longitude), wayPointLatLon: waypoint, angle: CLLocationDegrees(0), driverLat: driverLatitude, driverLon: driverLongitude)
            //            }
            
            self.driverMarker.groundAnchor = CGPoint(x: CGFloat(0.5), y: CGFloat(0.5))
            self.driverMarker.rotation = CLLocationDegrees(exactly: self.getHeadingForDirection(fromCoordinate: nLocation, toCoordinate: oLocation))!
            
            oldLocation.latitude = newLocation.latitude
            oldLocation.longitude = newLocation.longitude
        }
    }
    
    @objc func paymentBegin(_ notification: Notification?){
        let object = notification!.object as! NSDictionary
        print("the object is \(object)")
        // let rideDetails = object["rideDetails"]
        //print("the object is \(rideDetails)")
        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "FareBreakupVC") as! FareBreakupVC
        rideVc.rideDetails = object as! [String : Any]
        self.navigationController?.pushViewController(rideVc, animated: true)
    }
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree - 180.0
        }
        else {
            return (360 + degree) - 180
        }
        
    }
    func drawDirectionPathwayPoint(userlat: String, userlng: String, droplat: String, droplon: String, wayPointLatLon: String,angle:CLLocationDegrees,driverLat: String, driverLon: String) {
        let origin = "\(userlat),\(userlng)"
        let destination = "\(droplat),\(droplon)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&waypoints=\(wayPointLatLon)&mode=driving&key=AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw"
        Alamofire.request(url).responseJSON
            { response in
                self.driverPolyline.map = nil
                
                self.defaultPolyline.map = nil
                if let JSON = response.result.value {
                    let mapResponse: [String: AnyObject] = JSON as! [String : AnyObject]
                    let routesArray = (mapResponse["routes"] as? Array) ?? []
                    let routes = (routesArray.first as? Dictionary<String, AnyObject>) ?? [:]
                    let overviewPolyline = (routes["overview_polyline"] as? Dictionary<String,AnyObject>) ?? [:]
                    self.driverMarker.position = CLLocationCoordinate2D(latitude: Double(driverLat) as! CLLocationDegrees , longitude: Double(driverLon) as! CLLocationDegrees)
                    
                    
                    let markerIcon = UIImage(named: "trackCar") /// 16th
                    self.driverMarker.icon = markerIcon
                    self.driverMarker.map = self.mapView
                    
                    if(overviewPolyline.isEmpty){
                        
                    }else{
                        print("waypoint is \(wayPointLatLon)")
                        self.stepsCoords = decodePolyline(overviewPolyline["points"] as! String)!

                    }
                    
                    
                    let polypoints = (overviewPolyline["points"] as? String) ?? ""
                    
                    
                    
                    let line  = polypoints
                    //  self.addPolyLine(encodedString: line, hint: hint)
                    let path = GMSMutablePath(fromEncodedPath: line)
                    self.driverPolyline = GMSPolyline(path: path)
                    self.driverPolyline.strokeWidth = 8
                    self.driverPolyline.strokeColor = UIColor(red: 47/255.0, green: 163/255.0, blue: 225/255.0, alpha: 1.0)
                
                }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        NotificationCenter.default.removeObserver(self)
    }
    func drawPolyLine(pickUpLat:Double, pickUpLon:Double, dropLat:Double, dropLon:Double,hint:String){
        let origin = "\(pickUpLat),\(pickUpLon)"
        let destination = "\(dropLat),\(dropLon)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw"
        Alamofire.request(url).responseJSON
            { response in
                if let JSON = response.result.value {
                    let mapResponse: [String: AnyObject] = JSON as! [String : AnyObject]
                    let routesArray = (mapResponse["routes"] as? Array) ?? []
                    let routes = (routesArray.first as? Dictionary<String, AnyObject>) ?? [:]
                    let overviewPolyline = (routes["overview_polyline"] as? Dictionary<String,AnyObject>) ?? [:]
                    
                    let polypoints  = (overviewPolyline["points"] as? String) ?? ""
                    let line  = polypoints
                    self.addPolyLine(encodedString: line, hint: hint)
                }
        }
    }
    func addPolyLine(encodedString: String,hint:String) {
        defaultPolyline.map = nil
        // driverPolyline.map = nil
//        let pickUpPosition = CLLocationCoordinate2DMake(pickUpLatitude, pickUpLongitude)
//        let dropPosition = CLLocationCoordinate2DMake(dropLatitude, dropLongitude)
        let path = GMSMutablePath(fromEncodedPath: encodedString)
        pathDrawn = path!
        let polyline = GMSPolyline(path: path)
        
        self.pickUpMarker.map = nil
        self.dropMarker.map = nil
        
//        self.pickUpMarker = PlaceMarker(latitude: pickUpLatitude, longitude: pickUpLongitude, distance: 0.0, placeName: "pickUp")
//        self.pickUpMarker.map = self.mapView
//
//        self.dropMarker = PlaceMarker(latitude: dropLatitude, longitude: dropLongitude, distance: 0.0, placeName: "drop")
//        dropMarker.map = self.mapView
//
        
        polyline.strokeWidth = 8.0
        // polyline.map = self.mapView
        
        
        let greenToRed = GMSStrokeStyle.gradient(from: UIColor(red: 81/255.0, green: 190/255.0, blue: 19/255.0, alpha: 1.0), to: UIColor(red: 6/255.0, green: 152/255.0, blue: 212/255.0, alpha: 1.0))
        polyline.spans = [GMSStyleSpan(style: greenToRed)]
        polyline.map = self.mapView
        //driverPolyline.map = self.mapView
        //polyline.map = self.mapView
        // polyline.map = nil
        // let bounds = GMSCoordinateBounds(path: path!)
        // let update = GMSCameraUpdate.fit(bounds, withPadding: 20.0)
        //bounds=[bounds  initWithCoordinate:PickUpmarker.position coordinate:Dropmarker.position];
        //mapView.animate(with: update)
    }
    func shadowView(view:UIView){
        view.layer.cornerRadius = 15
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == popUpTableView {
            if buttonTag == "edit" {
                return 4
            } else if buttonTag == "safety" {
                return 3
            } else {
            return 0
            }
        } else {
        return 4
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == popUpTableView {
            if buttonTag == "edit"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PopUpEditTableCell", for: indexPath) as! PopUpEditTableCell
            cell.selectionStyle = .none
            cell.editNameLabel.text = EditArray[indexPath.row]
            cell.editImage.image = UIImage(named: EditImageArray[indexPath.row])

            return cell
            } else if buttonTag == "safety"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "PopUpSafetyTableCell", for: indexPath) as! PopUpSafetyTableCell
                cell.selectionStyle = .none
                cell.safetyHeadLbl.text = safetyArray[indexPath.row]
                cell.safetyImage.image = UIImage(named: safetyImageArray[indexPath.row])
                return cell
            } else {
                return UITableViewCell()
            }
            
        } else {
            
        if indexPath.row == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DriverProfileTableCell", for: indexPath) as! DriverProfileTableCell
            cell.tripwasHeight.constant = 0
            cell.feedBackHeight.constant = 0
            cell.driverNameLbl.text = driverName
            cell.vehNumberLbl.text = carNumber
            cell.vehTypeLbl.text = catagoryType
            cell.vehModelLbl.text = carName
            
            
            let rating = driverRating
            
            
            if(rating == ""){
                cell.star1.image = UIImage(named: "star_empty")
                cell.star2.image = UIImage(named: "star_empty")
                cell.star3.image = UIImage(named: "star_empty")
                cell.star4.image = UIImage(named: "star_empty")
                cell.star5.image = UIImage(named: "star_empty")
            }else{
                DispatchQueue.main.async {
                    let rate = self.driverRating
                    let ratingDouble = Double(rate)
                    let ratingInt = Int(ratingDouble!)
                    if(ratingInt == 1){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_empty")
                        cell.star3.image = UIImage(named: "star_empty")
                        cell.star4.image = UIImage(named: "star_empty")
                        cell.star5.image = UIImage(named: "star_empty")
                    }else if(ratingInt == 2){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_filled")
                        cell.star3.image = UIImage(named: "star_empty")
                        cell.star4.image = UIImage(named: "star_empty")
                        cell.star5.image = UIImage(named: "star_empty")
                    }else if(ratingInt == 3){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_filled")
                        cell.star3.image = UIImage(named: "star_filled")
                        cell.star4.image = UIImage(named: "star_empty")
                        cell.star5.image = UIImage(named: "star_empty")
                    }else if(ratingInt == 4){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_filled")
                        cell.star3.image = UIImage(named: "star_filled")
                        cell.star4.image = UIImage(named: "star_filled")
                        cell.star5.image = UIImage(named: "star_empty")
                    }else if(ratingInt == 5){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_filled")
                        cell.star3.image = UIImage(named: "star_filled")
                        cell.star4.image = UIImage(named: "star_filled")
                        cell.star5.image = UIImage(named: "star_filled")
                    }
                    
                }
                
            }
            
            
            
            
            
            let fullimageUrl = themes.checkNullValue(URL(string: driverImage)) as! String
            if(fullimageUrl == ""){
                cell.driverImgView.image = UIImage(named: "user_default")
            }else{
               
                let url = URL(string: driverImage)
                cell.driverImgView.load(url: url!)
            }
            
            
            
            
            cell.selectionStyle = .none
        
        return cell
        }
        else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DriverContactCell", for: indexPath) as! DriverContactCell
            cell.selectionStyle = .none
            cell.editBtn.addTarget(self, action: #selector(editBtnClicked(button:)), for: .touchUpInside)
            cell.saftyBtn.addTarget(self, action: #selector(saftyBtnClicked(button:)), for: .touchUpInside)
            cell.messageBtn.addTarget(self, action: #selector(messageBtnClicked(button:)), for: .touchUpInside)
            cell.contactBtn.addTarget(self, action: #selector(contactBtnClicked(button:)), for: .touchUpInside)
            
            return cell
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripTableCell", for: indexPath) as! TripTableCell
            cell.selectionStyle = .none
            cell.pickUpLbl.text = pickUpPrimary
            cell.dropLbl.text = dropPrimary
            cell.pickUpPointLbl.text = pickUpAddress
            cell.dropPointLbl.text = dropAddress
            cell.pickUpTimeLbl.text = estimatedPickUpTime
            cell.dropTimeLbl.text = estimatedDropTime
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FareCancelCell", for: indexPath) as! FareCancelCell
            cell.fareLbl.text = "$ " + vehicleFare
            cell.cancelBookingBtn.addTarget(self, action: #selector(cancelBookingBtnClicked(button:)), for: .touchUpInside)
            cell.selectionStyle = .none
            if(headingLabel.text == "Ride Completed"){
                cell.cancelBookingBtn.isHidden = true
            }
            return cell
           }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == popUpTableView {
            if buttonTag == "edit" {
                return 72
            } else {
                return 80
            }
        } else {
        if indexPath.row == 0 {
            return 128
        } else if indexPath.row == 1{
            return 110
        } else if indexPath.row == 2{
            return 224
        }else {
            return 214
          }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if tableView == popUpTableView {
            if buttonTag == "edit" {
                
                if(indexPath.row == 0){
                    if(headingLabel.text  == "WE FOUND YOU A DRIVER"){
                        let ratingVC = storyboard?.instantiateViewController(withIdentifier: "CancelRideVC") as! CancelRideVC
                        ratingVC.rideID = rideID
                        self.navigationController?.pushViewController(ratingVC, animated: true)
                    }else if (headingLabel.text  == "YOUR DRIVER HAS ARRIVED"){
                        
                        themes.showAlert(title: "Alert", message: "Already your driver has been arrived.You can't cancel this ride.", sender: self)
                        
                    }
                    else if (headingLabel.text  == "YOUR TRIP HAS BEGUN"){
                        
                        themes.showAlert(title: "Alert", message: "Already your ride has been started.You can't cancel this ride.", sender: self)
                        
                    } else if (headingLabel.text  == "Ride Completed"){
                        
                        
                        
                    }
                }
                
            } else {
                if(indexPath.row == 0){
                    
                    let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SaftyVC") as! SaftyVC
                    self.navigationController?.pushViewController(Vc, animated: true)
                }else if(indexPath.row == 1){
                    
                    let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ShareMyTripVC") as! ShareMyTripVC
                    self.navigationController?.pushViewController(Vc, animated: true)
                    
                }else if(indexPath.row == 2){
                    if let url = URL(string: "tel://\(911)") {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    }
                    
                    
                }
            }
        }
    }
    
    
    
    
    @objc func editBtnClicked(button: UIButton){
        
        driverOnWayView.isHidden = true
        self.popUpView.isHidden = false
        buttonTag = "edit"
        popUpHeadingHeight.constant = 0
        popUpViewHeight.constant = 370
       popUpTableView.reloadData()
    }
    
    @objc func saftyBtnClicked(button: UIButton){
        driverOnWayView.isHidden = true
        
        self.popUpView.isHidden = false
        buttonTag = "safety"
        popUpHeadingHeight.constant = 34
        popUpViewHeight.constant = 320
        popUpTableView.reloadData()
    }
    
    @objc func messageBtnClicked(button: UIButton){
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    @objc func cancelBookingBtnClicked(button: UIButton){
        if(headingLabel.text  == "WE FOUND YOU A DRIVER"){
            let ratingVC = storyboard?.instantiateViewController(withIdentifier: "CancelRideVC") as! CancelRideVC
            ratingVC.rideID = rideID
            self.navigationController?.pushViewController(ratingVC, animated: true)
        }else if (headingLabel.text  == "YOUR DRIVER HAS ARRIVED"){
            
          themes.showAlert(title: "Alert", message: "Already your driver has been arrived.You can't cancel this ride.", sender: self)
            
        }
        else if (headingLabel.text  == "YOUR TRIP HAS BEGUN"){
            
             themes.showAlert(title: "Alert", message: "Already your ride has been started.You can't cancel this ride.", sender: self)
            
        } else if (headingLabel.text  == "Ride Completed"){
        }
    }
    @objc func contactBtnClicked(button: UIButton){
        if let url = URL(string: "tel://\(mobileNumber)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func dragAction(_ sender: Any) {
        view.layoutIfNeeded()
        if(downHint){
            let height = UIScreen.main.bounds.height - 90
            tableHeight.constant = height
            downHint = false
            blueView.isHidden = false
            headingLabel.isHidden = false
            closeBtn.isHidden = false
            driverOnWayTableView.isScrollEnabled = true
            statusHeight.constant = 0
            statusLabel.isHidden = true
            driverTimeLabel.isHidden = true
        }else{
            driverOnWayTableView.setContentOffset(.zero, animated: true)
            statusHeight.constant = 50
            statusLabel.isHidden = false
            driverTimeLabel.isHidden = false
             driverOnWayTableView.isScrollEnabled = false
             tableHeight.constant = 313
            downHint = true
            blueView.isHidden = true
            headingLabel.isHidden = true
            closeBtn.isHidden = true
        }
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        }
}
extension UIView{
    func animShow(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                       animations: {
                        self.center.y -= self.bounds.height
                        self.layoutIfNeeded()
        }, completion: nil)
        self.isHidden = false
    }
    func animHide(){
        UIView.animate(withDuration: 2, delay: 0, options: [.curveLinear],
                       animations: {
                        self.center.y += self.bounds.height
                        self.layoutIfNeeded()
                        
        },  completion: {(_ completed: Bool) -> Void in
            self.isHidden = true
        })
    }
}
extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}
extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
