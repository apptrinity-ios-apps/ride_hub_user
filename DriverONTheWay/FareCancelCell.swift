//
//  FareCancelCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class FareCancelCell: UITableViewCell {
    
    
    @IBOutlet weak var fareLbl: UILabel!    
    @IBOutlet weak var cancelBookingBtn: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
    cancelBookingBtn.layer.cornerRadius = cancelBookingBtn.frame.height/2
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
