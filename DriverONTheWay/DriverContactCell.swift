//
//  DriverContactCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class DriverContactCell: UITableViewCell {
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var saftyBtn: UIButton!
    @IBOutlet weak var messageBtn: UIButton!
    @IBOutlet weak var contactBtn: UIButton!
    
    @IBOutlet weak var dotLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
         drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.minY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
//        shapeLayer.strokeColor = (UIColor.init(red: 6, green: 152, blue: 212, alpha: 1) as! CGColor)
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
//        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.lineDashPattern =  [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
}
