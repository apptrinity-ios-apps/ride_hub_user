//
//  HomeVC.swift
//  RideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import Alamofire
import SwiftyJSON
import AVFoundation
class HomeVC: UIViewController,UITableViewDataSource,UITableViewDelegate,GMSMapViewDelegate,CLLocationManagerDelegate,UISearchBarDelegate,UIWebViewDelegate,LocateOnTheMap1,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SlideToControlDelegate {
   
    func addPolyLine(encodedString: String) {
        let pickUpPosition = CLLocationCoordinate2DMake(pickUplatitude, pickUpLongitude)
        let dropPosition = CLLocationCoordinate2DMake(dropLatitude, dropLongitude)
        let path = GMSMutablePath(fromEncodedPath: encodedString)
        let polyline = GMSPolyline(path: path)
        self.pickUpMarker.position = pickUpPosition
        self.dropMarker.position = dropPosition
        pickUpMarker.map = mapView
        dropMarker.map = mapView
        polyline.map = nil
        polyline.strokeWidth = 3
        polyline.strokeColor = .blue
        polyline.map = self.mapView
        
        let bounds = GMSCoordinateBounds(coordinate: pickUpPosition, coordinate: dropPosition)
        
        mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 120.0))
        
    }
    var favObject = Array<AnyObject>()
    var recentObject = Array<AnyObject>()
    var pickUpPrimaryAddress = String()
    var dropPrimaryAddress = String()
    var Record_Driver = DriverRecord()
    var ridePrice = String()
    var nameofcar = String()
    var CarCategoryString = String()
    let menuListArray = ["Book a Ride","Ride History","Free Rides","Payment","Help","Settings"]
    let menuIconsArray = ["bookARide","rideHistory","freeRides","payment","help","setting"]
    var fare = String()
     var ETAtimeTaking = String()
    var rideTypeArray = [AnyObject]()
    var currentLatitude = Double()
    var currentLongtude = Double()
    var bookingRideId = String()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var locationManager = CLLocationManager()
    var pickUplatitude = Double()
    var pickUpLongitude = Double()
    var dropLatitude = Double()
    var dropLongitude = Double()
    var dropMarker = GMSMarker()
   // var searchResultController: SearchResultsController!
    var pickUpMarker = GMSMarker()
    var pickUpDropHint = Int()
    var pickupLoaction = String()
    var carSelectionHint = Int()
    var pickUpAddress1 = String()
    var dropAddress1 = String()
    @IBOutlet var confirmPickSubView: UIView!
    @IBOutlet var destinationlabel: UILabel!
    @IBOutlet var carNameLabel: UILabel!
    @IBOutlet var confirmPickUpsearchView: UIView!
    @IBOutlet var confirmPickUpView: UIView!
    @IBOutlet var processingView: UIView!
    @IBOutlet var slider: SlideToControl!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var confirmPickUpBtn: UIButton!
    @IBOutlet var noCarsImageView: UIImageView!
    @IBOutlet var goBtn: UIButton!
    @IBOutlet var carSelectionView: UIView!
    @IBOutlet var favouriteSearchView: UIView!
    @IBOutlet var carTypeCollectionView: UICollectionView!
    @IBOutlet var menuTapView: UIView!
    @IBOutlet var menuTableView: UITableView!
    @IBOutlet var menuViewWidth: NSLayoutConstraint!
    @IBOutlet var sideMenuView: UIView!
    @IBOutlet var favouriteView: UIView!
    @IBOutlet var favoutiteTableView: UITableView!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var favouriteSubView: UIView!
    @IBOutlet weak var wishLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
//        recentRideListAPI()
//        getFavoriteListAPI()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // slider.ser
        driverNote = ""
       
        let hour = Calendar.current.component(.hour, from: Date())
        
        switch hour {
        case 6..<12 :
            print(NSLocalizedString("Morning", comment: "Morning"))
            
            wishLabel.text = "Good Morning  " + themes.getUserName()
            
        case 12 :
            wishLabel.text = "Good Afternoon " + themes.getUserName()
            print(NSLocalizedString("Noon", comment: "Noon"))
        case 13..<17 :
            wishLabel.text = "Good Afternoon " + themes.getUserName()
            print(NSLocalizedString("Afternoon ", comment: "Afternoon"))
        case 17..<22 :
            wishLabel.text = "Good Evening " + themes.getUserName()
            print(NSLocalizedString("Evening", comment: "Evening"))
        default:
            wishLabel.text = "Hi " + themes.getUserName()
            print(NSLocalizedString("Night", comment: "Night"))
        }
        
        favouriteView.isHidden = false
        carSelectionView.isHidden = true
        confirmPickUpView.isHidden = true
        processingView.isHidden = true
        

        carSelectionHint = -1
        carSelectionView.isHidden = true
        slider.delegate = self
        activityIndicator.activityIndicatorViewStyle =  .whiteLarge
        activityIndicator.color =  UIColor(red: 81/255.0, green: 190/255.0, blue: 19/255.0, alpha: 1.0)
        self.goBtn.isEnabled = false
//        CGAffineTransform; transform = CGAffineTransformMakeScale(1.5,f, 1.5f);
//        activityIndicator.transform = transform;
//
        
        goBtn.applyGradient(colours: [UIColor(red: 6/255.0, green: 152/255.0, blue: 212/255.0, alpha: 1.0),UIColor(red: 3/255.0, green: 76/255.0, blue: 106/255.0, alpha: 1.0)])
        
        goBtn.layer.cornerRadius = goBtn.frame.height/2
        goBtn.clipsToBounds = true
        
        confirmPickUpBtn.layer.cornerRadius = confirmPickUpBtn.frame.height/2
        confirmPickUpBtn.clipsToBounds = true
        processingView.layer.cornerRadius = 15
        processingView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        processingView.layer.shadowOpacity = 0.5
        processingView.layer.shadowRadius = 3
        processingView.layer.masksToBounds = false
        processingView.layer.shadowColor = UIColor.gray.cgColor
        carSelectionView.layer.cornerRadius = 15
        carSelectionView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        carSelectionView.layer.shadowOpacity = 0.5
        carSelectionView.layer.shadowRadius = 3
        carSelectionView.layer.masksToBounds = false
        carSelectionView.layer.shadowColor = UIColor.gray.cgColor
 
//
//        favouriteView.layer.cornerRadius = 15
//        favouriteView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
//        favouriteView.layer.shadowOpacity = 0.5
//        favouriteView.layer.shadowRadius = 3
//        favouriteView.layer.masksToBounds = false
//        favouriteView.layer.shadowColor = UIColor.gray.cgColor
        favouriteSubView.layer.cornerRadius = 15
        shadowView(view: favouriteView)
        confirmPickUpView.layer.cornerRadius = 15
        confirmPickUpView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        confirmPickUpView.layer.shadowOpacity = 0.5
        confirmPickUpView.layer.shadowRadius = 3
        confirmPickUpView.layer.masksToBounds = false
        confirmPickUpView.layer.shadowColor = UIColor.gray.cgColor
        // Do any additional setup after loading the view.
        self.menuTableView.rowHeight = 50;
        self.menuTableView.backgroundColor = UIColor.clear
        self.menuTableView.separatorColor = UIColor.clear
        let screenSize: CGRect = UIScreen.main.bounds
        menuViewWidth.constant = screenSize.width * 0.75
        self.menuTapView.isHidden = true
        animation(viewAnimation: sideMenuView)
        
        favouriteSearchView.layer.cornerRadius = favouriteSearchView.frame.height / 2
        favouriteSearchView.clipsToBounds = true
        confirmPickUpsearchView.layer.cornerRadius = favouriteSearchView.frame.height / 2
        confirmPickUpsearchView.clipsToBounds = true
        confirmPickSubView.layer.cornerRadius = 15
        confirmPickSubView.clipsToBounds = true
        favouriteView.layer.cornerRadius = 25
//        favouriteView.clipsToBounds = true
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
          self.favoutiteTableView.register(UINib(nibName: "FavouriteLocationCell", bundle: nil), forCellReuseIdentifier: "FavouriteLocationCell")
        
        self.menuTableView.register(UINib(nibName: "HeaderMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "HeaderMenuTableViewCell")
        
        self.menuTableView.register(UINib(nibName: "LeftMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "LeftMenuTableViewCell")
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.pushnotification(_:)), name: NSNotification.Name("pushnotification"), object: nil)
        
        self.carTypeCollectionView.register(UINib(nibName: "CarTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CarTypeCollectionViewCell")
        
        recentRideListAPI()
        getFavoriteListAPI()
       
//        sahdowView(view: favouriteView)
//        sahdowView(view: carSelectionView)
    }
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        // Show the navigation bar on other view controllers
//        NotificationCenter.default.removeObserver(self)
//    }
    func shadowView(view:UIView){
        view.layer.cornerRadius = 15
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }
    @objc func pushnotification(_ notification: Notification?) {
        print("notification called")
        if(notification?.object is [AnyHashable : Any]){
            // Line 1. Create an instance of AVSpeechSynthesizer.
            let speechSynthesizer = AVSpeechSynthesizer()
            // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
            let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: "Hi \(themes.getUserName()).Welcome to ridehub.")
            //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
            speechUtterance.rate =  0.5
            // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
            speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            // Line 5. Pass in the urrerance to the synthesizer to actually speak.
            speechSynthesizer.speak(speechUtterance)
            let object = notification!.object as! NSDictionary
            driverDetails = object
            Record_Driver.driver_Name = object["key2"] as! String
            Record_Driver.car_Name = object["key12"] as! String
            Record_Driver.car_Number = object["key11"] as! String
            Record_Driver.latitude_driver = Double(object["key6"] as! String)!
            Record_Driver.longitude_driver = Double(object["key7"] as! String)!
            Record_Driver.driver_moblNumber = object["key10"] as! String
            Record_Driver.eta = object["key8"] as! String
            Record_Driver.message = object["message"] as! String
            Record_Driver.latitude_User = (object["key14"] as? NSNumber)?.doubleValue ?? 0.0
            Record_Driver.longitude_User = (object["key15"] as? NSNumber)?.doubleValue ?? 0.0
            Record_Driver.ride_ID = object["key9"] as! String
            Record_Driver.rating = object["key5"] as! String
            Record_Driver.driverImage = object["key4"] as! String
            //self.navigationController?.popViewController(animated: true)
            let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "DriverOnWayVc") as! DriverOnWayVc
            rideVc.estimatedPickUpTime = object["key8"] as! String
            rideVc.driverRating = object["key5"] as! String
            rideVc.catagoryType = object["key21"] as! String
            rideVc.vehicleFare = String()
            rideVc.pickUpPrimary = pickUpPrimaryAddress
           // rideVc.catagoryType = object["key21"] as! String
            rideVc.dropPrimary = dropPrimaryAddress
            rideVc.vehicleFare = fare
            rideVc.rideID = bookingRideId
            rideVc.pickUpPrimary = pickUpPrimaryAddress
            rideVc.driverName =  Record_Driver.driver_Name
            rideVc.carName = Record_Driver.car_Name
            rideVc.carNumber = Record_Driver.car_Number
            rideVc.driverImage = Record_Driver.driverImage
            rideVc.pickUpAddress = pickUpAddress1
            rideVc.dropAddress = dropAddress1
            rideVc.driverLatitude = Record_Driver.latitude_driver
            rideVc.driverLongitude = Record_Driver.longitude_driver
            rideVc.mobileNumber = Record_Driver.driver_moblNumber
            rideVc.pickUpLatitude = pickUpMarker.position.latitude
            rideVc.pickUpLongitude = pickUpMarker.position.longitude
            rideVc.dropLatitude = dropMarker.position.latitude
            rideVc.dropLongitude = dropMarker.position.longitude
            rideVc.eta = Record_Driver.eta
            self.navigationController?.pushViewController(rideVc, animated: true)
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    func sliderCameToEnd() {
        CancelRide()
    }
    
    func sahdowView(view: UIView){
        view.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 3)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 5.0
        view.layer.masksToBounds = true
    }
    
    private func animation(viewAnimation: UIView) {
        UIView.animate(withDuration: 0.4, animations: {
            viewAnimation.frame.origin.x = -viewAnimation.frame.width
        }) { (_) in
            
        }
    }

    @IBAction func menuAction(_ sender: Any) {
        self.sideMenuView.isHidden = false
        self.menuTapView.isHidden = false
        var leftMenuFrame: CGRect = sideMenuView.frame
      
        leftMenuFrame.origin.x = 0
        
        UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {() -> Void in
            self.sideMenuView.frame = leftMenuFrame
           
            self.sideMenuView.bringSubview(toFront: self.menuTableView)
//            self.sideMenuView.bringSubview(toFront: self.profileImg)
//            self.sideMenuView.bringSubview(toFront: self.nameLbl)
            self.menuTableView.reloadData()
            
        }, completion: {(_ finished: Bool) -> Void in
            self.menuTapView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tappedOnView)))
        })

    }
    
    @objc func tappedOnView(sender: UITapGestureRecognizer) {
        
        self.menuTapView.isHidden = true
        animation(viewAnimation: sideMenuView)
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == menuTableView){
             return menuListArray.count
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == menuTableView){

            let cell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuTableViewCell", for: indexPath)as! LeftMenuTableViewCell
            cell.menuNameLbl.text = menuListArray[indexPath.row]
            cell.menuImageView.image = UIImage(named: menuIconsArray[indexPath.row])
            cell.selectionStyle = .none
            return cell
        }else{
         let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteLocationCell", for: indexPath) as! FavouriteLocationCell
        tableView.separatorColor = UIColor.clear
        cell.selectionStyle = .none
            
//         let homeAddress = themes.checkNullValue(UserDefaults.standard.object(forKey: "homeAddress")) as! String
//            let officeAddress = themes.checkNullValue(UserDefaults.standard.object(forKey: "officeAddress")) as! String
            
    
        if(indexPath.row == 0){
            
            if(favObject.count == 0){
                cell.addressLabel.text = " - "
            }else{
                let obj = favObject[0]
                cell.addressLabel.text = (obj["address"] as! String)
                let type = obj["title"] as! String
               
                if(type == "home"){
                    cell.nameLabel.text = "Home"
                     cell.imageview.image = UIImage(named: "homeFav")
                }else{
                    cell.nameLabel.text = "Work"
                    cell.imageview.image = UIImage(named: "office")
                }
            }
        }else{
            cell.imageview.image = UIImage(named: "recentFav")
            
            if(recentObject.count == 0){
                 cell.nameLabel.text = "Recent"
                 cell.addressLabel.text = " - "
            }else{
                let obj = recentObject[0]
                cell.nameLabel.text = "Recent"
                cell.addressLabel.text = (obj["destination"] as! String)
                
            }
        }
        return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == menuTableView){
            if(indexPath.row == 0){
//
//                let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//
//                self.navigationController?.pushViewController(rideVc, animated: true)
    
                self.mapView.clear()
                self.favouriteView.isHidden = false
                self.processingView.isHidden = true
                self.confirmPickUpView.isHidden = true
                self.dropPrimaryAddress = ""
                self.dropAddress1 = ""
                self.slider.updateThumb()
                
               locationManager.startUpdatingLocation()
                
                
                
              sideMenuView.isHidden = true
                menuTapView.isHidden = true
               animation(viewAnimation: sideMenuView)
            }
            else if indexPath.row == 1 {
                let tripsVc = self.storyboard?.instantiateViewController(withIdentifier: "YourRidesViewController") as! YourRidesViewController
                self.navigationController?.pushViewController(tripsVc, animated: true)
            }else if indexPath.row == 2 {
                let settingsVc = self.storyboard?.instantiateViewController(withIdentifier: "InviteViewController") as! InviteViewController
                self.navigationController?.pushViewController(settingsVc, animated: true)
            }else if indexPath.row == 3 {
                let settingsVc = self.storyboard?.instantiateViewController(withIdentifier: "MakePaymentVC") as! MakePaymentVC
                self.navigationController?.pushViewController(settingsVc, animated: true)
            }else if indexPath.row == 4 {
                let settingsVc = self.storyboard?.instantiateViewController(withIdentifier: "HelpLastTripVC") as! HelpLastTripVC
                self.navigationController?.pushViewController(settingsVc, animated: true)
            } else if indexPath.row == 5 {
                let settingsVc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
                self.navigationController?.pushViewController(settingsVc, animated: true)
            }
        }else{
           
            if(indexPath.row == 0){
                
                if(favObject.count == 0){
                    
                }else{
                    let obj = favObject[0]
                    
                    let latitude = obj["latitude"] as! Double
                    let longitude = obj["longitude"] as! Double
//                    let lat = Double(latitude)
//                    let lon = Double(longitude)
                    getAddressFromLatLon(pdblLatitude: String(latitude), withLongitude: String(longitude), hint: "Drop")
                    
                    locateWithLongitude(pickLatiTude: pickUplatitude, picklongitude: pickUpLongitude, dropLatitude: latitude, dropLongitude: longitude, pickUpAddress: pickUpAddress1, dropAddresss: dropAddress1, pickUpMain: pickUpPrimaryAddress, dropMain: dropPrimaryAddress)

                }
                
                
            }else{
                
                if(recentObject.count == 0){
                    
                }else{
                    let obj = recentObject[0]
                    
                    let latitude = obj["pickup_lat"] as! Double
                    let longitude = obj["pickup_long"] as! Double
                    //                    let lat = Double(latitude)
                    //                    let lon = Double(longitude)
                    getAddressFromLatLon(pdblLatitude: String(latitude), withLongitude: String(longitude), hint: "Drop")
                    
                    locateWithLongitude(pickLatiTude: pickUplatitude, picklongitude: pickUpLongitude, dropLatitude: latitude, dropLongitude: longitude, pickUpAddress: pickUpAddress1, dropAddresss: dropAddress1, pickUpMain: pickUpPrimaryAddress, dropMain: dropPrimaryAddress)
 
                }

            }
        }
    }
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(tableView == menuTableView){
            let header = tableView.dequeueReusableCell(withIdentifier: "HeaderMenuTableViewCell")as! HeaderMenuTableViewCell
            header.driverPicture.layer.cornerRadius =  header.driverPicture.frame.size.height/2
            header.driverPicture.clipsToBounds = true
           // header.headerBtn.addTarget(self, action: #selector(self.headerBtnClicked(button:)), for: .touchUpInside)
            header.headerBtn.addTarget(self, action: #selector(profileEditBtnClicked(button:)), for: .touchUpInside)
            header.driverNameLbl.text = self.themes.getUserName()
            header.driverLocationLbl.text = self.themes.getmobileNumber()
            let image1 = themes.checkNullValue(self.themes.getUserDp()) as! String
            if(image1 == ""){
                header.driverPicture.image = UIImage(named: "user_default")
            }else{
                let image = self.themes.getUserDp()
                let fullimageUrl = URL(string: image)
                header.driverPicture.load(url: fullimageUrl!)
            }
            return header
        } else {
            return UIView()
        }
    }
    
    @objc func profileEditBtnClicked(button : UIButton){
        let editVc = self.storyboard?.instantiateViewController(withIdentifier: "EditAccountVC") as!  EditAccountVC
        
        self.navigationController?.pushViewController(editVc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if(tableView == menuTableView){
            return 114
        } else {
           return 0
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == menuTableView){
            return 56
        } else {
            return 62
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rideTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CarTypeCollectionViewCell", for: indexPath) as! CarTypeCollectionViewCell
        cell.carNameLabel.text = rideTypeArray[indexPath.row].value(forKey: "name") as? String
        cell.timeLabel.text = (rideTypeArray[indexPath.row].value(forKey: "eta") as? String)!
//error checking
        cell.carPriceLabel.text = "$ " + (rideTypeArray[indexPath.row].value(forKey: "max_amount") as? String)!
        
        cell.membersLabel.text = (rideTypeArray[indexPath.row].value(forKey: "seatcapacity_max") as? String)!
        
        if(carSelectionHint == indexPath.item){
            
            cell.memberImageview.image = UIImage(named: "Icon awesome-user-circle")
//cell.backview.backgroundColor = UIColor(red: 3/255.0, green: 76/255.0, blue: 106/255.0, alpha: 1.0)
            cell.backImageView.isHidden = false

            cell.carPriceLabel.textColor = UIColor(red: 186/255.0, green: 218/255.0, blue: 43/255.0, alpha: 1.0)
            
            cell.carNameLabel.textColor = UIColor.white
            cell.membersLabel.textColor = UIColor.white
            
            cell.timeLabel.textColor = UIColor.white
            
            
            
        }else{
             cell.backImageView.isHidden = true
            cell.memberImageview.image = UIImage(named: "chooseRideProfile")
            cell.backview.backgroundColor = UIColor.white
            
            cell.carPriceLabel.textColor = UIColor(red: 6/255.0, green: 152/255.0, blue: 212/255.0, alpha: 1.0)
            
            cell.carNameLabel.textColor = UIColor(red: 22/255.0, green: 48/255.0, blue: 61/255.0, alpha: 1.0)
            cell.membersLabel.textColor = UIColor.black
            
            cell.timeLabel.textColor = UIColor.black
            
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        carSelectionHint = indexPath.item
         carNameLabel.text = rideTypeArray[indexPath.row].value(forKey: "name") as? String
         fare =  (rideTypeArray[indexPath.row].value(forKey: "max_amount") as? String)!
        ETAtimeTaking = (rideTypeArray[indexPath.row].value(forKey: "eta") as? String)!
         let id = (rideTypeArray[indexPath.row].value(forKey: "id") as? String)!
        getHomePages(category: id, index: indexPath.row)
        
        carTypeCollectionView.reloadData()
    }
    
    func getHomePages(category:String,index : Int){
        
        let latitudeStr = "\(self.pickUpMarker.position.latitude)"
        let longitudeStr = "\(self.pickUpMarker.position.longitude)"
        let droplatitudeStr = "\(self.dropMarker.position.latitude)"
        let droplongitudeStr = "\(self.dropMarker.position.longitude)"
        let user_id = themes.checkNullValue(themes.getUserId()) as! String
        
        let parameters = ["user_id": user_id,
                          "pickup_lat": latitudeStr,
                          "pickup_lon": longitudeStr,
                          "drop_lat": droplatitudeStr,
                          "drop_lon": droplongitudeStr /*cc */,
            "category": category //,@"subcategory":CarSubCategoryString
            ] as [String : Any]
        print("parameters is \(parameters)")
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            urlService.serviceCallPostMethodWithParams(url:Map_and_Taxi_Selection, params: parameters ) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    if(success == "1"){
                        print(response)
                        // self.continueBookTable.isHidden = false
                        //  self.tapView.isHidden = false
                        let categaory = BookingRecord()
                        let response1 = response["response"]
                        let category = response1!["category"] as! Array<AnyObject>
                         self.rideTypeArray = response["response"]!["category"] as! [AnyObject]
                        for i in 0...category.count-1{
                            
                            let objCatDict = category[i]
                            if(i == index){
                                self.CarCategoryString = objCatDict["id"] as! String
                                self.nameofcar = objCatDict["name"] as! String
                                categaory.normal_image = objCatDict["icon_normal"] as! String
                                self.ETAtimeTaking = objCatDict["eta"] as! String
                                self.ridePrice = objCatDict["max_amount"] as! String
                                categaory.active_Image = objCatDict["icon_active"] as! String
                                categaory.isSelected = false
                            }
                            
                        }
 
                        
                        if((self.ETAtimeTaking == "No cabs") || (self.ETAtimeTaking == "no cabs")){
                            
                            self.goBtn.isEnabled = false
                            self.noCarsImageView.isHidden = false
                            
                        }else{
                            self.goBtn.isEnabled = true
                            self.noCarsImageView.isHidden = true
                            
                        }
                        self.carTypeCollectionView.reloadData()

                    }
                    else{
                        
                        let result = response["response"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        } else {
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 179, height: 141)
    }
    
//    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
//        let path = GMSMutablePath(fromEncodedPath: "")
//        let polyline = GMSPolyline(path: path)
//        polyline.map = nil
//        DispatchQueue.main.async { () -> Void in
//            let position = CLLocationCoordinate2DMake(lat, lon)
//            let middle = CLLocationCoordinate2D(latitude: lat , longitude: lon)
//            if(self.pickUpDropHint == 1){
//                self.pickUplatitude = lat
//                self.pickUpLongitude = lon
//
//
//                DispatchQueue.main.async {
//
//
//                self.pickUpMarker = PlaceMarker(latitude: lat, longitude: lon, distance: 0.0, placeName: "pickUp")
//                self.pickUpMarker.map = self.mapView
//
//                }
//
//               // self.pickUpMarker.map = self.mapView
//               // self.pickUpMarker.position = position
//               // self.pickUpBtn.setTitle(title, for: .normal)
//            }else{
//                self.dropLatitude = lat
//                self.dropLongitude = lon
//
//                self.dropMarker = PlaceMarker(latitude: lat, longitude: lon, distance: 0.0, placeName: "drop")
//                self.dropMarker.map = self.mapView
//
//
////                self.dropMarker.map = self.mapView
////                self.dropMarker.position = middle
//               // self.dropBtn.setTitle(title, for: .normal)
//                self.mapView.clear()
//               // self.drawPolyLine()
//            }
//            //  self.dropMarker.position = position
//
//
//            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 13, bearing: 270, viewingAngle: 45)
//            self.mapView.camera = camera
//        }
//    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         self.mapView .clear()
        let userLocation = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude, zoom: 12, bearing: 90, viewingAngle: 0)
        getAddressFromLatLon(pdblLatitude: String(userLocation!.coordinate.latitude), withLongitude: String(userLocation!.coordinate.longitude), hint: "PickUp")
       // pickUpMarker.position = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        self.pickUpMarker = PlaceMarker(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude, distance: 0.0, placeName: "pickUp")
        self.pickUpMarker.map = self.mapView
        pickUplatitude = userLocation!.coordinate.latitude
        pickUpLongitude = userLocation!.coordinate.longitude
        currentLatitude = userLocation!.coordinate.latitude
        currentLongtude = userLocation!.coordinate.longitude
       // self.pickUpMarker.map = mapView
        mapView.camera = camera
        //mapView.isMyLocationEnabled = true
        locationManager.stopUpdatingLocation()
        mapView.delegate = self
       //  mapView.settings.myLocationButton = true
       //  mapView.settings.compassButton = true
    }
    
    
    @IBAction func destinationAction(_ sender: Any) {
        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationController") as! SelectLocationController
        rideVc.pickUpPrimaryAddress = pickUpPrimaryAddress
        rideVc.dropPrimaryAddress = dropPrimaryAddress
        rideVc.pickUpLoaction = pickUpAddress1
        rideVc.dropLocation = dropAddress1
        rideVc.pickLatitude = pickUplatitude
        rideVc.pickLongitude = pickUpLongitude
        rideVc.delegate = self
        self.present(rideVc, animated: true, completion: nil)
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String,hint:String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                
            let places = self.themes.checkNullValue(placemarks) as! String
                
                if(places == ""){
                    
                }else{
                    let pm = placemarks! as [CLPlacemark]
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country)
                        print(pm.locality)
                        print(pm.subLocality)
                        print(pm.thoroughfare)
                        print(pm.postalCode)
                        print(pm.subThoroughfare)
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        print(addressString)
                        
                        
                        if(hint == "PickUp"){
                            self.pickUpPrimaryAddress = pm.subLocality!
                            
                            self.pickUpAddress1 = addressString
                        }else{
                            self.dropPrimaryAddress = pm.subLocality!
                            
                            self.dropAddress1 = addressString
                        }
                        
                        
                        //self.pickUpBtn.setTitle(addressString, for: .normal)
                    }
                }
                
               
        })
    }
    
    
     func locateWithLongitude(pickLatiTude: Double, picklongitude: Double, dropLatitude: Double, dropLongitude: Double, pickUpAddress: String, dropAddresss: String, pickUpMain: String, dropMain: String) {
        pickUpPrimaryAddress = pickUpMain
        dropPrimaryAddress = dropMain
        print(pickLatiTude,picklongitude,dropLatitude,dropLongitude)
        destinationlabel.text = pickUpAddress
        favouriteView.isHidden = true
        pickUpAddress1 = pickUpAddress
        dropAddress1 = dropAddresss
       self.mapView .clear()
      //  let pickUpPosition = CLLocationCoordinate2DMake(pickLatiTude, picklongitude)
        
        let dropPosition = CLLocationCoordinate2DMake(dropLatitude, dropLongitude)
 
        self.pickUpMarker = PlaceMarker(latitude: pickLatiTude, longitude: picklongitude, distance: 0.0, placeName: "pickUp")
        self.pickUpMarker.map = self.mapView
            
        
//        self.pickUpMarker.map = self.mapView
//        self.pickUpMarker.position = pickUpPosition
        self.dropMarker = PlaceMarker(latitude: dropLatitude, longitude: dropLongitude, distance: 0.0, placeName: "drop")
        self.dropMarker.map = self.mapView
//        self.dropMarker.map = self.mapView
//        self.dropMarker.position = dropPosition
        let camera = GMSCameraPosition.camera(withLatitude: pickUplatitude, longitude: pickUpLongitude, zoom: 13)
        self.mapView.camera = camera
        let origin = "\(pickUplatitude),\(pickUpLongitude)"
        let destination = "\(dropLatitude),\(dropLongitude)"
        self.drawPolyLine(origin: origin, destination: destination)
        
//        let view = Bundle.main.loadNibNamed("MarkerInfoView", owner: nil, options: nil)?.first as! MarkerInfoView
//
//        view.addressLabel.text = pickUpAddress
         self.getCarSelectionCheck()
    }
    func recentRideListAPI() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
             let userID =  themes.getUserId()
            let parameters = ["user_id" : userID] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:recentRideList, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                   
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        //let result = response["response"] as! String
                        print(response)
                       
                        
                        let list = response["response"]!["list"]  as! [AnyObject]
                        self.recentObject = list
                        
                        print("list is \(list)")
                       
                    self.favoutiteTableView.reloadData()
                    }
                    else{
                        self.themes.hideActivityIndicator(uiView: self.view)
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func getFavoriteListAPI() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
            let userID =  themes.getUserId()
            let parameters = ["user_id" : userID] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:getFavoriteLocation, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        //let result = response["response"] as! String
                        print(response)
                        self.favObject = response["locations"] as! [AnyObject]
                         self.favoutiteTableView.reloadData()
                    }
                    else{
                        self.themes.hideActivityIndicator(uiView: self.view)
                        let result = response["response"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }

    func drawPolyLine(origin:String,destination:String){
       
        
        //self.rideTypesUber(pickLatitude: self.pickUpMarker.position.latitude, pickLongitude: self.pickUpMarker.position.longitude, dropLatitude: self.dropMarker.position.latitude, dropLongitude: self.dropMarker.position.longitude)
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw"
        Alamofire.request(url).responseJSON { response in
            
            do{
            let json = try JSON(data: response.data!)
            let routes = json["routes"].arrayValue
            
            for route in routes
            {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                
                let polyline = GMSPolyline(path: path)
                //polyline.strokeColor = .black
                polyline.strokeWidth = 8.0
               // polyline.map = self.mapView
                
                
                let greenToRed = GMSStrokeStyle.gradient(from: UIColor(red: 81/255.0, green: 190/255.0, blue: 19/255.0, alpha: 1.0), to: UIColor(red: 6/255.0, green: 152/255.0, blue: 212/255.0, alpha: 1.0))
                polyline.spans = [GMSStyleSpan(style: greenToRed)]
                polyline.map = self.mapView
                
            }
                
            }catch{
                
            }
        }

    }
    
    func  getCarSelectionCheck(){
        let latitudeStr = "\(self.pickUpMarker.position.latitude)"
        let longitudeStr = "\(self.pickUpMarker.position.longitude)"
        let droplatitudeStr = "\(self.dropMarker.position.latitude)"
        let droplongitudeStr = "\(self.dropMarker.position.longitude)"
        if latitudeStr == "" && longitudeStr == "" && droplatitudeStr == "" && droplongitudeStr == "" {
        }else {
            getCarSelectionDetailsApi()
        }
    }
    
    func getCarSelectionDetailsApi() {
      //  self.uberTypeArray.removeAll()
        let latitudeStr = String(format:"%f",self.pickUpMarker.position.latitude)
        let longitudeStr = String(format:"%f",self.pickUpMarker.position.longitude)
        let droplatitudeStr = String(format:"%f",self.dropMarker.position.latitude)
        let droplongitudeStr = String(format:"%f",self.dropMarker.position.longitude)
        let userID =  themes.getUserId()
        let categoryStr = themes.getCategoryString()
        let parameters = ["user_id": userID,
                          "pickup_lat": latitudeStr,
                          "pickup_lon": longitudeStr,
                          "drop_lat": droplatitudeStr,
                          "drop_lon": droplongitudeStr,
                          "category": categoryStr]
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            urlService.serviceCallPostMethodWithParams(url:carsDetails_Taxi, params: parameters as! Dictionary<String, String>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                    if(isDead == "Yes"){
                        self.themes.hideActivityIndicator(uiView: self.view)
                        self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }else{
                        self.themes.hideActivityIndicator(uiView: self.view)
                        let success = response["status"] as! String
                        if(success == "1"){
                            self.rideTypeArray = response["response"]!["category"] as! [AnyObject]
                            let obj = self.rideTypeArray[0]
                            self.carNameLabel.text = obj.value(forKey: "name") as? String
                             self.carSelectionView.isHidden = false
                            self.carTypeCollectionView.reloadData()
//                            self.rideTypeTablview.isHidden = false
                        }
                        else{
                            
                            let result = response["response"] as! String
                            self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                        }
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
    }
    
    @IBAction func goBtnAction(_ sender: Any) {
        carSelectionView.isHidden = true
        confirmPickUpView.isHidden = false
        
     
    }
    @IBAction func pickUpConfirmAction(_ sender: Any) {
        bookRideApi()
    }
    func bookRideApi(){
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let DateString = formatter.string(from: Date())
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = NSTimeZone.system
        let TimeString = dateFormatter.string(from: now)
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            let latitudeStr = "\(self.pickUpMarker.position.latitude)"
            let longitudeStr = "\(self.pickUpMarker.position.longitude)"
            let droplatitudeStr = "\(self.dropMarker.position.latitude)"
            let droplongitudeStr = "\(self.dropMarker.position.longitude)"
            let parameters = [
                "user_id": themes.getUserId(),
                "pickup": pickUpAddress1,
                "drop_loc": dropAddress1,
                "pickup_lat": latitudeStr,
                "pickup_lon": longitudeStr,
                "drop_lat": droplatitudeStr,
                "drop_lon": droplongitudeStr,
                "category": themes.checkNullValue(CarCategoryString),
                "type": "0",
                "pickup_date": DateString,
                "pickup_time": TimeString /*"06:17 PM"; */,
                "code": "",
                "goods_type": "" /*goodsTitle, */,
                "goods_type_quanity": "" /*goodsType, */,
                "ride_id": "",
                "vehicle_type": themes.checkNullValue(nameofcar),
                "subcategory": themes.getSubCategoryString(),
                "note": driverNote
                ] as [String: AnyObject]
            
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:confirmRide, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    if(success == "1"){
                        print("book ride response is \(response)")
                        
                        let appDelagate = UIApplication.shared.delegate as? AppDelegate
                        appDelagate?.connectToXmpp()
                        
                        self.activityIndicator.startAnimating()
                        
                        self.confirmPickUpView.isHidden = true
                        
                        self.processingView.isHidden = false
                        let response1 = response["response"]
                        self.bookingRideId = response1!["ride_id"] as! String
                        
//                        let settingVc = self.storyboard?.instantiateViewController(withIdentifier: "DriverOnWayVc") as! DriverOnWayVc
//                        let response1 = response["response"]
//                        let seconds = response1!["response_time"] as! Int
//                        globalRideId = response1!["ride_id"] as! String
//                        cabType = "RideHub"
//                        settingVc.selectedSecs = seconds
//                        self.navigationController?.pushViewController(settingVc, animated: true)
                    }
                    else{
                        let result = response["response"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    func CancelRide(){
       
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
           
            let parameters = [
                "user_id": themes.getUserId(),
                "ride_id": bookingRideId,
                ] as [String: AnyObject]
            
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:deleteRide, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    if(success == "1"){
                        print("book ride response is \(response)")
                        
                        self.favouriteView.isHidden = false
                        self.processingView.isHidden = true
                        self.confirmPickUpView.isHidden = true
                        self.dropPrimaryAddress = ""
                        self.dropAddress1 = ""
                         self.slider.updateThumb()
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        
                        //        popOverVC.fromScreen = "Payment"
                        popOverVC.headingMesg = "Alert"
                        popOverVC.subMesg = "Your Ride cancelled successfully."
                        
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
                        
//                        let alert: UIAlertController = UIAlertController(title: "Alert", message: "Your Ride cancelled successfully.", preferredStyle: .alert)
//
//                        let action: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { _ in
//                            alert.dismiss(animated: true, completion: nil)
//                        }
//                        alert.addAction(action)
//                        self.present(alert, animated: true, completion: nil)
                    }
                        
                    else{
                        let result = response["response"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
         print("marker clickedddd")
        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationController") as! SelectLocationController
        rideVc.pickUpPrimaryAddress = pickUpPrimaryAddress
        rideVc.dropPrimaryAddress = dropPrimaryAddress
        rideVc.pickUpLoaction = pickUpAddress1
        rideVc.dropLocation = dropAddress1
        rideVc.pickLatitude = pickUplatitude
        rideVc.pickLongitude = pickUpLongitude
        rideVc.delegate = self
        self.present(rideVc, animated: true, completion: nil)
        return true
    }
   
    
    @IBAction func driverNoteAction(_ sender: Any) {
        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "NoteToDriverVC") as! NoteToDriverVC
        self.present(rideVc, animated: true, completion: nil)
        
    }
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        // Show the navigation bar on other view controllers
//        NotificationCenter.default.removeObserver(self)
//
//        // NotificationCenter.default.removeObserver("pushnotification")
//    }
   
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        // Show the navigation bar on other view controllers
//        NotificationCenter.default.removeObserver(self)
//    }
    /*
    // MARK: - Navigation0

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
class PlaceMarker: GMSMarker {
    //Initialize with lat and long, then set position equal to the coordinate.
    // 'position' comes from inheriting from GMSMarker, which is google marker.
    init(latitude: Double, longitude: Double, distance: Double, placeName: String) {
        super.init()
        if let lat: CLLocationDegrees = latitude,
            let long: CLLocationDegrees = longitude {
            let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            
          //  let position = CLLocationCoordinate2DMake(lat, lon)
            position = coordinate
        }
        
        let view = Bundle.main.loadNibNamed("MarkerInfoView", owner: nil, options: nil)?.first as! MarkerInfoView
   
        
        var themes = Themes()
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
      
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = latitude
        center.longitude = longitude
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                let places = themes.checkNullValue(placemarks) as! String
                
                if(places == ""){
              print("places not found")
                }else{
                    let pm = placemarks! as [CLPlacemark]
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        print(pm.country)
                        print(pm.locality)
                        print(pm.subLocality)
                        print(pm.thoroughfare)
                        print(pm.postalCode)
                        print(pm.subThoroughfare)
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        print(addressString)
                        // view.locationBtn.addTarget(self, action: #selector(self.locationBtnClicked(button:)), for: .touchUpInside)
                        if(placeName == "pickUp"){
                            let sublocality = themes.checkNullValue(pm.subLocality!) as! String
                            
                            
                            view.addressLabel.text = sublocality
                            view.imageview.image = UIImage(named: "pickUpMarkerIcon")
                            view.headingLabel.text = "PICKUP AT"
                            
                            view.betweenLine.backgroundColor = UIColor(red: 0/255.0, green: 115/255.0, blue: 187/255.0, alpha: 0.7)
                            
                            view.pointimageview.image = UIImage(named: "pickUP")
                            
                            view.dropMarkerBoderImageview.isHidden = true
                            
                        }else{
                            let sublocality = themes.checkNullValue(pm.subLocality!) as! String
                            
                            
                            view.addressLabel.text = sublocality
                            view.imageview.image = UIImage(named: "dropoff")
                            view.headingLabel.text = "DROP-OFF"
                            view.betweenLine.backgroundColor = UIColor(red: 81/255.0, green: 190/255.0, blue: 19/255.0, alpha: 0.7)
                            
                            view.pointimageview.image = UIImage(named: "drop")
                            
                            view.dropMarkerBoderImageview.isHidden = false
                        }
                        
                        
                        
                        //  self.pickUpAddress1 = addressString
                        
                        //self.pickUpBtn.setTitle(addressString, for: .normal)
                    }
                }
                
               
        })
        
 
        
        //view.pointView.layer.masksToBounds = true
        view.backView.layer.cornerRadius = view.backView.frame.height / 2
        view.backView.layer.borderWidth = 1
        view.backView.layer.borderColor = UIColor(red: 65/255.0, green: 144/255.0, blue: 198/255.0, alpha: 1.0).cgColor
        
        // Once your view is ready set iconView property coming from inheriting to
        // your view as following:
        
        iconView = view
        appearAnimation = .pop //not necessarily but looks nice.
    }
    let obj = HomeVC()
    @objc func locationBtnClicked(button: UIButton){
//        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "SelectLocationController") as! SelectLocationController
//
//        rideVc.pickUpPrimaryAddress = pickUpPrimaryAddress
//        rideVc.dropPrimaryAddress = dropPrimaryAddress
//        rideVc.pickUpLoaction = pickUpAddress1
//        rideVc.dropLocation = dropAddress1
//        rideVc.pickLatitude = pickUplatitude
//        rideVc.pickLongitude = pickUpLongitude
//        rideVc.delegate = self
//        self.present(rideVc, animated: true, completion: nil)
        
        obj.destinationAction(button)
        
        
    }
    
}
extension UIScrollView {
    func scrollToTop() {
        let desiredOffset = CGPoint(x: 0, y: -contentInset.top)
        setContentOffset(desiredOffset, animated: true)
    }
}
