//
//  FavouriteLocationCell.swift
//  RideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class FavouriteLocationCell: UITableViewCell {

    @IBOutlet weak var lineView: UIView!
    @IBOutlet var imageview: UIImageView!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
