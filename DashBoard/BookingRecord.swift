//
//  BookingRecord.swift
//  RideHubUser-Swift
//
//  Created by nagaraj  kumar on 11/02/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit

class BookingRecord: NSObject {
    
    var categoryID = ""
    var subCategoryID = ""
    var categoryName = ""
    var categoryETA = ""
    var currency = ""
    var note = ""
    var vehicletypes = ""
    var categorySubName = ""
    var amountafter_fare = ""
    var amountmin_fare = ""
    var amountother_fare = ""
    var after_fare_text = ""
    var min_fare_text = ""
    var other_fare_text = ""
    var rideEstimatePrice = ""
    var isSelected = false
    var couponCode = ""
    var normal_image = ""
    var active_Image = ""
    var booking_ID = ""
    var timinrloading = ""
    //subcat tonnes
    var tonnsubCategoryid = ""
    var subEta = ""
    var subId = ""
    var subName = ""
    
    

}
