//
//  LeftMenuTableViewCell.swift
//  RideHubProfile
//
//  Created by S s Vali on 12/17/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuIconView: UIView!
    
    @IBOutlet weak var menuImageView: UIImageView!
   
    @IBOutlet weak var menuNameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.menuIconView.layer.cornerRadius = (menuIconView.frame.size.height)/2
        self.menuImageView.layer.cornerRadius = (menuImageView.frame.size.height)/2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
