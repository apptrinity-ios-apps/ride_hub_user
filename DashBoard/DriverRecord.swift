//
//  DriverRecord.swift
//  RideHubUser-Swift
//
//  Created by INDOBYTES on 15/02/19.
//  Copyright © 2019 Indobytes. All rights reserved.
import UIKit
class DriverRecord: NSObject {
    var driver_Name = ""
    var car_Name = ""
    var car_Number = ""
    var latitude_driver: Double = 0.0
    var longitude_driver: Double = 0.0
    var driver_moblNumber = ""
    var ride_ID = ""
    var isCancel = false
    var driverImage = ""
    var latitude_User: Double = 0.0
    var longitude_User: Double = 0.0
    var reason = ""
    var id_Reason = ""
    var message = ""
    var eta = ""
    var rating = ""
    var status = ""
    var drop_latitude_User: Double = 0.0
    var drop_longitude_User: Double = 0.0
}
