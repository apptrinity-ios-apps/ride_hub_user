//
//  SelectLocationController.swift
//  RideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
protocol LocateOnTheMap1{
    func locateWithLongitude( pickLatiTude:Double,picklongitude:Double,dropLatitude:Double,dropLongitude:Double,pickUpAddress:String,dropAddresss:String,pickUpMain:String,dropMain:String)
    
    
    
    
}

class SelectLocationController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    var searchHint = Int()
    var delegate: LocateOnTheMap1!
    @IBOutlet var dropTextField: UITextField!
    @IBOutlet var pickUpTextField: UITextField!
    @IBOutlet var favouriteTableView: UITableView!
    @IBOutlet var pickUpDropSearchView: UIView!
    @IBOutlet var dropCancelView: UIView!
    
    @IBOutlet var pickCancelView: UIView!
    var pickUpPrimaryAddress = String()
     var dropPrimaryAddress = String()
    var pickLatitude = Double()
    var pickLongitude = Double()
    var dropLatitude = Double()
    var dropLongitude = Double()
    var pickUpLoaction = String()
    var dropLocation = String()
     var resultsTotalArray = [String]()
    var resultsPrimaryArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        searchHint = 0
        // Do any additional setup after loading the view.
        pickUpDropSearchView.layer.borderWidth = 1
        pickUpDropSearchView.layer.borderColor = UIColor.lightGray.cgColor
        pickUpDropSearchView.layer.cornerRadius = 15
        self.favouriteTableView.register(UINib(nibName: "FavouriteLocationCell", bundle: nil), forCellReuseIdentifier: "FavouriteLocationCell")
        pickUpTextField.text = pickUpLoaction
        dropTextField.text = dropLocation
        pickCancelView.layer.cornerRadius = pickCancelView.frame.height / 2
        dropCancelView.layer.cornerRadius = dropCancelView.frame.height / 2
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsTotalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavouriteLocationCell", for: indexPath) as! FavouriteLocationCell
        tableView.separatorColor = UIColor.clear
        cell.selectionStyle = .none
        cell.imageview.image = UIImage(named: "recentLocation")
        cell.nameLabel?.text = self.resultsPrimaryArray[indexPath.row]
        cell.addressLabel?.text = self.resultsTotalArray[indexPath.row]
        return cell
        
        
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
     func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath){
        
        if(searchHint == 1){
            pickUpTextField.text = self.resultsTotalArray[indexPath.row]
            pickUpPrimaryAddress = self.resultsPrimaryArray[indexPath.row]
            
            
            let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(self.resultsTotalArray[indexPath.row]) { (placemarks, error) in
                guard
                    let placemarks = placemarks,
                    let location = placemarks.first?.location
                    else {
                        return
                }
                self.pickLatitude = location.coordinate.latitude
                self.pickLongitude = location.coordinate.longitude
                
                if(self.pickUpTextField.text == "" || self.dropTextField.text == ""){
                    
                    
                }else{
                    self.delegate.locateWithLongitude(pickLatiTude: self.pickLatitude, picklongitude: self.pickLongitude, dropLatitude: self.dropLatitude, dropLongitude: self.dropLongitude, pickUpAddress: self.pickUpTextField.text!,dropAddresss: self.dropTextField.text!, pickUpMain: self.pickUpPrimaryAddress, dropMain: self.dropPrimaryAddress)
                }
            }
            
            
        }else if(searchHint == 2){
            dropTextField.text = self.resultsTotalArray[indexPath.row]
            dropPrimaryAddress = self.resultsPrimaryArray[indexPath.row]
            let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(self.resultsTotalArray[indexPath.row]) { (placemarks, error) in
                guard
                    let placemarks = placemarks,
                    let location = placemarks.first?.location
                    else {
                        return
                }
                self.dropLatitude = location.coordinate.latitude
                self.dropLongitude = location.coordinate.longitude
                if(self.pickUpTextField.text == "" || self.dropTextField.text == ""){
                    
                    
                }else{
                    self.delegate.locateWithLongitude(pickLatiTude: self.pickLatitude, picklongitude: self.pickLongitude, dropLatitude: self.dropLatitude, dropLongitude: self.dropLongitude, pickUpAddress: self.pickUpTextField.text!,dropAddresss: self.dropTextField.text!, pickUpMain: self.pickUpPrimaryAddress, dropMain: self.dropPrimaryAddress)
                }
                
                
            }
        }
        // 1
        
        // 2
        //        let urlpath = "https://maps.googleapis.com/maps/api/geocode/json?address=\
        
        if(pickUpTextField.text == "" || dropTextField.text == ""){
            
            
        }else{
           self.dismiss(animated: true, completion: nil)
        }
        
        
    }
    @IBAction func closeAction(_ sender: Any) {
        
        
        self.dismiss(animated: true, completion: nil)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if(textField == pickUpTextField){
            searchHint = 1
        }else{
           searchHint = 2
        }
        
        
        let placeClient = GMSPlacesClient()
     
        if let text = textField.text as NSString? {
            let txtAfterUpdate = text.replacingCharacters(in: range, with: string)
            placeClient.autocompleteQuery(txtAfterUpdate, bounds: nil, filter: nil)  {(results, error: Error?) -> Void in
                // NSError myerr = Error;
                print("Error @%",Error.self)
                
                self.resultsTotalArray.removeAll()
                self.resultsPrimaryArray.removeAll()
                if results == nil {
                    return
                }
                
                for result in results! {
                    if let result = result as? GMSAutocompletePrediction {
                        self.resultsTotalArray.append(result.attributedFullText.string)
                        self.resultsPrimaryArray.append(result.attributedPrimaryText.string)
                        
                        
                        
                        
                    }
                }
                
            }
        }
        
      
            self.favouriteTableView.reloadData()
            return true
    }
    @IBAction func clearPickUpLoaction(_ sender: Any) {
        
        pickUpTextField.text = ""
    }
    
    @IBAction func clearDropLocation(_ sender: Any) {
        dropTextField.text = ""
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
