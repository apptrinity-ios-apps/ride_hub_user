//
//  HeaderMenuTableViewCell.swift
//  RideHubProfile
//
//  Created by S s Vali on 12/17/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class HeaderMenuTableViewCell: UITableViewCell {

    @IBOutlet var subProfileImageView: UIImageView!
    @IBOutlet weak var driverProfView: UIView!
    @IBOutlet weak var headerBtn: UIButton!
    @IBOutlet weak var driverPicture: UIImageView!
    @IBOutlet weak var driverNameLbl: UILabel!
    @IBOutlet weak var driverLocationLbl: UILabel!    
    @IBOutlet weak var dotLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.driverProfView.layer.cornerRadius = (driverProfView.frame.size.height)/2
        self.driverPicture.layer.cornerRadius = (driverPicture.frame.size.height)/2
        // Initialization code
        subProfileImageView.layer.cornerRadius = subProfileImageView.frame.height/2
        subProfileImageView.layer.borderWidth = 2
        subProfileImageView.layer.borderColor = UIColor.white.cgColor
        
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        //        shapeLayer.strokeColor = (UIColor.init(red: 6, green: 152, blue: 212, alpha: 1) as! CGColor)
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        //        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
}
