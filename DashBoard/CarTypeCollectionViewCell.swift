//
//  CarTypeCollectionViewCell.swift
//  RideHub
//
//  Created by INDOBYTES on 24/10/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class CarTypeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet var memberImageview: UIImageView!
    @IBOutlet var backview: UIView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var membersLabel: UILabel!
    @IBOutlet var carPriceLabel: UILabel!
    @IBOutlet var carNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       backImageView.layer.cornerRadius = 15
        backImageView.clipsToBounds = true
        backview.layer.cornerRadius = 15
        backview.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        backview.layer.shadowOpacity = 0.5
        backview.layer.shadowRadius = 3
        backview.layer.masksToBounds = false
       // backview.layer.insertSublayer(backgroundLayer, at: 1)
        backview.layer.shadowColor = UIColor.gray.cgColor
    }
   

}

extension UIView {
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}
