//
//  TripDetailsVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 28/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import MessageUI
class TripDetailsVC: UIViewController,UITableViewDelegate,UITableViewDataSource,MFMailComposeViewControllerDelegate {
    var helpArray = ["I was involved in a accedent","Review my fare or fees","I last an item","My driver was unprofessional","My vehicle wasn't what i excepted","I had a different issue","I want to report a service animal issue","Critical Safety Responce line"]
    var receiptArray = ["Tips","Promotions","Payment Type","Ride Distance","Time Taken","Wait Time","Sub Total","Total"]
    var helpCategoryList = Array<AnyObject>()
    var buttonNumber = ""
    var pickprimaryAddress = String()
    var dropprimaryAddress = String()
    var rideStatus = String()
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    let themes = Themes()
    @IBOutlet weak var mailSubmitbtn: UIButton!
    @IBOutlet weak var mailTF: UITextField!
    @IBOutlet weak var mailInvoiceView: UIView!
    @IBOutlet weak var hideView: UIView!
    @IBOutlet weak var tripDetailsTableView: UITableView!
     var detail = [String : AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tripDetailsTableView.separatorColor = UIColor.clear
        rideStatus = detail["ride_status"] as! String
        buttonNumber = "0"
        tripDetailsTableView.delegate = self
        tripDetailsTableView.dataSource = self
        self.tripDetailsTableView.register(UINib(nibName: "MapTableViewCell", bundle: nil), forCellReuseIdentifier: "MapTableViewCell")
        self.tripDetailsTableView.register(UINib(nibName: "TripTableCell", bundle: nil), forCellReuseIdentifier: "TripTableCell")
        self.tripDetailsTableView.register(UINib(nibName: "DriverRateEditTableViewCell", bundle: nil), forCellReuseIdentifier: "DriverRateEditTableViewCell")
        self.tripDetailsTableView.register(UINib(nibName: "HelpHeaderTableCell", bundle: nil), forCellReuseIdentifier: "HelpHeaderTableCell")
        self.tripDetailsTableView.register(UINib(nibName: "HelpDetailsTableCell", bundle: nil), forCellReuseIdentifier: "HelpDetailsTableCell")
        self.tripDetailsTableView.register(UINib(nibName: "ReceiptDetailsTableCell", bundle: nil), forCellReuseIdentifier: "ReceiptDetailsTableCell")
        self.tripDetailsTableView.register(UINib(nibName: "TripDetailNeedHelpCell", bundle: nil), forCellReuseIdentifier: "TripDetailNeedHelpCell")
        mailInvoiceView.layer.cornerRadius = 20
        mailSubmitbtn.layer.cornerRadius = mailSubmitbtn.frame.size.height / 2
        mailTF.layer.cornerRadius = mailTF.frame.size.height / 2
        mailTF.layer.borderWidth = 1
        mailTF.layer.borderColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        let pickUp = detail["pickup"] as! [String : AnyObject]
        let drop = detail["drop"] as! [String : AnyObject]
        let pickUplatLong = pickUp["latlong"] as! [String : AnyObject]
        let droplatLong = drop["latlong"] as! [String : AnyObject]
        let pickUpLat = pickUplatLong["lat"] as? Double
        let pickUpLon = pickUplatLong["lon"] as? Double
        let dropLat = droplatLong["lat"] as? Double
        let dropLon = droplatLong["lon"] as? Double
        getAddressFromLatLon(pdblLatitude: pickUpLat!, withLongitude: pickUpLon!, hint: "1")
        getAddressFromLatLon(pdblLatitude: dropLat!, withLongitude: dropLon!, hint: "2")
        // Do any additional setup after loading the view.
        helpListApi()
    }
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if(rideStatus == "Cancelled"){
                return 3
            }else{
               return 5
            }
        } else if section == 1 {
            if buttonNumber == "0" {
              return self.helpCategoryList.count
            } else {
                return receiptArray.count
            }
        } else {
            return 0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let header = tableView.dequeueReusableCell(withIdentifier: "HelpHeaderTableCell") as! HelpHeaderTableCell
            header.helpBtn.addTarget(self, action: #selector(helpBtnClicked(button:)), for: .touchUpInside)
            header.receiptBtn.addTarget(self, action: #selector(receiptBtnClicked(button:)), for: .touchUpInside)
            if buttonNumber == "0"{
                header.helpBtn.setTitleColor(UIColor.init(red: 22/255, green: 48/255, blue: 61/255, alpha: 1.0), for: .normal)
                header.receiptView.isHidden = true
                header.receiptBtn.setTitleColor(UIColor.init(red: 22/255, green: 48/255, blue: 61/255, alpha: 0.3), for: .normal)
            } else {
                header.helpBtn.setTitleColor(UIColor.init(red: 22/255, green: 48/255, blue: 61/255, alpha: 0.3), for: .normal)
                header.helpView.isHidden = true
                header.receiptBtn.setTitleColor(UIColor.init(red: 22/255, green: 48/255, blue: 61/255, alpha: 1.0), for: .normal)
            }
            return header
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
             if indexPath.row == 0 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MapTableViewCell", for: indexPath) as! MapTableViewCell
                
                let pickUp = detail["pickup"] as! [String : AnyObject]
                let drop = detail["drop"] as! [String : AnyObject]
                let pickUplatLong = pickUp["latlong"] as! [String : AnyObject]
                 let droplatLong = drop["latlong"] as! [String : AnyObject]
//                let pickUpLat = pickUplatLong["lat"] as? Double
//                let pickUpLon = pickUplatLong["lon"] as? Double
//                let dropLat = droplatLong["lat"] as? Double
//                let dropLon = droplatLong["lon"] as? Double
                cell.carNameLabel.text = detail["cab_type"] as? String
                cell.carBrandLabel.text = detail["vehicle_model"] as? String
                cell.dateLabel.text = detail["ride_end_date"] as? String
                
                let fullimageUrl = themes.checkNullValue(detail["image"] as AnyObject) as! String
                if(fullimageUrl == ""){
                    cell.mapImageView.image = UIImage(named: "defaultmap")
                }else{
                    
                    let url = URL(string: mapsBaseUrl + fullimageUrl)
                    cell.mapImageView.load(url: url!)
                }
                
               
                
              
                 let fare = detail["fare"] as! [String : AnyObject]
                
                let price = themes.checkNullValue(fare["total_paid"] as? String) as! String
                cell.priceLabel.text = "$ " + price
                
            cell.selectionStyle = .none
            
        
        return cell
        } else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TripTableCell", for: indexPath) as! TripTableCell
            cell.addStopBtn.isHidden = true
            cell.dotLine.isHidden = true
            cell.tripLabelHeight.constant = 0
            cell.selectionStyle = .none
                let pickUp = detail["pickup"] as! [String : AnyObject]
                let drop = detail["drop"] as! [String : AnyObject]
                let pickUplatLong = pickUp["latlong"] as! [String : AnyObject]
                let droplatLong = drop["latlong"] as! [String : AnyObject]
                let pickUpLat = pickUplatLong["lat"] as? Double
                let pickUpLon = pickUplatLong["lon"] as? Double
                let dropLat = droplatLong["lat"] as? Double
                let dropLon = droplatLong["lon"] as? Double
              //  getAddressFromLatLon(pdblLatitude: pickUpLat!, withLongitude: pickUpLon!, hint: "1")
                // getAddressFromLatLon(pdblLatitude: dropLat!, withLongitude: dropLon!, hint: "2")
               // getAddressFromLatLon(pdblLatitude: dropLat!, withLongitude: dropLon!, hint: "")
                cell.pickUpPointLbl.text = pickUp["location"] as? String
                cell.dropPointLbl.text = drop["location"] as? String
                cell.pickUpLbl.text = pickprimaryAddress
                cell.dropLbl.text = dropprimaryAddress
                cell.pickUpTimeLbl.text = detail["ride_begin_time"] as? String
                cell.dropTimeLbl.text = detail["ride_end_time"] as? String
            return cell
        } else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DriverRateEditTableViewCell", for: indexPath) as! DriverRateEditTableViewCell
                
                cell.driverNameLbl.text = detail["driver_name"] as? String
                
                let driverImage = themes.checkNullValue(detail["driver_image"] as? String) as! String
                if(driverImage == ""){
                    cell.driverImg.image = UIImage(named: "user_default")
                }else{
                     let image = "https://ridehub.co/" + driverImage
                    let url = URL(string: image)
                    cell.driverImg.load(url: url!)
                }
                
                
                let rating = themes.checkNullValue(detail["driver_rating"] as? String) as! String
                
                
                if(rating == ""){
                    cell.star1.image = UIImage(named: "star_empty")
                    cell.star2.image = UIImage(named: "star_empty")
                    cell.star3.image = UIImage(named: "star_empty")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else{
                    DispatchQueue.main.async {
                        let rate = (self.detail["driver_rating"] as? String)!
                        let ratingDouble = Double(rate)
                        let ratingInt = Int(ratingDouble!)
                        if(ratingInt == 1){
                            cell.star1.image = UIImage(named: "star_filled")
                            cell.star2.image = UIImage(named: "star_empty")
                            cell.star3.image = UIImage(named: "star_empty")
                            cell.star4.image = UIImage(named: "star_empty")
                            cell.star5.image = UIImage(named: "star_empty")
                        }else if(ratingInt == 2){
                            cell.star1.image = UIImage(named: "star_filled")
                            cell.star2.image = UIImage(named: "star_filled")
                            cell.star3.image = UIImage(named: "star_empty")
                            cell.star4.image = UIImage(named: "star_empty")
                            cell.star5.image = UIImage(named: "star_empty")
                        }else if(ratingInt == 3){
                            cell.star1.image = UIImage(named: "star_filled")
                            cell.star2.image = UIImage(named: "star_filled")
                            cell.star3.image = UIImage(named: "star_filled")
                            cell.star4.image = UIImage(named: "star_empty")
                            cell.star5.image = UIImage(named: "star_empty")
                        }else if(ratingInt == 4){
                            cell.star1.image = UIImage(named: "star_filled")
                            cell.star2.image = UIImage(named: "star_filled")
                            cell.star3.image = UIImage(named: "star_filled")
                            cell.star4.image = UIImage(named: "star_filled")
                            cell.star5.image = UIImage(named: "star_empty")
                        }else if(ratingInt == 5){
                            cell.star1.image = UIImage(named: "star_filled")
                            cell.star2.image = UIImage(named: "star_filled")
                            cell.star3.image = UIImage(named: "star_filled")
                            cell.star4.image = UIImage(named: "star_filled")
                            cell.star5.image = UIImage(named: "star_filled")
                        }
                        
                    }
                    
                }
                
                
                
                
                
                
                
            cell.selectionStyle = .none
            return cell
        } else if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TripDetailNeedHelpCell", for: indexPath) as! TripDetailNeedHelpCell
                 tableView.separatorColor = UIColor.clear
                
                            if(rideStatus == "Booked"){
                
                                cell.nameLabel.text = "Cancel Ride"
                                cell.imageview.image = UIImage(named: "TripCancel")
                
                            }else if(rideStatus == "Onride"){
                               cell.nameLabel.text = "Track"
                                  cell.imageview.image = UIImage(named: "TripTrack")
                
                            }else if(rideStatus == "Completed"){
                                cell.nameLabel.text = "Mail Invoice"
                                 cell.imageview.image = UIImage(named: "mailInvoice")
                            }else if(rideStatus == "Finished"){
                                cell.nameLabel.text = "Payment"
                                cell.imageview.image = UIImage(named: "TripPayment")
                            }
                
                cell.selectionStyle = .none
                
                return cell
             }else if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "TripDetailNeedHelpCell", for: indexPath) as! TripDetailNeedHelpCell
                
                tableView.separatorColor = UIColor.clear
                if(rideStatus == "Booked"){
                    
                    cell.nameLabel.text = "Share Trip"
                    
                    cell.imageview.image = UIImage(named: "TripShare")
                    
                }else if(rideStatus == "Onride"){
                    cell.nameLabel.text = "Share Trip"
                     cell.imageview.image = UIImage(named: "TripShare")
                    
                }else if(rideStatus == "Completed"){
                    cell.nameLabel.text = "Report Issue"
                     cell.imageview.image = UIImage(named: "caution")
                }else if(rideStatus == "Finished"){
                    cell.nameLabel.text = "Share Trip"
                    cell.imageview.image = UIImage(named: "TripShare")
                }
                
                cell.selectionStyle = .none
                
                return cell
             }  else {
                
                
               
                
                
                
 //           let cell = tableView.dequeueReusableCell(withIdentifier: "DriverRateEditTableViewCell", for: indexPath) as! DriverRateEditTableViewCell
            
            return UITableViewCell()
            
          }
        } else {
            if buttonNumber == "0" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "HelpDetailsTableCell", for: indexPath) as! HelpDetailsTableCell
               let obj = self.helpCategoryList[indexPath.row]
                cell.helpLabel.text = (obj["category"] as! String)
                cell.selectionStyle = .none
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ReceiptDetailsTableCell", for: indexPath) as! ReceiptDetailsTableCell
                cell.selectionStyle = .none
                cell.receiptLabel.text = receiptArray[indexPath.row]
                if(indexPath.row == 0){
                    let fare = (detail["fare"] as AnyObject)
                    cell.receiptAmountLabel.text = (fare["tips_amount"] as! String)
                }
                else if(indexPath.row == 1){
                    cell.receiptAmountLabel.text = "$ 0"
                }
                else if(indexPath.row == 2){
                    cell.receiptAmountLabel.text = (detail["payment_type"] as! String)
                }else if(indexPath.row == 3){
                    let summary = (detail["summary"] as AnyObject)
                    
                    cell.receiptAmountLabel.text = (summary["ride_distance"] as! String)
                }else if(indexPath.row == 4){
                    let summary = (detail["summary"] as AnyObject)
                    cell.receiptAmountLabel.text = (summary["ride_duration"] as! String)
                }else if(indexPath.row == 5){
                    let summary = (detail["summary"] as AnyObject)
                    
                    cell.receiptAmountLabel.text = (summary["waiting_duration"] as! String)
                }else if(indexPath.row == 6){
                    let fare = (detail["fare"] as AnyObject)
                    
                    cell.receiptAmountLabel.text = (fare["total_bill"] as! String)
                }else if(indexPath.row == 7){
                    
                    let fare = (detail["fare"] as AnyObject)
                    
                    cell.receiptAmountLabel.text = (fare["grand_bill"] as! String)
                    
                }
                
                
                return cell
            }
            
        }
    }
    @IBAction func mailInvoiceCloseAction(_ sender: Any) {
        hideView.isHidden = true
        mailInvoiceView.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            if(indexPath.row == 3){
                if(rideStatus == "Booked"){
                    
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "CancelRideVC") as! CancelRideVC
                    rideVc.rideID = detail["ride_id"] as! String
                    self.navigationController?.pushViewController(rideVc, animated: true)
                    
                    
                    
                }else if(rideStatus == "Onride"){
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "DriverOnWayVc") as! DriverOnWayVc
                    rideVc.CheckStatus_Notify = true
                    rideVc.trackDetail = detail
                    rideVc.rideStatus = rideStatus
                    self.navigationController?.pushViewController(rideVc, animated: true)
                    
                }else if(rideStatus == "Completed"){
                    mailInvoiceView.isHidden = false
                    hideView.isHidden = false
                }else if(rideStatus == "Finished"){
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    rideVc.rideID = detail["ride_id"] as! String
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }
            }else if(indexPath.row == 4){
                if(rideStatus == "Booked"){
                    
                    let text = "I have an RideHub coupon worth $  for you.Sign up with my code  to ride free.Enjoy! https://itunes.apple.com/us/app/ridehubuser/id1361522193"
                    // set up activity view controller
                    let textToShare = [ text ]
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                    
                    // exclude some activity types from the list (optional)
                    activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                    
                    // present the view controller
                    self.present(activityViewController, animated: true, completion: nil)
                    
                }else if(rideStatus == "Onride"){
                    let text = "I have an RideHub coupon worth $  for you.Sign up with my code  to ride free.Enjoy! https://itunes.apple.com/us/app/ridehubuser/id1361522193"
                    // set up activity view controller
                    let textToShare = [ text ]
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                    
                    // exclude some activity types from the list (optional)
                    activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                    
                    // present the view controller
                    self.present(activityViewController, animated: true, completion: nil)
                    
                }else if(rideStatus == "Completed"){
                    let recipientEmail = "info@zoplay.com"
                    let subject = "Subject"
                    let body = "Message"
                    // Show default mail composer
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self
                        mail.setToRecipients([recipientEmail])
                        mail.setSubject(subject)
                        mail.setMessageBody(body, isHTML: false)
                        present(mail, animated: true)
                        
                        // Show third party email composer if default Mail app is not present
                    }
                }else if(rideStatus == "Finished"){
                    let text = "I have an RideHub coupon worth $  for you.Sign up with my code  to ride free.Enjoy! https://itunes.apple.com/us/app/ridehubuser/id1361522193"
                    // set up activity view controller
                    let textToShare = [ text ]
                    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                    activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                    
                    // exclude some activity types from the list (optional)
                    activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                    
                    // present the view controller
                    self.present(activityViewController, animated: true, completion: nil)
                }
            } 
        }else if(indexPath.section == 1){
            
            if(buttonNumber == "0"){
                
                let helpSubViewController = self.storyboard?.instantiateViewController(withIdentifier: "HelpSubViewController") as! HelpSubViewController
                let obj = helpCategoryList[indexPath.row]
                                helpSubViewController.question = (obj["category"] as! String)
                helpSubViewController.categoryId = obj["id"] as! String
                self.navigationController?.pushViewController(helpSubViewController, animated: true)
                
            }
        }
    
    }
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double,hint:String)
    {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
       // let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
       // let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = pdblLatitude
        center.longitude = pdblLongitude
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
               
                let pm = placemarks! as [CLPlacemark]
                if pm.count > 0 {
                    let pm = placemarks![0]
                    print(pm.country)
                    print(pm.locality)
                  var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    print(addressString)
                    
                    
                    let cell = self.tripDetailsTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as! TripTableCell

                    if(hint == "1"){
                         cell.pickUpLbl.text = pm.subLocality!
                         self.pickprimaryAddress = pm.subLocality!
                    }else{
                        cell.dropLbl.text = pm.subLocality!
                         self.dropprimaryAddress = pm.subLocality!
                    }
                   
                    
//
//                    self.pickUpAddress1 = addressString
                    //self.pickUpBtn.setTitle(addressString, for: .normal)
                }
        })
//        DispatchQueue.main.async {
//            self.tripDetailsTableView.reloadData()
//        }
//
        
    }
    
    
    
    func drawPolyLine(pickUpLat:Double, pickUpLon:Double, dropLat:Double, dropLon:Double,mapView:GMSMapView){
        let origin = "\(pickUpLat),\(pickUpLon)"
        let destination = "\(dropLat),\(dropLon)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw"
        Alamofire.request(url).responseJSON
            { response in
                if let JSON = response.result.value {
                    let mapResponse: [String: AnyObject] = JSON as! [String : AnyObject]
                    let routesArray = (mapResponse["routes"] as? Array) ?? []
                    let routes = (routesArray.first as? Dictionary<String, AnyObject>) ?? [:]
                    let overviewPolyline = (routes["overview_polyline"] as? Dictionary<String,AnyObject>) ?? [:]
                    
                    let polypoints  = (overviewPolyline["points"] as? String) ?? ""
                    let line  = polypoints
                    
                    
                    let path = GMSMutablePath(fromEncodedPath: line)
                    let polyline = GMSPolyline(path: path)
                    
                    polyline.strokeWidth = 4.0
                    polyline.strokeColor = UIColor(red: 34/255.0, green: 43/255.0, blue: 69/255.0, alpha: 1.0)
                    // polyline.map = self.mapView
                    
                    //                    let greenToRed = GMSStrokeStyle.gradient(from: UIColor(red: 81/255.0, green: 190/255.0, blue: 19/255.0, alpha: 1.0), to: UIColor(red: 6/255.0, green: 152/255.0, blue: 212/255.0, alpha: 1.0))
                    //                    polyline.spans = [GMSStyleSpan(style: greenToRed)]
                    polyline.map = mapView
                    
                    
                    
                    
                    
                    
                    
                    var bounds = GMSCoordinateBounds()
                    
                    for index in 1...path!.count() {
                        bounds = bounds.includingCoordinate(path!.coordinate(at: index))
                    }
                    
                    mapView.animate(with: GMSCameraUpdate.fit(bounds))
                    
                    
                    
                    //    self.addPolyLine(encodedString: line, pickUpLat: pickUpLat, pickLon: pickUpLon, dropLat: dropLat, dropLon: dropLon, mapView: mapView)
                }
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 56
        } else {
            return 0
        }
    }
    
    @IBAction func mailInvoiceSubmitAction(_ sender: Any) {
        
        if(mailTF.text == ""){
            self.themes.showAlert(title: "Alert", message: "Email is Mandatory", sender: self)
        }else if(!(isValidEmail(testStr: mailTF.text!))){
            themes.showAlert(title: "Alert", message: "Please Enter Valid Email", sender: self)
        }else{
            mailInvoiceAPI()
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    func mailInvoiceAPI() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
            let parameters = ["email" : mailTF.text ?? "","ride_id" : detail["ride_id"] as! String] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:mailInvoice, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.mailTF.resignFirstResponder()
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        //let result = response["response"] as! String
                        print(response)
                        
                        let message =  response["response"] as! String
                        self.themes.showAlert(title: "Success", message: message, sender: self)
                        self.mailInvoiceView.isHidden = true
                        self.hideView.isHidden = true
                    }
                    else{
                        self.themes.hideActivityIndicator(uiView: self.view)
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func helpListApi() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            //themes.showActivityIndicator(uiView: self.view)
            let parameters = [:] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:helpCategory, params: parameters as Dictionary<String, Any>) { response in
                self.themes.hideActivityIndicator(uiView: self.view)
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                  //  let response =  response["response"] as AnyObject
                    self.helpCategoryList = response["data"] as! [AnyObject]
                    
                    
                    self.tripDetailsTableView.reloadData()
                    
                    
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
        if indexPath.row == 0 {
            return 253
        } else if indexPath.row == 1 {
            return 173
        } else if indexPath.row == 2 {
            return 132
        } else {
          return 70
         }
      } else if indexPath.section == 1 {
            if buttonNumber == "0"{
                return UITableViewAutomaticDimension
            } else {
       return 44
           }
        } else {
            return 0
        }
    }
    
    @objc func helpBtnClicked(button : UIButton){
        buttonNumber = "0"
        tripDetailsTableView.reloadData()
    }
    
    @objc func receiptBtnClicked(button : UIButton){
        buttonNumber = "1"
        tripDetailsTableView.reloadData()
    }
    
}
