//
//  YourRidesTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 28/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit
import GoogleMaps

class YourRidesTableCell: UITableViewCell {
    
    @IBOutlet weak var star1: UIImageView!
    
    @IBOutlet weak var star2: UIImageView!
    
    @IBOutlet weak var star3: UIImageView!
    
   
    @IBOutlet weak var mapImageView: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    
    @IBOutlet weak var star5: UIImageView!
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var carNameLbl: UILabel!
    @IBOutlet weak var carCompanyLbl: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        
        shadowView(view: backView)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func shadowView(view:UIView){
        view.layer.cornerRadius = 15
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }
    
}
