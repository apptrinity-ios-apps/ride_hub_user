//
//  YourRidesViewController.swift
//  NewRideHub
//
//  Created by INDOBYTES on 28/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
class YourRidesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource {
    var urlService = URLservices.sharedInstance
    var themes = Themes()
    
    var selectIndex = Int()
    var CompleteArr = [AnyObject]()
    var UpComingArr = [AnyObject]()
    var CancelledArr = [AnyObject]()
    
    @IBOutlet weak var noDataLable: UILabel!
    @IBOutlet weak var YourRidesCollectionView: UICollectionView!
    @IBOutlet weak var YourRidesTableView: UITableView!
    @IBAction func closeAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        selectIndex = 0
        YourRidesTableView.delegate = self
        YourRidesTableView.dataSource = self
        YourRidesCollectionView.dataSource = self
        YourRidesCollectionView.delegate = self
        YourRidesTableView.separatorColor = .clear
        
        self.YourRidesCollectionView.register(UINib(nibName: "RidesCollectionCell", bundle: nil), forCellWithReuseIdentifier: "RidesCollectionCell")
        self.YourRidesTableView.register(UINib(nibName: "YourRidesTableCell", bundle: nil), forCellReuseIdentifier: "YourRidesTableCell")
        GetRideList()
        // Do any additional setup after loading the view.
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RidesCollectionCell", for: indexPath) as! RidesCollectionCell
        
        cell.ridesLabel.layer.cornerRadius = cell.ridesLabel.frame.size.height/2
        cell.ridesLabel.layer.borderWidth = 0.5
        cell.ridesLabel.layer.masksToBounds = true
        cell.ridesLabel.layer.borderColor = UIColor.lightGray.cgColor
        cell.ridesLabel.textColor = UIColor(red: 22/255, green: 48/255, blue: 61/255, alpha: 1)
        
        if indexPath.item == 0 {
            cell.ridesLabel.text = "Past"
            cell.ridesLabel.backgroundColor = UIColor(red: 3/255, green: 116/255, blue: 187/255, alpha: 1)
            cell.ridesLabel.textColor = UIColor.white
        } else if indexPath.item == 1 {
            cell.ridesLabel.text = "Upcoming"
        } else if indexPath.item == 2{
            cell.ridesLabel.text = "Cancelled"
        } else {
            
        }
        
        if selectIndex == indexPath.item {
            cell.ridesLabel.backgroundColor = UIColor(red: 3/255, green: 116/255, blue: 187/255, alpha: 1)
            cell.ridesLabel.textColor = UIColor.white
            return cell
        }else{
            cell.ridesLabel.backgroundColor = UIColor.clear
            cell.ridesLabel.textColor = UIColor(red: 22/255, green: 48/255, blue: 61/255, alpha: 1)
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectIndex = indexPath.item
        self.YourRidesCollectionView.reloadData()
        
        self.YourRidesTableView.reloadData()
        
    }
    
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
         return CGSize(width: screenWidth / 3 , height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
            return -15
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(selectIndex) {
        case 0:
            if(CompleteArr.count == 0){
                YourRidesTableView.isHidden = true
            }else{
                YourRidesTableView.isHidden = false
            }
            return CompleteArr.count
        case 1:
            if(UpComingArr.count == 0){
                YourRidesTableView.isHidden = true
            }else{
                YourRidesTableView.isHidden = false
            }
            return UpComingArr.count
        case 2:
            if(CancelledArr.count == 0){
                YourRidesTableView.isHidden = true
            }else{
                YourRidesTableView.isHidden = false
            }
            return CancelledArr.count
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "YourRidesTableCell", for: indexPath) as! YourRidesTableCell
        cell.selectionStyle = .none
        if(selectIndex == 0){
            
           
            
             let dict = CompleteArr[indexPath.row]
            let date = dict["ride_date"] as? String
            
             let rating = themes.checkNullValue(dict["driver_rating"] as? String) as! String
            
            if(rating == ""){
                cell.star1.image = UIImage(named: "star_empty")
                cell.star2.image = UIImage(named: "star_empty")
                cell.star3.image = UIImage(named: "star_empty")
                cell.star4.image = UIImage(named: "star_empty")
                cell.star5.image = UIImage(named: "star_empty")
            }else{
               DispatchQueue.main.async {
                let rate = (dict["driver_rating"] as? String)!
                 let ratingDouble = Double(rate)
                let ratingInt = Int(ratingDouble!)
                if(ratingInt == 1){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_empty")
                    cell.star3.image = UIImage(named: "star_empty")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 2){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_empty")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 3){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 4){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_filled")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 5){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_filled")
                    cell.star5.image = UIImage(named: "star_filled")
                }

            }

            }
            
            
            
            
            let time = dict["ride_time"] as? String
            let pickUpLat = dict["pickup_lat"] as? Double
            let pickUpLon = dict["pickup_long"] as? Double
            let dropLat = dict["drop_lat"] as? Double
            let dropLon = dict["drop_long"] as? Double
//            let pickUpMarker = GMSMarker()
//            let dropMarker = GMSMarker()
//            dropMarker.position = CLLocationCoordinate2D(latitude: dropLat!, longitude: dropLon!)
//            dropMarker.appearAnimation = GMSMarkerAnimation.none
//            let markerIcon3 = UIImage(named: "pin") /// 16th
//            dropMarker.icon = markerIcon3
//            dropMarker.map = cell.mapview
//            pickUpMarker.position = CLLocationCoordinate2D(latitude: pickUpLat!, longitude: pickUpLon!)
//            pickUpMarker.appearAnimation = GMSMarkerAnimation.none
//            let markerIcon2 = UIImage(named: "pin") /// 16th
//            pickUpMarker.icon = markerIcon2
//            pickUpMarker.map = cell.mapview
//          
//           // cell.mapview.camera = GMSCameraPosition(target: pickUpMarker.position, zoom: 15, bearing: 0, viewingAngle: 0)
//            
//            drawPolyLine(pickUpLat: pickUpLat!, pickUpLon: pickUpLon!, dropLat: dropLat!, dropLon: dropLon!, mapView: cell.mapview)
            
            
            let fullimageUrl = themes.checkNullValue(dict["image"] as AnyObject) as! String
            if(fullimageUrl == ""){
                cell.mapImageView.image = UIImage(named: "defaultmap")
            }else{
                
                let url = URL(string: mapsBaseUrl + fullimageUrl)
                cell.mapImageView.load(url: url!)
            }
            
            
            cell.dateLabel.text = date! + "," + time!
            
           
            let fare = self.themes.checkNullValue(dict["total_fare"] as AnyObject) as! String
            cell.costLabel.text = "$ " + fare
         
            cell.carCompanyLbl.text = dict["vehicle_model"] as? String
             cell.carNameLbl.text = dict["cab_type"] as? String
            return cell
        }else if(selectIndex == 1){
            let dict = UpComingArr[indexPath.row]
            let rating = themes.checkNullValue(dict["driver_rating"] as? String) as! String
            
            if(rating == ""){
                cell.star1.image = UIImage(named: "star_empty")
                cell.star2.image = UIImage(named: "star_empty")
                cell.star3.image = UIImage(named: "star_empty")
                cell.star4.image = UIImage(named: "star_empty")
                cell.star5.image = UIImage(named: "star_empty")
            }else{
                let rate = (dict["driver_rating"] as? String)!
                let ratingDouble = Double(rate)
                let ratingInt = Int(ratingDouble!)
                
                if(ratingInt == 1){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_empty")
                    cell.star3.image = UIImage(named: "star_empty")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 2){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_empty")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 3){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 4){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_filled")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 5){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_filled")
                    cell.star5.image = UIImage(named: "star_filled")
                }
                
                
                
            }
            
            let date = dict["ride_date"] as? String
            let time = dict["ride_time"] as? String
            cell.dateLabel.text = date! + "," + time!
            
            let fare = self.themes.checkNullValue(dict["total_fare"] as AnyObject) as! String
            cell.costLabel.text = "$ " + fare
            cell.carCompanyLbl.text = dict["vehicle_model"] as? String
            cell.carNameLbl.text = dict["cab_type"] as? String
            return cell
        }else {
             let dict = CancelledArr[indexPath.row]
            let rating = themes.checkNullValue(dict["driver_rating"] as? String) as! String
            
            if(rating == ""){
                cell.star1.image = UIImage(named: "star_empty")
                cell.star2.image = UIImage(named: "star_empty")
                cell.star3.image = UIImage(named: "star_empty")
                cell.star4.image = UIImage(named: "star_empty")
                cell.star5.image = UIImage(named: "star_empty")
            }else{
                let rate = (dict["driver_rating"] as? String)!
                let ratingDouble = Double(rate)
                let ratingInt = Int(ratingDouble!)
                
                if(ratingInt == 1){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_empty")
                    cell.star3.image = UIImage(named: "star_empty")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 2){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_empty")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 3){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_empty")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 4){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_filled")
                    cell.star5.image = UIImage(named: "star_empty")
                }else if(ratingInt == 5){
                    cell.star1.image = UIImage(named: "star_filled")
                    cell.star2.image = UIImage(named: "star_filled")
                    cell.star3.image = UIImage(named: "star_filled")
                    cell.star4.image = UIImage(named: "star_filled")
                    cell.star5.image = UIImage(named: "star_filled")
                }
                
                
                
            }
            
           
            let date = dict["ride_date"] as? String
            let time = dict["ride_time"] as? String
            cell.dateLabel.text = date! + "," + time!
           
            let fare = self.themes.checkNullValue(dict["total_fare"] as AnyObject) as! String
            cell.costLabel.text = "$ " + fare
            cell.carCompanyLbl.text = dict["vehicle_model"] as? String
            cell.carNameLbl.text = dict["cab_type"] as? String
            return cell
        }
       
    }
    func drawPolyLine(pickUpLat:Double, pickUpLon:Double, dropLat:Double, dropLon:Double,mapView:GMSMapView){
        let origin = "\(pickUpLat),\(pickUpLon)"
        let destination = "\(dropLat),\(dropLon)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyCUjZuD0jxWOFVkrWdqp3NEMhJQAcejxMw"
        Alamofire.request(url).responseJSON
            { response in
                if let JSON = response.result.value {
                    let mapResponse: [String: AnyObject] = JSON as! [String : AnyObject]
                    let routesArray = (mapResponse["routes"] as? Array) ?? []
                    let routes = (routesArray.first as? Dictionary<String, AnyObject>) ?? [:]
                    let overviewPolyline = (routes["overview_polyline"] as? Dictionary<String,AnyObject>) ?? [:]
                    
                    
                    
                    
                    
                  
                    
                    
                    
                    let polypoints  = (overviewPolyline["points"] as? String) ?? ""
                    let line  = polypoints
                    
                    
                    let path = GMSMutablePath(fromEncodedPath: line)
                    let polyline = GMSPolyline(path: path)
                    
                    polyline.strokeWidth = 4.0
                    polyline.strokeColor = UIColor(red: 34/255.0, green: 43/255.0, blue: 69/255.0, alpha: 1.0)
                    // polyline.map = self.mapView
                    
//                    let greenToRed = GMSStrokeStyle.gradient(from: UIColor(red: 81/255.0, green: 190/255.0, blue: 19/255.0, alpha: 1.0), to: UIColor(red: 6/255.0, green: 152/255.0, blue: 212/255.0, alpha: 1.0))
//                    polyline.spans = [GMSStyleSpan(style: greenToRed)]
                    polyline.map = mapView
                    
                    
                    
                   
                    
                    
                    
                    var bounds = GMSCoordinateBounds()
                    
                  
//                    if(path!.count == 0){
//
//                    }else{
                        for index in 1...path!.count() {
                            bounds = bounds.includingCoordinate(path!.coordinate(at: index))
                        }
                        mapView.animate(with: GMSCameraUpdate.fit(bounds))
//                    }
                    
                        
                    
                   
                    
                    
                    
                    
                //    self.addPolyLine(encodedString: line, pickUpLat: pickUpLat, pickLon: pickUpLon, dropLat: dropLat, dropLon: dropLon, mapView: mapView)
                }
        }
    }
    func addPolyLine(encodedString: String,pickUpLat:Double,pickLon:Double,dropLat:Double,dropLon:Double,mapView:GMSMapView) {
        
        // driverPolyline.map = nil
       
       
      
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        var ride_id = String()
        if(selectIndex == 0){
           
                let result = CompleteArr[indexPath.row]
                ride_id = result["ride_id"] as! String
                retrieveMyRideslist(id: ride_id)
            
            
            
            
            
        }else if(selectIndex == 1){
            let result = UpComingArr[indexPath.row]
            ride_id = result["ride_id"] as! String
            retrieveMyRideslist(id: ride_id)
            
            
            
        }else if(selectIndex == 2){
            let result = CancelledArr[indexPath.row]
            ride_id = result["ride_id"] as! String
            retrieveMyRideslist(id: ride_id)
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
        
    }
    func retrieveMyRideslist(id:String){
        themes.showActivityIndicator(uiView: self.view)
        let parameters = [ "user_id": themes.getUserId(), "ride_id": id] as [String : Any]
        print("parameters is \(parameters)")
        urlService.serviceCallPostMethodWithParams(url:viewRide, params: parameters as Dictionary<String, Any>) { response in
            //            print(response)
            let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
            if(isDead == "Yes"){
                self.themes.hideActivityIndicator(uiView: self.view)
                self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(rideVc, animated: true)
            }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let some =  response["response"] as! [String : AnyObject]
                //            print(some)
                let details = some["details"] as! [String : AnyObject]
                //            print(self.totalRideArr)
                let ridevc = self.storyboard?.instantiateViewController(withIdentifier: "TripDetailsVC") as! TripDetailsVC
                ridevc.detail = details
                self.navigationController?.pushViewController(ridevc, animated: true)
            }
        }
    }
    func GetRideList(){
        themes.showActivityIndicator(uiView: self.view)
        let parameters = [ "user_id": themes.getUserId() , "type": "all" ] as [String : Any]
        urlService.serviceCallPostMethodWithParams(url:myRide, params: parameters as Dictionary<String, Any>) { response in
            //            print(response)
            let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
            if(isDead == "Yes"){
                self.themes.hideActivityIndicator(uiView: self.view)
                self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.navigationController?.pushViewController(rideVc, animated: true)
            }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                
               
               
                
                let some =  response["response"] as! [String : AnyObject]
                //            print(some)
                let ridesArray = some["rides"] as! [AnyObject]
                //            print(self.totalRideArr)
                
                
                
                for index in 0..<ridesArray.count {
                    // print(self.totalRideArr.count)
                    let cmpltArry = ridesArray[index] as? AnyObject
                    print(cmpltArry)
                    let statArr = cmpltArry!["ride_status"] as? String
                    //             print(statArr)
                    
                    var dict = [String:AnyObject]()
                    if statArr == "Completed" {
                        
                     //   dict = ["pickup": cmpltArry!["pickup"] as? String, "ride_date": cmpltArry!["ride_date"] as? String, "ride_id": cmpltArry!["ride_id"] as? String, "ride_time": cmpltArry!["ride_time"] as? String, "group": cmpltArry!["group"] as? String, "ride_status": cmpltArry!["ride_status"] as? String, "datetime": cmpltArry!["datetime"] as? String, "upcoming": cmpltArry!["upcoming"] as? String,"total_fare":cmpltArry!["total_fare"] as? String,"vehicle_model":cmpltArry!["vehicle_model"] as? String,"vehicle_model":cmpltArry!["vehicle_model"] as? String] as [String : AnyObject]
                        self.CompleteArr.append(cmpltArry as AnyObject)
  
                        // self.CompleteArr = self.totalRideArr
                        //                   print(self.CompleteArr)
                        
                        // statArr == "Onride" || statArr == "Booked"
                    } else if statArr == "Upcoming" || statArr == "Onride" || statArr == "Booked"{
                        
                       // dict = ["pickup": cmpltArry!["pickup"] as? String, "ride_date": cmpltArry!["ride_date"] as? String, "ride_id": cmpltArry!["ride_id"] as? String, "ride_time": cmpltArry!["ride_time"] as? String, "group": cmpltArry!["group"] as? String, "ride_status": cmpltArry!["ride_status"] as? String, "datetime": cmpltArry!["datetime"] as? String, "upcoming": cmpltArry!["upcoming"] as? String] as [String : AnyObject]
                        self.UpComingArr.append(cmpltArry as AnyObject)
                    }else if(statArr == "Cancelled"){
                      //  dict = ["pickup": cmpltArry!["pickup"] as? String, "ride_date": cmpltArry!["ride_date"] as? String, "ride_id": cmpltArry!["ride_id"] as? String, "ride_time": cmpltArry!["ride_time"] as? String, "group": cmpltArry!["group"] as? String, "ride_status": cmpltArry!["ride_status"] as? String, "datetime": cmpltArry!["datetime"] as? String, "upcoming": cmpltArry!["upcoming"] as? String] as [String : AnyObject]
                        self.CancelledArr.append(cmpltArry as AnyObject)
                    }
                }
                
                print(self.CompleteArr.count)
                print(self.UpComingArr.count)
                self.YourRidesTableView.reloadData()
            }
        }
    }
  
    
}
