//
//  RatingVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class RatingVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
    let urlService = URLservices.sharedInstance
    var themes = Themes()
    var starArr = ""
    var rideID = String()
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var blueView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingTableVIew: UITableView!
    
    
    var selectedHintArray = Array<String>()
    var reviewOptionsArray = Array<AnyObject>()
   
    var commentText = ""
    
    let namesArray = ["Mr.Frank"]
    var pickUpAr = ""
    var clearCarAr = ""
    var courtousArr = ""
    var smartDrivAr = ""
    var commentArr = ""
    var selectedRatingArray = Array<AnyObject>()
    
    @IBAction func closeAction(_ sender: Any) {
        cabArrivedHint = false
        requestPaymentHint = false
        ratingHint = false
        let dashVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")as? HomeVC
        self.navigationController?.pushViewController(dashVc!, animated: true)
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        ratingView.layer.cornerRadius = 15
        ratingTableVIew.layer.cornerRadius = 15
        ratingTableVIew.delegate = self
        ratingTableVIew.dataSource = self
        ratingTableVIew.separatorColor = .none
        shadowView(view: ratingView)
        NotificationCenter.default.addObserver(self, selector: #selector(FareBreakupVC.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FareBreakupVC.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.ratingTableVIew.register(UINib(nibName: "DriverProfileTableCell", bundle: nil), forCellReuseIdentifier: "DriverProfileTableCell")
        self.ratingTableVIew.register(UINib(nibName: "RatingTableCell", bundle: nil), forCellReuseIdentifier: "RatingTableCell")
        
         self.ratingTableVIew.register(UINib(nibName: "RatingRateCell", bundle: nil), forCellReuseIdentifier: "RatingRateCell")
        
        optionslistApi()
        
        // Do any additional setup after loading the view.
    }
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            // self.view.frame.origin.y = 200
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y = -200
            }
        }
    }
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
          return self.reviewOptionsArray.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if(indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "DriverProfileTableCell", for: indexPath)as! DriverProfileTableCell
            cell.selectionStyle = .none
            //        cell.pickOneBtn?.addTarget(self, action:#selector(pickUpBtnClick(_:)), for:.touchUpInside)
//
//            Record_Driver.driver_Name = object["key2"] as! String
//            Record_Driver.car_Name = object["key12"] as! String
//            Record_Driver.car_Number = object["key11"] as! String
//            Record_Driver.latitude_driver = Double(object["key6"] as! String)!
//            Record_Driver.longitude_driver = Double(object["key7"] as! String)!
//            Record_Driver.driver_moblNumber = object["key10"] as! String
//            Record_Driver.eta = object["key8"] as! String
//            Record_Driver.message = object["message"] as! String
//            Record_Driver.latitude_User = (object["key14"] as? NSNumber)?.doubleValue ?? 0.0
//            Record_Driver.longitude_User = (object["key15"] as? NSNumber)?.doubleValue ?? 0.0
//            Record_Driver.ride_ID = object["key9"] as! String
//            Record_Driver.rating = object["key5"] as! String
//            Record_Driver.driverImage = object["key4"] as! String
            let fullimageUrl = themes.checkNullValue(URL(string: driverDetails["key4"] as! String)) as! String
            
            if(fullimageUrl == ""){
                cell.driverImgView.image = UIImage(named: "user_default")
            }else{
                let url = URL(string: driverDetails["key4"] as! String)
                cell.driverImgView.load(url: url!)
            }
            cell.driverNameLbl.text = (driverDetails["key2"] as! String)
            
            return cell
        }else if(indexPath.row == self.reviewOptionsArray.count + 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingTableCell", for: indexPath)as! RatingTableCell
            cell.submitRiviewBtn?.addTarget(self, action:#selector(finishBtnClick(_:)), for:.touchUpInside)
            cell.commentsTextView.delegate = self
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RatingRateCell", for: indexPath)as! RatingRateCell
            let obj = reviewOptionsArray[indexPath.row - 1]
            cell.rating1Btn.tag = indexPath.row - 1
            cell.rating2Btn.tag = indexPath.row - 1
            cell.rating3Btn.tag = indexPath.row - 1
            cell.rating4Btn.tag = indexPath.row - 1
            cell.rating5Btn.tag = indexPath.row - 1
            cell.nameLabel.text = obj["option_title"] as? String
            cell.rating1Btn.addTarget(self, action:#selector(rating1BtnClick(_:)), for:.touchUpInside)
          
            cell.rating2Btn.addTarget(self, action:#selector(rating2BtnClick(_:)), for:.touchUpInside)
            cell.rating3Btn?.addTarget(self, action:#selector(rating3BtnClick(_:)), for:.touchUpInside)
            cell.rating4Btn?.addTarget(self, action:#selector(rating4BtnClick(_:)), for:.touchUpInside)
            cell.rating5Btn?.addTarget(self, action:#selector(rating5BtnClick(_:)), for:.touchUpInside)
            cell.selectionStyle = .none
            return cell
        }
    }
    @objc func finishBtnClick(_ sender: AnyObject) {
        if(selectedRatingArray.count == reviewOptionsArray.count){
            RideReview()
        }else{
            themes.showAlert(title: "Alert", message: "Please give all fields", sender: self)
        }
    }
    @objc func rating1BtnClick(_ sender: UIButton) {
        // let title =
        
        sender.rotate360Degrees()
        let buttonPosition = sender.convert(CGPoint(), to:ratingTableVIew)
        let indexPath = ratingTableVIew.indexPathForRow(at:buttonPosition)
        let obj = reviewOptionsArray[indexPath!.row - 1]
        let dict = ["option_title":obj["option_title"] as AnyObject,"option_id":obj["option_id"] as AnyObject,"rating":"1"] as [String : Any]
        //        if let index = selectedHintArray.index(of: obj["option_id"] as AnyObject) {
        //            selectedHintArray.remove(at: index)
        //        }
        if selectedHintArray.contains(obj["option_id"] as! String) {
            print("yes")
            let index = selectedHintArray.index(of: obj["option_id"] as! String)
            selectedHintArray.remove(at: index!)
            selectedRatingArray.remove(at: index!)
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }else{
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }
    }
    @objc func rating2BtnClick(_ sender: UIButton) {
       // sender.rotate360Degrees()
        let buttonPosition = sender.convert(CGPoint(), to:ratingTableVIew)
        let indexPath = ratingTableVIew.indexPathForRow(at:buttonPosition)
        let obj = reviewOptionsArray[indexPath!.row - 1]
        let dict = ["option_title":obj["option_title"] as AnyObject,"option_id":obj["option_id"] as AnyObject,"rating":"2"] as [String : Any]
        

        
        
        let cell = ratingTableVIew.cellForRow(at: indexPath!) as! RatingRateCell
        
        cell.rating1Btn.rotate360Degrees()
        cell.rating2Btn.rotate360Degrees()
        
        
        
        if selectedHintArray.contains(obj["option_id"] as! String) {
            print("yes")
            let index = selectedHintArray.index(of: obj["option_id"] as! String)
            selectedHintArray.remove(at: index!)
            selectedRatingArray.remove(at: index!)
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }else{
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }
    }
    @objc func rating3BtnClick(_ sender: UIButton) {
        //sender.rotate360Degrees()
        
        
        let buttonPosition = sender.convert(CGPoint(), to:ratingTableVIew)
        let indexPath = ratingTableVIew.indexPathForRow(at:buttonPosition)
        let obj = reviewOptionsArray[indexPath!.row - 1]
        let dict = ["option_title":obj["option_title"] as AnyObject,"option_id":obj["option_id"] as AnyObject,"rating":"3"] as [String : Any]
        
        
        let cell = ratingTableVIew.cellForRow(at: indexPath!) as! RatingRateCell
        
        cell.rating1Btn.rotate360Degrees()
        cell.rating2Btn.rotate360Degrees()
        cell.rating3Btn.rotate360Degrees()
        if selectedHintArray.contains(obj["option_id"] as! String) {
            print("yes")
            let index = selectedHintArray.index(of: obj["option_id"] as! String)
            selectedHintArray.remove(at: index!)
            selectedRatingArray.remove(at: index!)
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }else{
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }
    }
    @objc func rating4BtnClick(_ sender: UIButton) {
      //  sender.rotate360Degrees()
        let buttonPosition = sender.convert(CGPoint(), to:ratingTableVIew)
        let indexPath = ratingTableVIew.indexPathForRow(at:buttonPosition)
        let obj = reviewOptionsArray[indexPath!.row - 1]
        let dict = ["option_title":obj["option_title"] as AnyObject,"option_id":obj["option_id"] as AnyObject,"rating":"4"] as [String : Any]
        
        let cell = ratingTableVIew.cellForRow(at: indexPath!) as! RatingRateCell
        
        cell.rating1Btn.rotate360Degrees()
        cell.rating2Btn.rotate360Degrees()
        cell.rating3Btn.rotate360Degrees()
        cell.rating4Btn.rotate360Degrees()
        
        
        if selectedHintArray.contains(obj["option_id"] as! String) {
            print("yes")
            
            let index = selectedHintArray.index(of: obj["option_id"] as! String)
            selectedHintArray.remove(at: index!)
            selectedRatingArray.remove(at: index!)
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }else{
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }
        
    }
    @objc func rating5BtnClick(_ sender: UIButton) {
        sender.rotate360Degrees()
        let buttonPosition = sender.convert(CGPoint(), to:ratingTableVIew)
        let indexPath = ratingTableVIew.indexPathForRow(at:buttonPosition)
        let obj = reviewOptionsArray[indexPath!.row - 1]
        let dict = ["option_title":obj["option_title"] as AnyObject,"option_id":obj["option_id"] as AnyObject,"rating":"5"] as [String : Any]
        let cell = ratingTableVIew.cellForRow(at: indexPath!) as! RatingRateCell
        cell.rating1Btn.rotate360Degrees()
        cell.rating2Btn.rotate360Degrees()
        cell.rating3Btn.rotate360Degrees()
        cell.rating4Btn.rotate360Degrees()
         cell.rating5Btn.rotate360Degrees()
        if selectedHintArray.contains(obj["option_id"] as! String) {
            print("yes")
            
            let index = selectedHintArray.index(of: obj["option_id"] as! String)
            selectedHintArray.remove(at: index!)
            selectedRatingArray.remove(at: index!)
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }else{
            selectedRatingArray.append(dict as AnyObject)
            selectedHintArray.append(obj["option_id"] as! String)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0){
            return 185
        }else if(indexPath.row == self.reviewOptionsArray.count + 1){
            return 256
        }else{
            return 68
        }
    }
    
    func shadowView(view:UIView){
        view.layer.cornerRadius = 15
        view.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }
    func RideReview(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["comments" : commentText,
                              "ratingsFor" : "driver",
                              "ride_id" : rideID,"ratings":selectedRatingArray] as [String : Any]
            print("Ride Review parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParamsWithOutConversion(url:RateSubmit, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    if(success == "1"){
                        cabArrivedHint = false
                        requestPaymentHint = false
                        ratingHint = false
                        let thankyouVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                        self.navigationController?.pushViewController(thankyouVc, animated: true)
                        let appDelagate = UIApplication.shared.delegate as? AppDelegate
                        appDelagate?.GetAppInfo()
                    }
                    else{
                        
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func optionslistApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["optionsFor" : "driver",
                              "ride_id" : rideID] as [String : Any]
            
            print("Ride Review parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:optionslist, params: parameters as Dictionary<String, Any>) { response in
                print("option list response is \(response)")
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    if(success == "1"){
                        self.reviewOptionsArray = response["review_options"] as! [AnyObject]
                        let image = response["driver_image"] as! String
                        let driverName = response["driver_name"] as! String
                        
                        let cell = self.ratingTableVIew.cellForRow(at: IndexPath(row: 0, section: 0)) as! DriverProfileTableCell
                        cell.driverImgView.image = UIImage()
                        cell.driverImgView.image = UIImage(url: URL(string: image))
                        cell.driverNameLbl.text = driverName
                        self.ratingTableVIew.reloadData()
                    }
                    else{
                        
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Additional comments...."
            textView.textColor = UIColor.lightGray
        }else{
                commentText = textView.text!
                print(commentText)
        }
    }
    @objc func starBtnClicked(button: UIButton){
        switch button.tag {
        case 1:
            starArr = "1"
        case 2:
            starArr = "2"
        case 3:
            starArr = "3"
        case 4:
            starArr = "4"
        case 5:
            starArr = "5"
        default:
            print("0")
            return
        }
    }
}
extension UIImage {
    convenience init?(url: URL?) {
        guard let url = url else { return nil }
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch {
            print("Cannot load image from url: \(url) with error: \(error)")
            return nil
        }
    }
}
extension UIButton {
    
    func rotate360Degrees(duration: CFTimeInterval = 0.8, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(.pi * 2.0)
        rotateAnimation.duration = duration
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    
}
