//
//  RatingRateCell.swift
//  RideHubUser-Swift
//
//  Created by INDOBYTES on 05/08/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit

class RatingRateCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rating5Btn: UIButton!
    @IBOutlet weak var rating4Btn: UIButton!
    @IBOutlet weak var rating3Btn: UIButton!
    @IBOutlet weak var rating2Btn: UIButton!
    @IBOutlet weak var rating1Btn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
