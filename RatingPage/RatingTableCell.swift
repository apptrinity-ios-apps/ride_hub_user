//
//  RatingTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 21/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class RatingTableCell: UITableViewCell {
    
    @IBOutlet weak var commentsTextView: UITextView!
    
    @IBOutlet weak var submitRiviewBtn: UIButton!
    
    @IBOutlet weak var starBtn1: UIButton!
    @IBOutlet weak var starBtn2: UIButton!
    @IBOutlet weak var starBtn3: UIButton!
    @IBOutlet weak var starBtn4: UIButton!
    @IBOutlet weak var starBtn5: UIButton!    
    @IBOutlet weak var ratingStackView: RatingController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        commentsTextView.layer.borderWidth = 1
//        commentsTextView.layer.borderColor = UIColor.init(red: 179, green: 215, blue: 238, alpha: 1) as! CGColor
//        commentsTextView.layer.borderColor = UIColor.lightGray.cgColor
        commentsTextView.layer.borderColor = UIColor(red: 179/255, green: 215/255, blue: 238/255, alpha: 1.0).cgColor
        commentsTextView.layer.cornerRadius = 2
      submitRiviewBtn.layer.cornerRadius = submitRiviewBtn.frame.height/2
        
        commentsTextView.text = "Additional comments...."
        commentsTextView.textColor = UIColor.lightGray
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
