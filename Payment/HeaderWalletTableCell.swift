//
//  HeaderWalletTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 25/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class HeaderWalletTableCell: UITableViewCell {
    

    
    @IBOutlet weak var addWalletBtn: UIButton!
    
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var walletLabel: UILabel!
    
    @IBOutlet weak var walletCheckBtn: UIButton!
    
    @IBOutlet weak var dotLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowColor = UIColor.lightGray.cgColor
        shadowView.layer.shadowOpacity = 0.5
        shadowView.layer.shadowOffset = CGSize(width: 2, height: 2)
        shadowView.layer.shadowRadius = 1
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
}
