 //
//  SavedCardsListVC.swift
//  RideHubUser-Swift
//
//  Created by INDOBYTES on 07/03/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit
 class SavedCardsListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    let urlservices = URLservices()
    let themes = Themes()
    var savedCardsArray = Array<AnyObject>()
    var mobId = String()
    var rideID = String()
    var walletAmount = String()
    var fromScreen = String()
    var walletSelection = String()
    @IBOutlet weak var savedCardTableveiw: UITableView!
    
    @IBOutlet weak var dotLine: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
          drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        
        // Do any additional setup after loading the view.
        self.savedCardTableveiw.register(UINib(nibName: "PaymentCardTableCell", bundle: nil), forCellReuseIdentifier: "PaymentCardTableCell")
        savedCardTableveiw.separatorColor = UIColor.clear
        getSavedCardsDetails()
    }
    @IBAction func addNewCardAction(_ sender: Any) {
        
        if(fromScreen == "Wallet"){
            let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            rideVc.fromScreen = "Wallet"
            rideVc.walletAmount = walletAmount
            self.navigationController?.pushViewController(rideVc, animated: true)
        }else{
//            let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//            rideVc.rideID1 = rideID
//            rideVc.mobileID = mobId
//            rideVc.walletSelection = walletSelection
//            self.navigationController?.pushViewController(rideVc, animated: true)
            
            let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            rideVc.walletSelection = walletSelection
            rideVc.rideId = rideID
            self.navigationController?.pushViewController(rideVc, animated: true)
        }
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    func getSavedCardsDetails(){
        let networkRechability = urlservices.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId()] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlservices.serviceCallPostMethodWithParams(url:savedCards, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                print(success)
                if(success == "1") {
                    let some =  response["response"] as! [String : AnyObject]
                    self.savedCardsArray = some["cardlist"] as! Array
                    self.savedCardTableveiw.reloadData()
                }
                else{
                    let result = response["response"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func paymentWithSavedCardApi(cardId:String){
        let networkRechability = urlservices.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId(),"ride_id":rideID,"stripeToken":cardId,"wallet_check":walletSelection] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlservices.serviceCallPostMethodWithParams(url:paymentWithSavedCard, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                print(success)
                if(success == "1") {
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
                    rideVc.rideID = self.rideID
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }
                else{
                    let result = response["response"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func walletWithSavedCardApi(cardId:String){
        let networkRechability = urlservices.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId(),"stripeToken":cardId,"total_amount":walletAmount] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlservices.serviceCallPostMethodWithParams(url:walletPayment, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        let dashVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")as? HomeVC
                        self.navigationController?.pushViewController(dashVc!, animated: true)
                        
                        
                        
                    }
                    else{
                        let result = response["response"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(fromScreen == "Wallet"){
            let obj = savedCardsArray[indexPath.row]
            
            let cardId = obj["card_id"] as! String
            
          walletWithSavedCardApi(cardId: cardId)
        }else{
            let obj = savedCardsArray[indexPath.row]
            
            let cardId = obj["card_id"] as! String
            
            paymentWithSavedCardApi(cardId: cardId)
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return savedCardsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardTableCell", for: indexPath) as! PaymentCardTableCell
        cell.selectionStyle = .none
        let obj =  self.savedCardsArray[indexPath.row]
        cell.cardNumberLbl.text = (obj["card_number"] as! String)
        let type = obj["credit_card_type"] as! String
        if(type == "Visa"){
            cell.cardImgView.image = UIImage(named: "visaImage")
        }else if(type == "Discover"){
            cell.cardImgView.image = UIImage(named: "Discover")
        }else{
            cell.cardImgView.image = UIImage(named: "masterCradImage")
        }
        //        cell.cardImgView.clipsToBounds = true
        //        cell.imageView?.layer.masksToBounds = true
        let expireMonth = (obj["exp_month"] as! String)
        let expireYear = (obj["exp_year"] as! String)
        cell.cardExpireLbl.text = "Expires " + expireMonth + " / " + expireYear
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
