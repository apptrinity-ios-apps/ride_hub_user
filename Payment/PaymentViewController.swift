//
//  PaymentViewController.swift
//  BI-IL.GOV
//
//  Created by INDOBYTES on 23/07/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit
import Stripe


class PaymentViewController: UIViewController,STPPaymentCardTextFieldDelegate,UITextFieldDelegate {
    @IBOutlet var PayBtn: UIButton!
    var paymentTextField: STPPaymentCardTextField!
    var nameTextfield :UITextField!
    var amountStr = String()
    var totalAmount1 = String()
    var serviceId1 = String()
    var stripeToken = String()
    var id1 = String()
    var address = String()
    var dueType = String()
    var paymentType = String()
    var walletAmount = String()
    var themes = Themes()
     var urlSer = URLservices.sharedInstance
    var remainingAmount = String()
    var button = UIButton()
    var rideId = String()
    var walletSelection = String()
    var fromScreen = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // add stripe built-in text field to fill card information in the middle of the view
        super.viewDidLoad()
         button = UIButton(type: .system) // let preferred over var here
        button.frame = CGRect(x:80, y:260, width:self.view.frame.size.width - 160, height:40)
        
        button.layer.cornerRadius = 10
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        button.backgroundColor = UIColor.black
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Submit", for: UIControlState.normal)
         button.addTarget(self, action: #selector(self.submitClicked(button:)), for: .touchUpInside)
       // button.addTarget(self, action: "Action:", for: UIControlEvents.touchUpInside)
        self.view.addSubview(button)
        
        
        let frame1 = CGRect(x: 20, y: 150, width: self.view.frame.size.width - 40, height: 40)
        paymentTextField = STPPaymentCardTextField(frame: frame1)

       // paymentTextField.center = view.center
        paymentTextField.delegate = self
        
        let frame2 = CGRect(x: 20, y: 200, width: self.view.frame.size.width - 40, height: 40)
       
        nameTextfield = UITextField(frame: frame2)
        nameTextfield.backgroundColor = UIColor.clear
        nameTextfield.layer.cornerRadius = 5
        nameTextfield.layer.borderWidth = 1
        nameTextfield.layer.borderColor = UIColor.lightGray.cgColor
        nameTextfield.placeholder = " Name on card"
        nameTextfield.delegate = self
        if(fromScreen == "Wallet"){
             button.setTitle("Add " + "$ " + walletAmount, for: .normal)
        }else{
             button.setTitle("Submit", for: .normal)
        }
        
//        let hint = UserDefaults.standard.object(forKey: "fromScreenHint") as! String
//        if(hint == "1"){
//
//
//
//        }else{
//             button.setTitle("Pay " + "$ " + remainingAmount, for: .normal)
//        }
        
 
        view.addSubview(paymentTextField)
        view.addSubview(nameTextfield)
        //disable payButton if there is no card information
        button.isEnabled = false
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
         // adminStatusCheckServiceCall()
    }
    func paymentWithNewCardApi(){
        let networkRechability = urlSer.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId(),"ride_id":rideId,"stripeToken":stripeToken,"wallet_check":walletSelection] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlSer.serviceCallPostMethodWithParams(url:paymentWithSavedCard, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
                        rideVc.rideID = self.rideId
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }
                    else{
                        let result = response["response"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @objc func submitClicked(button:UIButton){
        let card = paymentTextField.cardParams
        //SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        //SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
        //send card information to stripe to get back a token
         print("1nd is \(card)")
       
       // getStripeToken(card: card)
        let cardParams = STPCardParams()
        cardParams.number = paymentTextField?.cardNumber
        cardParams.expMonth = (paymentTextField?.expirationMonth)!
        cardParams.expYear = (paymentTextField?.expirationYear)!
        cardParams.cvc = paymentTextField?.cvc
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                // Present error to user...
                return
            }
            //            self.dictPayData["stripe_token"] = token.tokenId
            print("2nd is \(token.tokenId)")
             self.stripeToken = token.tokenId
            if(self.fromScreen == "Wallet"){
                 self.payServiceApi()
            }else{
              self.paymentWithNewCardApi()
            }
        }
    }
    
    

    @IBAction func payBtn(_ sender: Any)
    {
        let card = paymentTextField.cardParams
        //SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        //SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
        //send card information to stripe to get back a token
      //  getStripeToken(card: card)
        
        let cardParams = STPCardParams()
        cardParams.number = paymentTextField?.cardNumber
        cardParams.expMonth = (paymentTextField?.expirationMonth)!
        cardParams.expYear = (paymentTextField?.expirationYear)!
        cardParams.cvc = paymentTextField?.cvc
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                // Present error to user...
                return
            }
//            self.dictPayData["stripe_token"] = token.tokenId
            print(token.tokenId)
            self.stripeToken = token.tokenId
        }
      
        
    }
    
//    func getStripeToken() {
//        // get stripe token for current card
//        STPAPIClient.shared().createToken(withCard: stripeToken) { token, error in
//            if let token = token {
//                print(token)
//               // self.themes.showAlert(title: "Alert", message: "Stripe token successfully received: \(token)", sender: self)
//                //SVProgressHUD.showSuccess(withStatus: "Stripe token successfully received: \(token)")
//
//              // self.payServiceApi(token: token)
//
////                let hint = UserDefaults.standard.object(forKey: "fromScreenHint") as! String
////                if(hint == "1"){
////                    self.postStripeToken(token: token)
////                }else{
////                    self.payServiceApi(token: token)
////                }
////
//
//            } else {
//                print(error)
//
//                //SVProgressHUD.showError(withStatus:error?.localizedDescription)
//            }
//        }
//    }
    
    
    func payServiceApi()
    {
        let networkRechability =  urlSer.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
           
             let parameters = ["user_id" : themes.getUserId(),"stripeToken":stripeToken,"total_amount":walletAmount] as [String:Any]
            urlSer.serviceCallPostMethodWithParams(url: walletPayment, params: parameters as Dictionary<String, AnyObject>) { response in
                print(response)
                let success = response["status"] as! String
                if(success == "1"){
                    
                    let alertController = UIAlertController(title: "Alert", message: "Payment Succeded", preferredStyle: .alert)
                    
                    // Create the actions
                    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                        UIAlertAction in
                        NSLog("OK Pressed")
                        
                       
                        let dashVc = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC")as? HomeVC
                        self.navigationController?.pushViewController(dashVc!, animated: true)
                        }
                    
                    print("api success")
                    
                    alertController.addAction(okAction)
                    
                    
                    // Present the controller
                    self.present(alertController, animated: true, completion: nil)
                    
                  //  self.ToastView(message: "Payment Sucess")
                    
                    self.themes.hideActivityIndicator(uiView: self.view)
                    
                }
                else{
                    let result = response["msg"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                print(response)
                
                
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
        
        
    }
    
    
    
   
    func paymentCardTextFieldDidChange(_ textField: STPPaymentCardTextField) {
        if textField.isValid{
            button.isEnabled = true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        return true
    }
    
    //MARK: - CardIO Methods

    //Allow user to cancel card scanning
//    func userDidCancelPaymentViewController(paymentViewController: CardIOPaymentViewController!) {
//        print("user canceled")
//        paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
//    }
//
//    //Callback when card is scanned correctly
//    func userDidProvideCreditCardInfo(cardInfo: CardIOCreditCardInfo!, inPaymentViewController paymentViewController: CardIOPaymentViewController!) {
//        if let info = cardInfo {
//            let str = NSString(format: "Received card info.\n Number: %@\n expiry: %02lu/%lu\n cvv: %@.", info.redactedCardNumber, info.expiryMonth, info.expiryYear, info.cvv)
//            print(str)
//
//            //dismiss scanning controller
//            paymentViewController?.dismissViewControllerAnimated(true, completion: nil)
//
//            //create Stripe card
//            let card: STPCardParams = STPCardParams()
//            card.number = info.cardNumber
//            card.expMonth = info.expiryMonth
//            card.expYear = info.expiryYear
//            card.cvc = info.cvv
//
//            //Send to Stripe
//            getStripeToken(card)
//
//        }
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func CancelBtn(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
}
