//
//  WebViewVC.swift
//  RideHubUser-Swift
//
//  Created by INDOBYTES on 07/03/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit
class WebViewVC: UIViewController,UIWebViewDelegate {
    var rideID1 = String()
    var mobileID = String()
    var walletSelection = String()
    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webView.loadRequest(NSURLRequest(url: NSURL(string: AppbaseUrl + "mobile/proceed-payment?mobileId=" + mobileID + "?wallet_check=" + walletSelection)! as URL) as URLRequest)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let currentUrl = webView.request?.url
        if(currentUrl?.description.contains("failed"))!{
            self.navigationController?.popViewController(animated: true)
            
        }else if(currentUrl?.description.contains("/success"))!{
            let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
            rideVc.rideID = rideID1
            self.navigationController?.pushViewController(rideVc, animated: true)
            //self.navigationController?.popViewController(animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
