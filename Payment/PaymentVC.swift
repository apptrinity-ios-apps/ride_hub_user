//
//  PaymentVC.swift
//  RideHubUser-Swift
//
//  Created by INDOBYTES on 06/03/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit

class PaymentVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    let urlservices = URLservices()
    let themes = Themes()
    var selectedID = String()
    var rideID = String()
    var paymentArry = Array<AnyObject>()
    var walletSelection = String()
    @IBOutlet weak var paymentTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.paymentTable.register(UINib(nibName: "PaymentCell", bundle: nil), forCellReuseIdentifier: "PaymentCell")
        
        self.paymentTable.separatorColor = UIColor.clear
        paymentListAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appdelegete = UIApplication.shared.delegate as! AppDelegate
        appdelegete.xmpp_plugin()
    }
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func submitAction(){
        if(selectedID == "cash"){
            let networkRechability = urlservices.connectedToNetwork()
            if(networkRechability){
                themes.showActivityIndicator(uiView: self.view)
                let parameters = ["user_id" : themes.getUserId(),"ride_id":rideID,"wallet_check":walletSelection] as [String:Any]
                //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
                print(" parameters is \(parameters)")
                urlservices.serviceCallPostMethodWithParams(url:PaymentByCash, params: parameters as Dictionary<String, Any>) { response in
                    print(response)
                    let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                    if(isDead == "Yes"){
                        self.themes.hideActivityIndicator(uiView: self.view)
                        self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
//                          self.themes.showAlert(title: "Sucsess", message: "Your payment was successful. Please wait for a confirmation from the driver", sender: self)
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        
                        popOverVC.rideId = self.rideID
                        popOverVC.fromScreen = "Payment"
                        popOverVC.headingMesg = "Sucsess"
                        popOverVC.subMesg = "Your payment was successful. Please wait for a confirmation from the driver."
                        
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
                        
//                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
//                        rideVc.rideID = self.rideID
//                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }
                    else{
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                  }
                }
            }else{
                themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
            }
        }else if(selectedID == "swipe"){
            let networkRechability = urlservices.connectedToNetwork()
            if(networkRechability){
                themes.showActivityIndicator(uiView: self.view)
                let parameters = ["user_id" : themes.getUserId(),"ride_id":rideID,"wallet_check":walletSelection] as [String:Any]
                //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
                print(" parameters is \(parameters)")
                urlservices.serviceCallPostMethodWithParams(url:PaymentByCash, params: parameters as Dictionary<String, Any>) { response in
                    print(response)
                    let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                    if(isDead == "Yes"){
                        self.themes.hideActivityIndicator(uiView: self.view)
                        self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }else{
                        self.themes.hideActivityIndicator(uiView: self.view)
                        let success = response["status"] as! String
                        print(success)
                        if(success == "1") {
                            self.themes.showAlert(title: "Sucsess", message: "Your payment was successful.", sender: self)
                            let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
                            rideVc.rideID = self.rideID
                            self.navigationController?.pushViewController(rideVc, animated: true)
                        }
                        else{
                            let result = response["message"] as! String
                            self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                        }
                    }
                }
            }else{
                themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
            }
        }else if(selectedID == "wallet"){
            let networkRechability = urlservices.connectedToNetwork()
            if(networkRechability){
                themes.showActivityIndicator(uiView: self.view)
                let parameters = ["user_id" : themes.getUserId(),"ride_id":rideID,"wallet_check":walletSelection] as [String:Any]
                //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
                print(" parameters is \(parameters)")
                urlservices.serviceCallPostMethodWithParams(url:PaymentByWallet, params: parameters as Dictionary<String, Any>) { response in
                    print(response)
                    let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                    if(isDead == "Yes"){
                        self.themes.hideActivityIndicator(uiView: self.view)
                        self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        
                        popOverVC.fromScreen = "Payment"
                        //                    popOverVC.ImageStr = ""
                        popOverVC.headingMesg = "Sucsess"
                        popOverVC.subMesg = "Your Payment successfully finished"
                        
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
                        
//                        self.themes.showAlert(title: "Sucsess", message: "Your Payment successfully finished", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
                        rideVc.rideID = self.rideID
//                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }else if(success == "2"){
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        
                        popOverVC.fromScreen = "wallet"
                        //                    popOverVC.ImageStr = ""
                        popOverVC.headingMesg = "Sucsess"
                        popOverVC.subMesg = "Your Wallet amount successfully used"
                        
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
                        
//                        self.themes.showAlert(title: "Sucsess", message: "Your Wallet amount successfully used", sender: self)
                        self.paymentListAPI()
                    }else if(success == "0"){
                        self.themes.showAlert(title: "Sucsess", message: "Your Wallet is empty", sender: self)
                        self.paymentListAPI()
                    }
                    else{
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
                }
            }else{
                themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
            }
        }else if(selectedID == "auto_detect"){
            let networkRechability = urlservices.connectedToNetwork()
            if(networkRechability){
                themes.showActivityIndicator(uiView: self.view)
                let parameters = ["user_id" : themes.getUserId(),"ride_id":rideID] as [String:Any]
                //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
                print(" parameters is \(parameters)")
                urlservices.serviceCallPostMethodWithParams(url:detectiontionAuto, params: parameters as Dictionary<String, Any>) { response in
                    print(response)
                    let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                    if(isDead == "Yes"){
                        self.themes.hideActivityIndicator(uiView: self.view)
                        self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        
                        popOverVC.fromScreen = "Payment"
                        //                    popOverVC.ImageStr = ""
                        popOverVC.headingMesg = "Sucsess"
                        popOverVC.subMesg = "Your Payment successfully finished"
                        
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
//                        self.themes.showAlert(title: "Sucsess", message: "Your Payment successfully finished", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
                        rideVc.rideID = self.rideID
//                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }
                    else{
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
                }
            }else{
                themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
            }
        }else{
            let networkRechability = urlservices.connectedToNetwork()
            if(networkRechability){
                themes.showActivityIndicator(uiView: self.view)
                let parameters = ["user_id" : themes.getUserId(),"ride_id":rideID,"gateway":selectedID] as [String:Any]
                //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
                print(" parameters is \(parameters)")
                urlservices.serviceCallPostMethodWithParams(url:paymentGetWay, params: parameters as Dictionary<String, Any>) { response in
                    print(response)
                    let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                    if(isDead == "Yes"){
                        self.themes.hideActivityIndicator(uiView: self.view)
                        self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    let mobile_id = response["mobile_id"] as! String
                    print(success)
                    if(success == "1") {
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "SavedCardsListVC") as! SavedCardsListVC
                        rideVc.rideID = self.rideID
                        rideVc.walletSelection = self.walletSelection
                        rideVc.mobId = mobile_id
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }
                    else{
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
                }
            }else{
                themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentArry.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! PaymentCell
        cell.selectionStyle = .none
        let obj = paymentArry[indexPath.row]
        cell.typeLabel.text = obj["name"] as? String
        
        if(indexPath.row == 0){
            cell.cardImageView.image = UIImage(named:"dollerGreen")
        }else if(indexPath.row == 1){
            cell.cardImageView.image = UIImage(named:"creditCardImage")
        }else{
            cell.cardImageView.image = UIImage(named:"creditOnFile")
        }
        
        
        
        let id = obj["code"] as? String
        
        
        
        
        if(selectedID == id){
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = paymentArry[indexPath.row]
        selectedID = obj["code"] as! String
        
         submitAction()
       // paymentTable.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    func paymentListAPI() {
        let networkRechability = urlservices.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId(),"ride_id":rideID] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlservices.serviceCallPostMethodWithParams(url:paymentList, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                print(success)
                if(success == "1") {
                    //let result = response["response"] as! String
                    print(response)
                    let some =  response["response"] as! [String : AnyObject]
                    self.paymentArry = some["payment"] as! Array<AnyObject>
                    self.paymentTable.reloadData()
                    // let id = response["id"] as! String
                    
                    
                }
                else{
                    
                    let result = response["response"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func submitReasonAPI() {
        let networkRechability = urlservices.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId(),"ride_id" : rideID,"reason":selectedID] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlservices.serviceCallPostMethodWithParams(url:rideCanel, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                print(success)
                if(success == "1") {
                    //let result = response["response"] as! String
                    print(response)
                    let some =  response["response"] as! [String : AnyObject]
                    let message =  some["message"] as! String
                    self.themes.showAlert(title: "Success", message: message, sender: self)
                    UIApplication.shared.keyWindow!.rootViewController as! HomeVC
                }
                else{
                    let result = response["message"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
