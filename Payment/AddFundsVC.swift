//
//  AddFundsVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 23/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class AddFundsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var dotLine: UIView!
    
    @IBOutlet weak var addFundsTableVIew: UITableView!
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func closeAction(_ sender: Any) {
//        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "HelpSubscriptionsVC") as! HelpSubscriptionsVC
//        
//        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addFundsTableVIew.delegate = self
        addFundsTableVIew.dataSource = self
        
        self.addFundsTableVIew.register(UINib(nibName: "HeadeFundsTableCell", bundle: nil), forCellReuseIdentifier: "HeadeFundsTableCell")
        self.addFundsTableVIew.register(UINib(nibName: "AddFundsTableCell", bundle: nil), forCellReuseIdentifier: "AddFundsTableCell")
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)

        // Do any additional setup after loading the view.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier:  "HeadeFundsTableCell") as! HeadeFundsTableCell
        
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddFundsTableCell", for: indexPath) as! AddFundsTableCell
        cell.selectionStyle = .none

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 171
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116
    }
    
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    

}
