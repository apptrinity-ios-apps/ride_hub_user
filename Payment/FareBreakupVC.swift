//  FareBreakupVC.swift
//  RideHubProfile
//  Created by S s Vali on 11/15/18.
//  Copyright © 2018 Indobytes. All rights reserved.
import UIKit
class FareBreakupVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    var rideDetails = [String:Any]()
    var tipsValue = Bool()
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var referPointValue = Bool()
    var pointsHint = Int()
    var rideTotalAmount = String()
    var tipsApplyHint = false
    var tipsAmount = String()
    var referApplyAmount = Double()
   var walletAmount = Int()
    var walletSelection = Bool()
    @IBOutlet weak var dotLine: UIView!
  @IBOutlet weak var fareBreakupTableView: UITableView!
 override func viewDidLoad(){
        super.viewDidLoad()
        tipsValue = false
        referPointValue = false
        fareBreakupTableView.delegate = self
        fareBreakupTableView.dataSource = self
        self.fareBreakupTableView.register( UINib(nibName:"ProfileTableViewCell" , bundle : nil), forCellReuseIdentifier: "ProfileTableViewCell")
        self.fareBreakupTableView.register(UINib(nibName:"BaseFareTableViewCell" , bundle: nil), forCellReuseIdentifier: "BaseFareTableViewCell")
        self.fareBreakupTableView.register(UINib(nibName:"SubToatalTableViewCell" , bundle: nil), forCellReuseIdentifier: "SubToatalTableViewCell")
//    NotificationCenter.default.addObserver(self, selector: #selector(FareBreakupVC.keyboardUp), name: NSNotification.Name.UIResponder.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(FareBreakupVC.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
     drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
      NotificationCenter.default.addObserver(self, selector: #selector(FareBreakupVC.keyboardUp), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(FareBreakupVC.keyboardDown), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    walletAmountApi()
        // Do any additional setup after loading the view.
    }
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        //        shapeLayer.strokeColor = (UIColor.init(red: 6, green: 152, blue: 212, alpha: 1) as! CGColor)
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        //        shapeLayer.strokeColor = UIColor.blue.cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        }else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorColor = .clear
        if indexPath.section == 0 {
            let cell  = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath)as! ProfileTableViewCell
            
            let userImg = rideDetails["key11"] as! String
             let url = URL(string: userImg)
//          cell1.profileImageView.image = UIImage(url: URL(string: userImg))
       //  cell1.carnum
        cell.profileImageView.load(url: url!)
        cell.profileImageView.layer.cornerRadius = (cell.profileImageView.frame.size.height)/2
//            cell1.profileImageView.layer.masksToBounds = true
            cell.profileImageView.clipsToBounds = true
            cell.profileNameLabel.text = rideDetails["key10"] as? String
            cell.carNumberLbl.text = rideDetails["key26"] as? String
            cell.carNameLbl.text = rideDetails["key28"] as? String
            cell.carBrandLbl.text = (themes.checkNullValue(rideDetails["key27"] as? String) as! String)
            
            let rating = themes.checkNullValue(rideDetails["key12"] as? String) as! String
            
            if(rating == ""){
                cell.star1.image = UIImage(named: "star_empty")
                cell.star2.image = UIImage(named: "star_empty")
                cell.star3.image = UIImage(named: "star_empty")
                cell.star4.image = UIImage(named: "star_empty")
                cell.star5.image = UIImage(named: "star_empty")
            }else{
                DispatchQueue.main.async {
                    let rate = rating
                    let ratingDouble = Double(rate)
                    let ratingInt = Int(ratingDouble!)
                    if(ratingInt == 1){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_empty")
                        cell.star3.image = UIImage(named: "star_empty")
                        cell.star4.image = UIImage(named: "star_empty")
                        cell.star5.image = UIImage(named: "star_empty")
                    }else if(ratingInt == 2){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_filled")
                        cell.star3.image = UIImage(named: "star_empty")
                        cell.star4.image = UIImage(named: "star_empty")
                        cell.star5.image = UIImage(named: "star_empty")
                    }else if(ratingInt == 3){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_filled")
                        cell.star3.image = UIImage(named: "star_filled")
                        cell.star4.image = UIImage(named: "star_empty")
                        cell.star5.image = UIImage(named: "star_empty")
                    }else if(ratingInt == 4){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_filled")
                        cell.star3.image = UIImage(named: "star_filled")
                        cell.star4.image = UIImage(named: "star_filled")
                        cell.star5.image = UIImage(named: "star_empty")
                    }else if(ratingInt == 5){
                        cell.star1.image = UIImage(named: "star_filled")
                        cell.star2.image = UIImage(named: "star_filled")
                        cell.star3.image = UIImage(named: "star_filled")
                        cell.star4.image = UIImage(named: "star_filled")
                        cell.star5.image = UIImage(named: "star_filled")
                    }
                    
                }
                
            }
            
            
            
            if(tipsApplyHint){
               
                    cell.rideTotalLbl.text = "$ " + rideTotalAmount
                
            }else{
                    let rideTotal = rideDetails["key2"] as? String
                    cell.rideTotalLbl.text = "$ " + rideTotal!
            }
            cell.selectionStyle = .none
        return cell
        } else if indexPath.section == 1 {
            let cell3 = tableView.dequeueReusableCell(withIdentifier: "BaseFareTableViewCell", for: indexPath)as! BaseFareTableViewCell
             cell3.selectionStyle = .none
            let rideFare = rideDetails["key18"] as! String
            
            cell3.baseFareLabel.text = "US $" + rideFare
            cell3.discountLabel.text = rideDetails["key19"] as? String
            let points:String = (rideDetails["key23"] as? String)!
            
            let convertPoints = (rideDetails["key24"] as? String)!
             let convertDollors = (rideDetails["key25"] as? String)!
            cell3.walletAmount.text = String(walletAmount)
            cell3.pointsToDollorsLabel.text = convertPoints + " points equal to $" + convertDollors

            if(points == ""){
                cell3.totalLbl.text = "0"
                pointsHint = 0
            }else{
                cell3.totalLbl.text = points
                pointsHint = 1
                
            }
            cell3.timeDiscountLabel.text = rideDetails["key4"] as? String
            cell3.waitingLabel.text = rideDetails["key5"] as? String
            cell3.distanceLabel.text = rideDetails["key3"] as? String
            cell3.serviceTaxLabel.text = rideDetails["key20"] as? String
            cell3.pointCheckBtn?.addTarget(self, action:#selector(referPointsCheckBtnClick(_:)), for:.touchUpInside)
            
             cell3.walletBtn?.addTarget(self, action:#selector(walletBtnClick(_:)), for:.touchUpInside)
            
            if(tipsApplyHint){
                //cell3.tipsLabel.text = tips
                cell3.tipsHeightConstraint.constant = 30
                
                cell3.tipsLabel.text = self.tipsAmount
            }else{
                cell3.tipsHeightConstraint.constant = 0
            }
            
            if(walletSelection){
                
                cell3.walletBtn.setImage(UIImage(named: "check"), for: .normal)
                
            }else{
               cell3.walletBtn.setImage(UIImage(named: "uncheck"), for: .normal)
            }
             if(referPointValue){
                 cell3.pointCheckBtn.setImage(UIImage(named: "check"), for: .normal)
                if(points == ""){
                  
                }else{
                    cell3.pointsUsedHeightConstraint.constant = 30
                    let rideTotal:String = (rideDetails["key2"] as? String)!
                    
                    if(tipsApplyHint){
                        let val = Double(rideTotalAmount)! * Double(convertPoints)!
                        let point = Double(points)!
                        if(val >= point){
                            
                            
                            cell3.pointsUsedLabel.text = String(format:"%.2f", point)
                        }else{
                            cell3.pointsUsedLabel.text = String(format:"%.2f", val)
                        }
                      
                    }else{
                        let val = Double(rideTotal)! * Double(convertPoints)!
                        let point = Double(points)!
                        if(val >= point){
                           
                            
                            cell3.pointsUsedLabel.text = String(format:"%.2f", point)
                        }else{
                            cell3.pointsUsedLabel.text = String(format:"%.2f", val)
                        }
                    }
                }
                
             }else{
                cell3.pointsUsedHeightConstraint.constant = 0
                cell3.pointCheckBtn.setImage(UIImage(named: "uncheck"), for: .normal)
            }
            
            
//            cell3.tipsCheckBtn.tag = indexPath.row
//            cell3.tipsCheckBtn?.addTarget(self, action:#selector(tipsCheckBtnClick(_:)), for:.touchUpInside)
            cell3.applyBtn?.addTarget(self, action:#selector(tipsApplyBtnClick(_:)), for:.touchUpInside)
            cell3.tipsField.delegate = self
            
            return cell3
        } else {
            let cell4 = tableView.dequeueReusableCell(withIdentifier: "SubToatalTableViewCell", for: indexPath)as! SubToatalTableViewCell
            cell4.selectionStyle = .none
            if(tipsApplyHint){
                
                if(referPointValue){
   
                    if(Double(rideTotalAmount)! < referApplyAmount){
                         cell4.subTotalLabel.text = "$ 0"
                    }else{
                        let val = Double(rideTotalAmount)! - referApplyAmount
                        cell4.subTotalLabel.text = "$ " + String(format:"%.2f", val)
                    }
                }else{
                    cell4.subTotalLabel.text = "$ " + rideTotalAmount
                }
            }else{
                let val1:String = (rideDetails["key2"] as? String)!
                if(referPointValue){
                    if(Double(val1)! < referApplyAmount){
                        cell4.subTotalLabel.text = "$ 0"
                    }else{
                        let val1:String = (rideDetails["key2"] as? String)!
                        let val = Double(val1)! - referApplyAmount
                        cell4.subTotalLabel.text = "$ " + String(format:"%f", val)
                    }
                    
                }else{
                    let rideTotal = rideDetails["key2"] as? String
                    cell4.subTotalLabel.text = "$ " + rideTotal!
                }
            }
   
            cell4.makePaymentButton?.addTarget(self, action:#selector(paymentBtnClick(_:)), for:.touchUpInside)
//            if(tipsValue){
//                cell4.tipsCheckBtn.setImage(UIImage(named: "check"), for: .normal)
//                cell4.tipsView.isHidden = false
//            }else{
//                cell4.tipsCheckBtn.setImage(UIImage(named: "uncheck"), for: .normal)
//                cell4.tipsView.isHidden = true
//            }
            return cell4
        }
}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 129
        } else if indexPath.section == 1 {
           // return 437
            
            return UITableViewAutomaticDimension
        } else {
                return 111
           
       }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
                //emailTFT.becomeFirstResponder()
        return true
    }
    @objc func paymentBtnClick(_ sender: AnyObject){
        if(referPointValue){
            if(pointsHint == 0){
                self.themes.showAlert(title: "Message", message: "Your Refer points is empty.Please uncheck your checkbox", sender: self)
            }else{
                referPointsPaymentApi()
            }
        }else{
            if(walletSelection){
                if(rideTotalAmount == ""){
                    rideTotalAmount = rideDetails["key2"] as! String
                }
                
                
                    let rideAmount:Double = Double(rideTotalAmount)!
                    let walletAm:Double = Double(walletAmount)
                    if(rideAmount > walletAm){
                        let paymentVc = storyboard?.instantiateViewController(withIdentifier: "PaymentVC")as! PaymentVC
                        paymentVc.walletSelection = "Yes"
                        let rideId = rideDetails["key6"] as! String
                        paymentVc.rideID = rideId
                        self.navigationController?.pushViewController(paymentVc, animated: true)
                    }else{
                        paymentUsingWallet()
                    }
                
                
            }else{
                let paymentVc = storyboard?.instantiateViewController(withIdentifier: "PaymentVC")as! PaymentVC
                 paymentVc.walletSelection = "No"
                let rideId = rideDetails["key6"] as! String
                paymentVc.rideID = rideId
                self.navigationController?.pushViewController(paymentVc, animated: true)
            }
        }
    }
    func walletAmountApi()
    {
        let networkRechability =  urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = ["user_id" : themes.getUserId()] as [String:Any]
            urlService.serviceCallPostMethodWithParams(url: getWalletMoney, params: parameters as Dictionary<String, AnyObject>) { response in
                print(response)
                let success = response["status"] as! String
                if(success == "1"){
                    let response = response["response"] as AnyObject
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.walletAmount = response["current_balance"] as! Int
                    self.fareBreakupTableView.reloadData()
                }      
                else{
                    let result = response["msg"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                print(response)
                
                
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
   
    }
    func paymentUsingWallet(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId(),"ride_id":rideDetails["key6"] as! String] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:PaymentByWallet, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(success)
                    if(success == "1") {
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        popOverVC.fromScreen = "Payment"
                        //                    popOverVC.ImageStr = ""
                        popOverVC.headingMesg = "Sucsess"
                        popOverVC.subMesg = "Your Payment successfully finished"
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
                        //                        self.themes.showAlert(title: "Sucsess", message: "Your Payment successfully finished", sender: self)
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
                        
                        rideVc.rideID = self.rideDetails["key6"] as! String
                        
                        self.navigationController?.pushViewController(rideVc, animated: true)
                    }else if(success == "2"){
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
                        popOverVC.fromScreen = "wallet"
                        //                    popOverVC.ImageStr = ""
                        popOverVC.headingMesg = "Sucsess"
                        popOverVC.subMesg = "Your Wallet amount successfully used"
                        self.addChildViewController(popOverVC)
                        popOverVC.view.center = self.view.center
                        self.view.addSubview(popOverVC.view)
                        popOverVC.didMove(toParentViewController: self)
                    }else if(success == "0"){
                        self.themes.showAlert(title: "Sucsess", message: "Your Wallet is empty", sender: self)
                        
                    }
                    else{
                        let result = response["message"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    @objc func tipsApplyBtnClick(_ sender: AnyObject){

        let cell = fareBreakupTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? BaseFareTableViewCell
        if(cell?.tipsField.text == ""){
            themes.showAlert(title: "Alert", message: "Please add tip amount", sender: self)
        }else{
            tipsApi(amount: (cell?.tipsField.text)!)
        }
    }
    
    
    
    @objc func walletBtnClick(_ sender: AnyObject){
        if(walletSelection == false){
            
            if(walletAmount == 0){
                themes.showAlert(title: "Alert", message: "Please add money to wallet", sender: self)
            }else{
                
                walletSelection = true
                
            }
        }else{
            walletSelection = false
        }
        
        DispatchQueue.main.async {
            self.fareBreakupTableView.reloadData()
        }
     
        
        let indexPath = IndexPath(row: 0, section: 2)
        self.fareBreakupTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    
    @objc func referPointsCheckBtnClick(_ sender: AnyObject){
        
        referApplyAmount = 0.0
        if(referPointValue == false){
             let points:String = (rideDetails["key23"] as? String)!
            let val:String = (rideDetails["key24"] as? String)!
            if(points == ""){
               // referApplyAmount =
            }else{

                let point = Double(points)

                referApplyAmount = point! / Double(val)!
            }
      
            referPointValue = true
        }else{
            referPointValue = false
        }
        fareBreakupTableView.reloadData()
        let indexPath = IndexPath(row: 0, section: 1)
        self.fareBreakupTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
    @objc func tipsCheckBtnClick(_ sender: AnyObject){
        if tipsValue ==  true {
            tipsValue = false
        } else{
            tipsValue = true
        }
        fareBreakupTableView.reloadData()
    }
    func referPointsPaymentApi(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let rideId = rideDetails["key6"] as! String
            let parameters = [ "user_id":themes.getUserId(),"ride_id":rideId,"pay_by_points":"Yes"] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:referPointsApi, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                   print("response is \(response)")
                    let message = response["response"] as! String
                    let AlertMesg = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
                    AlertMesg.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                        
                        let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
                        rideVc.rideID = rideId
                        self.navigationController?.pushViewController(rideVc, animated: true)
                        
                        //            print("Handle Ok logic here")
                    }))
                    
                    self.present(AlertMesg, animated: true, completion: nil)
                    
                    
                    
                }else if(success == "2"){
                    
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    rideVc.rideID = rideId
                    self.navigationController?.pushViewController(rideVc, animated: true)
                    
                    
                }
                else{
                    let result = response["message"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    func tipsApi(amount:String){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
             let rideId = rideDetails["key6"] as! String
            let parameters = [ "tips_amount":amount,"ride_id":rideId] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:tipsAdding, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                    print("response is \(response)")
                     let some =  response["response"] as! [String : AnyObject]
                    self.rideTotalAmount = some["total"] as! String
                    self.tipsAmount = some["tips_amount"] as! String
                    self.view.endEditing(true)
                    self.tipsApplyHint = true
                    self.tipsValue = false
                    
                    
                    DispatchQueue.main.async {
                        self.fareBreakupTableView.reloadData()
                    }
                }
                else{
                    let result = response["message"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    @objc func keyboardUp(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            // self.view.frame.origin.y = 200
            if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
                let keyboardRectangle = keyboardFrame.cgRectValue
                let keyboardHeight = keyboardRectangle.height
                self.view.frame.origin.y = -200
            }
        }
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        let appdelegete = UIApplication.shared.delegate as! AppDelegate
        
        appdelegete.xmpp_plugin()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Show the navigation bar on other view controllers
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDown(notification : NSNotification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            self.view.frame.origin.y = 0
        }
    }
  }

