//
//  HeaderAddCashTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 06/11/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class HeaderAddCashTableCell: UITableViewCell {
    
    @IBOutlet weak var amountLabel: UILabel!
    
    @IBOutlet weak var addCashBtn: UIButton!
    
    @IBOutlet weak var autoRefillBtn: UIButton!
    
    @IBOutlet weak var giftCodeBtn: UIButton!
    
    @IBOutlet weak var dotLine: UIView!
    
    @IBOutlet weak var amountTextField: UITextField!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        addCashBtn.layer.cornerRadius = addCashBtn.frame.size.height/2
        amountTextField.layer.cornerRadius = amountTextField.frame.size.height/2
        
        amountTextField.layer.borderWidth = 1
        amountTextField.layer.borderColor = UIColor.lightGray.cgColor
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
}
