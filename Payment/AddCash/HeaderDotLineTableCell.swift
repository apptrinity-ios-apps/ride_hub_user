//
//  HeaderDotLineTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 06/11/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class HeaderDotLineTableCell: UITableViewCell {
    
    @IBOutlet weak var header1Btn: UIButton!
    
    @IBOutlet weak var header1Lbl: UILabel!
    
    @IBOutlet weak var dotLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
}
