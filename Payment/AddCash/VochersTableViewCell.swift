//
//  VochersTableViewCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 06/11/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class VochersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var voucherImg: UIImageView!
    
    @IBOutlet weak var voucherLbl: UILabel!
    
    @IBOutlet weak var voucherNumberLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
