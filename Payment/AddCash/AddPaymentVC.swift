//
//  AddPaymentVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 28/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class AddPaymentVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
     var amount = String()
    var cardsArray = ["4702","1234","2345","3456","4567"]
 var urlSer = URLservices.sharedInstance
     let themes = Themes()
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var dotLine: UIView!
    
    @IBOutlet weak var addCashTableView: UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        
        addCashTableView.delegate = self
        addCashTableView.dataSource = self
        
        self.addCashTableView.register(UINib(nibName: "HeaderAddCashTableCell", bundle: nil), forCellReuseIdentifier: "HeaderAddCashTableCell")
        self.addCashTableView.register(UINib(nibName: "HeaderDotLineTableCell", bundle: nil), forCellReuseIdentifier: "HeaderDotLineTableCell")
        self.addCashTableView.register(UINib(nibName: "HeaderDotLIneTableCell2", bundle: nil), forCellReuseIdentifier: "HeaderDotLIneTableCell2")
        self.addCashTableView.register(UINib(nibName: "PaymentCardTableCell", bundle: nil), forCellReuseIdentifier: "PaymentCardTableCell")
        self.addCashTableView.register(UINib(nibName: "ProfilesTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfilesTableViewCell")
        self.addCashTableView.register(UINib(nibName: "VochersTableViewCell", bundle: nil), forCellReuseIdentifier: "VochersTableViewCell")
        
     //   let cell = addCashTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! HeaderAddCashTableCell
        
      
        

        // Do any additional setup after loading the view.
    }
    @objc func donePicker() {
      //  noteTextField.resignFirstResponder()
        self.view.endEditing(true)
    }
    
    @objc func cancelPicker() {
         self.view.endEditing(true)
//        noteTextField.text = ""
//        noteTextField.resignFirstResponder()
    }
   
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [4, 2] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
           if section == 1 {
            let header = tableView.dequeueReusableCell(withIdentifier: "HeaderDotLineTableCell") as! HeaderDotLineTableCell
            
            header.header1Btn.setTitle("Add Payment Method", for: .normal)
            header.header1Lbl.text = "Ride Profiles"
            header.header1Btn.addTarget(self, action: #selector(AddPaymentBtnClicked(button:)), for: .touchUpInside)
            
            return header
        } else if section == 2 {
            let header = tableView.dequeueReusableCell(withIdentifier: "HeaderDotLIneTableCell2") as! HeaderDotLIneTableCell2
            
            header.header2Lbl.text = "Payment Offers"
            
            return header
        } else if section == 3 {
            let header = tableView.dequeueReusableCell(withIdentifier: "HeaderDotLineTableCell") as! HeaderDotLineTableCell
            header.header1Btn.addTarget(self, action: #selector(ViewOffersBtnClicked(button:)), for: .touchUpInside)
            header.header1Btn.setTitle("View All Offers", for: .normal)
            header.header1Lbl.text = "Promotions"
            
            return header
        } else {
            let header = tableView.dequeueReusableCell(withIdentifier: "HeaderDotLIneTableCell2") as! HeaderDotLIneTableCell2
            
            header.header2Lbl.text = "Vouchers"
            
            return header
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return cardsArray.count + 1
        } else if section == 1 {
            return 2
        } else if section == 2 {
            return 2
        } else if section == 3 {
            return 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderAddCashTableCell") as! HeaderAddCashTableCell
                cell.addCashBtn.addTarget(self, action: #selector(addCashBtnClicked(button:)), for: .touchUpInside)
                cell.autoRefillBtn.addTarget(self, action: #selector(autoRefillBtnClicked(button:)), for: .touchUpInside)
                cell.giftCodeBtn.addTarget(self, action: #selector(giftCodeBtnClicked(button:)), for: .touchUpInside)
                cell.amountTextField.delegate = self
                let toolBar = UIToolbar()
                toolBar.barStyle = UIBarStyle.default
                toolBar.isTranslucent = true
                toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
                toolBar.sizeToFit()
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(AddPaymentVC.donePicker))
                let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
                let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(AddPaymentVC.cancelPicker))
                toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
                toolBar.isUserInteractionEnabled = true
                
                 cell.amountTextField.inputAccessoryView = toolBar
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardTableCell", for: indexPath) as! PaymentCardTableCell
                
                cell.cardNumberLbl.text = cardsArray[indexPath.row - 1]
                
                return cell
            }
        } else if indexPath.section == 1 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilesTableViewCell", for: indexPath) as! ProfilesTableViewCell
                cell.profileLbl.text = "Personal"
                cell.businessLbl.isHidden = true
                cell.profileImg.image = UIImage(named: "user_blue")
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilesTableViewCell", for: indexPath) as! ProfilesTableViewCell
                cell.profileLbl.text = "Start using Ride hub for bussiness"
                cell.businessLbl.isHidden = false
                cell.profileImg.image = UIImage(named: "bussiness")
                
                return cell
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilesTableViewCell", for: indexPath) as! ProfilesTableViewCell
                cell.profileLbl.text = "Amex Benifit: $200 Ride hub cash annually"
                cell.businessLbl.isHidden = true
                cell.profileImg.image = UIImage(named: "amex")
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilesTableViewCell", for: indexPath) as! ProfilesTableViewCell
                cell.profileLbl.text = "Earn Ride hub Cash with visa"
                cell.businessLbl.isHidden = true
                cell.profileImg.image = UIImage(named: "cashImage")
                
                return cell
            }
        } else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VochersTableViewCell", for: indexPath) as! VochersTableViewCell
            cell.voucherImg.isHidden = true
            cell.voucherLbl.text = "Add Promo Code"
            cell.voucherNumberLbl.isHidden = true
            
            return cell
        } else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "VochersTableViewCell", for: indexPath) as! VochersTableViewCell
            cell.voucherImg.isHidden = false
            cell.voucherLbl.text = "Vouchers"
            cell.voucherNumberLbl.isHidden = false
            
            return cell
        } else {
           return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        } else if section == 1 {
            return 97
        } else if section == 2 {
            return 62
        } else if section == 3 {
            return 97
        } else if section == 4 {
            return 62
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return 359
            } else {
            return 62
            }
        } else if indexPath.section == 1 {
            return 79
        } else if indexPath.section == 2 {
            return 79
        } else if indexPath.section == 3 {
            return 68
        } else if indexPath.section == 4 {
            return 68
        } else {
            return 0
        }
    }
    
    @objc func addCashBtnClicked(button : UIButton){
        self.view.endEditing(true)
       
        
        if(amount == ""){
          themes.showAlert(title: "Alert", message: "Please enter amount", sender: self)
        }else{
            let Vc = self.storyboard?.instantiateViewController(withIdentifier: "SavedCardsListVC") as! SavedCardsListVC
            
            Vc.fromScreen = "Wallet"
            Vc.walletAmount = amount
            
            self.navigationController?.pushViewController(Vc, animated: true)
        }
       
    }
    
    @objc func autoRefillBtnClicked(button : UIButton){
        
    }
    
    @objc func giftCodeBtnClicked(button : UIButton){
        
    }
    
    @objc func AddPaymentBtnClicked(button : UIButton){
//        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "ChangePaymentVC") as! ChangePaymentVC
//
//        self.navigationController?.pushViewController(Vc, animated: true)
//
    }
    
    @objc func ViewOffersBtnClicked(button : UIButton){
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        amount = textField.text!
        return true
    }

}
