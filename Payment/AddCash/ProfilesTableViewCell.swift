//
//  ProfilesTableViewCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 06/11/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class ProfilesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var profileLbl: UILabel!
    
    @IBOutlet weak var businessLbl: UILabel!
    
    @IBOutlet weak var arrowImage: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
