//
//  PaymentCardTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 25/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class PaymentCardTableCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var cardCheckBtn: UIButton!
    @IBOutlet weak var cardImgView: UIImageView!
    @IBOutlet weak var cardNumberLbl: UILabel!
    @IBOutlet weak var cardExpireLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.layer.masksToBounds = false
        backView.layer.shadowColor = UIColor.lightGray.cgColor
        backView.layer.shadowOpacity = 0.5
        backView.layer.shadowOffset = CGSize(width: 2, height: 2)
        backView.layer.shadowRadius = 1
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
