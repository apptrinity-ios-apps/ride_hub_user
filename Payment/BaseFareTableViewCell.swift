//
//  BaseFareTableViewCell.swift
//  RideHubProfile
//
//  Created by S s Vali on 11/15/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class BaseFareTableViewCell: UITableViewCell {

    @IBOutlet weak var tipsField: UITextField!
    
    @IBOutlet weak var walletBtn: UIButton!
    @IBOutlet weak var applyBtn: UIButton!
    
    @IBOutlet weak var walletAmount: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var baseFareLabel: UILabel!
    
    @IBOutlet weak var discountLabel: UILabel!
    
    @IBOutlet weak var pointsToDollorsLabel: UILabel!
    @IBOutlet weak var timeDiscountLabel: UILabel!
    
    @IBOutlet weak var waitingLabel: UILabel!
    
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var serviceTaxLabel: UILabel!
    
    @IBOutlet weak var totalLbl: UILabel!
    
    @IBOutlet weak var pointCheckBtn: UIButton!
   
    @IBOutlet weak var pointsUsedLabel: UILabel!
    
    @IBOutlet weak var pointsUsedHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tipsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tipsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        backView.layer.shadowColor = UIColor.black.cgColor
//        backView.layer.shadowOpacity = 0.5;
//        backView.layer.shadowRadius = 2;
//        backView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        
        //backView.dropShadow(color: .black, opacity: 0.5, offSet: CGSize(width: 3.0, height: 3.0), radius: 2, scale: true)
  
        backView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        backView.layer.shadowOffset = CGSize(width: 0, height: 3)
        backView.layer.shadowOpacity = 0.5
        backView.layer.shadowRadius = 5.0
        backView.layer.masksToBounds = false
        
        self.tipsField.layer.borderWidth = 1
        
        self.tipsField.layer.borderColor =  UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        
        self.tipsField.layer.cornerRadius = self.tipsField.frame.height / 2
        self.applyBtn.layer.cornerRadius = self.applyBtn.frame.height / 2
        
        self.tipsField.placeholder = "Enter Tips Amount"
        
        
        
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 1
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
