//
//  AddFundsTableCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 23/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class AddFundsTableCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var amountLbl: UILabel!    
    @IBOutlet weak var actualAmountLbl: UILabel!
    
    @IBOutlet weak var offerAmountLbl: UILabel!
    
    @IBOutlet weak var offerPercentLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backView.layer.cornerRadius = 5
        backView.layer.masksToBounds = false
        backView.layer.shadowColor = UIColor.lightGray.cgColor
        backView.layer.shadowOpacity = 0.5
        backView.layer.shadowOffset = CGSize(width: 2, height: 2)
        backView.layer.shadowRadius = 1
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
