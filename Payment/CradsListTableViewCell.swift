//
//  CradsListTableViewCell.swift
//  RideHubProfile
//
//  Created by S s Vali on 11/2/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class CradsListTableViewCell: UITableViewCell {

    
   
    
    @IBOutlet weak var cardTypeLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
   
    @IBOutlet weak var cardAddDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
