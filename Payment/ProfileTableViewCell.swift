//
//  ProfileTableViewCell.swift
//  RideHubProfile
//
//  Created by S s Vali on 11/15/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var carNameLbl: UILabel!
    
    @IBOutlet weak var carBrandLbl: UILabel!
    
    @IBOutlet weak var carNumberLbl: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var backView: UIView!
    
    @IBOutlet weak var profileNameLabel: UILabel!
   
    @IBOutlet weak var driverRateView: RatingController!
    
    @IBOutlet weak var rideTotalLbl: UILabel!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.carNameLbl.layer.cornerRadius = (carNameLbl.frame.size.height)/2
        carNameLbl.layer.masksToBounds = true
        
        self.profileImageView.layer.cornerRadius = (profileImageView.frame.size.height)/2
                                                              
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
