//
//  MakePaymentVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 25/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class MakePaymentVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
   
    @IBOutlet weak var dotLine2: UIView!
    @IBOutlet weak var makePaymentBtn: UIButton!
    @IBAction func closeAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var dotLine: UIView!
    
    @IBOutlet weak var makePaymentTableView: UITableView!
    var urlSer = URLservices.sharedInstance
    let themes = Themes()
    var walletAmount = Int()
    @IBAction func addPaymentAction(_ sender: Any) {
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "AddFundsVC") as! AddFundsVC
        
        self.navigationController?.pushViewController(Vc, animated: true)
        
    }
    var savedCardsArray = Array<AnyObject>()
    
    
    var urlService = URLservices.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()
        makePaymentTableView.layer.masksToBounds = true
        makePaymentTableView.delegate = self
        makePaymentTableView.dataSource = self
        makePaymentTableView.separatorColor = .none
        
        makePaymentBtn.layer.cornerRadius = makePaymentBtn.frame.size.height/2
        makePaymentBtn.layer.masksToBounds = false
        makePaymentBtn.layer.shadowColor = UIColor.lightGray.cgColor
        makePaymentBtn.layer.shadowOpacity = 0.5
        makePaymentBtn.layer.shadowOffset = CGSize(width: 2, height: 2)
        makePaymentBtn.layer.shadowRadius = 1
        
        self.makePaymentTableView.register(UINib(nibName: "HeaderWalletTableCell", bundle: nil), forCellReuseIdentifier: "HeaderWalletTableCell")
        self.makePaymentTableView.register(UINib(nibName: "PaymentCardTableCell", bundle: nil), forCellReuseIdentifier: "PaymentCardTableCell")

        drawDottedLine(start: CGPoint(x: dotLine.bounds.minX, y: dotLine.bounds.maxY), end: CGPoint(x: dotLine.bounds.maxX, y: dotLine.bounds.minY), view: dotLine)
        drawDottedLine(start: CGPoint(x: dotLine2.bounds.minX, y: dotLine2.bounds.maxY), end: CGPoint(x: dotLine2.bounds.maxX, y: dotLine2.bounds.minY), view: dotLine2)

        // Do any additional setup after loading the view.
        
        walletAmountApi()
         getSavedCardsDetails()
        
        
    }
    
    @IBAction func makePaymentAction(_ sender: Any) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "HeaderWalletTableCell") as!
        HeaderWalletTableCell
        
        header.addWalletBtn.addTarget(self, action: #selector(addMoneyBtnClicked(button:)), for: .touchUpInside)
        header.walletLabel.text = "$ " + String(walletAmount)
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.savedCardsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCardTableCell", for: indexPath) as! PaymentCardTableCell
        cell.selectionStyle = .none
        let obj =  self.savedCardsArray[indexPath.row]
        cell.cardNumberLbl.text = (obj["card_number"] as! String)
        let type = obj["credit_card_type"] as! String
        if(type == "Visa"){
            cell.cardImgView.image = UIImage(named: "visaImage")
        }else if(type == "Discover"){
              cell.cardImgView.image = UIImage(named: "Discover")
        }else{
              cell.cardImgView.image = UIImage(named: "masterCradImage")
        }
//        cell.cardImgView.clipsToBounds = true
//        cell.imageView?.layer.masksToBounds = true
        let expireMonth = (obj["exp_month"] as! String)
         let expireYear = (obj["exp_year"] as! String)
        cell.cardExpireLbl.text = "Expires " + expireMonth + " / " + expireYear
        return cell
    }
    func walletAmountApi()
    {
        let networkRechability =  urlSer.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            
            let parameters = ["user_id" : themes.getUserId()] as [String:Any]
            urlSer.serviceCallPostMethodWithParams(url: getWalletMoney, params: parameters as Dictionary<String, AnyObject>) { response in
                print(response)
                let success = response["status"] as! String
                if(success == "1"){
                    let response = response["response"] as AnyObject
                    
                    self.walletAmount = response["current_balance"] as! Int
                    self.makePaymentTableView.reloadData()
                }
                else{
                    let result = response["msg"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
                print(response)
                
                
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 164
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    @objc func addMoneyBtnClicked(button : UIButton){
        
        let Vc = self.storyboard?.instantiateViewController(withIdentifier: "AddPaymentVC") as! AddPaymentVC
        
        self.navigationController?.pushViewController(Vc, animated: true)
    }
    
    func drawDottedLine(start p0: CGPoint, end p1: CGPoint, view: UIView) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = UIColor(red: 6/255, green: 152/255, blue: 212/255, alpha: 1.0).cgColor
        shapeLayer.lineWidth = 1
        
        shapeLayer.lineDashPattern = [6, 4] // 7 is the length of dash, 3 is length of the gap.
        
        let path = CGMutablePath()
        path.addLines(between: [p0, p1])
        shapeLayer.path = path
        view.layer.addSublayer(shapeLayer)
    }
    func getSavedCardsDetails(){
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId()] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:savedCards, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                    self.themes.hideActivityIndicator(uiView: self.view)
                    let success = response["status"] as! String
                    print(response)
                    if(success == "1") {
                        let some =  response["response"] as! [String : AnyObject]
                        self.savedCardsArray = some["cardlist"] as! Array
                        self.makePaymentTableView.reloadData()
                    }
                    else{
                        let result = response["response"] as! String
                        self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                    }
                }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
}
