//
//  SubToatalTableViewCell.swift
//  RideHubProfile
//
//  Created by S s Vali on 11/15/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class SubToatalTableViewCell: UITableViewCell {

    
    @IBOutlet weak var subTotalLabel: UILabel!    
   
    @IBOutlet weak var makePaymentButton: UIButton!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    
      
        self.makePaymentButton.layer.cornerRadius = 20
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
