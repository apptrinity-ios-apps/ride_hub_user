//
//  CancelRideVC.swift
//  RideHubUser-Swift
//
//  Created by INDOBYTES on 05/03/19.
//  Copyright © 2019 Indobytes. All rights reserved.
//

import UIKit
class CancelRideVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var reasonArray = Array<AnyObject>()
    let urlservices = URLservices()
    let themes = Themes()
    var selectedID = String()
    var rideID = String()
    var selectedReason = String()
    @IBOutlet weak var cancelTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        getCancelReasonAPI()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reasonArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let obj = reasonArray[indexPath.row]
        cell.textLabel?.text = obj["reason"] as? String
        let id = obj["id"] as? String
        if(selectedID == id){
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = reasonArray[indexPath.row]
        selectedReason = obj["reason"] as! String
        selectedID = obj["id"] as! String
        submitReasonAPI()
      //  cancelTable.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
   
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
   
    func getCancelReasonAPI() {
        let networkRechability = urlservices.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId()] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlservices.serviceCallPostMethodWithParams(url:cancelReason, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                print(success)
                if(success == "1") {
                   //let result = response["response"] as! String
                    print(response)
                     let some =  response["response"] as! [String : AnyObject]
                    self.reasonArray = some["reason"] as! Array<AnyObject>
                    self.cancelTable.reloadData()
                   // let id = response["id"] as! String
                    
                   
                }
                else{
                    
                    let result = response["message"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }

    func submitReasonAPI() {
        
        let networkRechability = urlservices.connectedToNetwork()
        if(networkRechability){
            themes.showActivityIndicator(uiView: self.view)
            let parameters = ["user_id" : themes.getUserId(),"ride_id" : rideID,"reason":selectedID] as [String:Any]
            //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            print(" parameters is \(parameters)")
            urlservices.serviceCallPostMethodWithParams(url:rideCanel, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                print(success)
                if(success == "1") {
                    //let result = response["response"] as! String
                    print(response)
                    let some =  response["response"] as! [String : AnyObject]
                     let message =  some["message"] as! String
                    self.themes.showAlert(title: "Success", message: message, sender: self)
                   // UIApplication.shared.keyWindow!.rootViewController as! DashBoardViewController
                    
                    cabArrivedHint = false
                    requestPaymentHint = false
                    ratingHint = false
                    let ratingVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                    self.navigationController?.pushViewController(ratingVC, animated: true)

                }
                else{
                    
                    let result = response["message"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
