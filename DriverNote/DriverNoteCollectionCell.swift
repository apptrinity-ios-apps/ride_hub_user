//
//  DriverNoteCollectionCell.swift
//  NewRideHub
//
//  Created by INDOBYTES on 22/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class DriverNoteCollectionCell: UICollectionViewCell {
    
    
    @IBOutlet weak var noteLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        noteLabel.layer.cornerRadius = noteLabel.frame.height/2
        noteLabel.layer.masksToBounds = true
        // Initialization code
    }

}
