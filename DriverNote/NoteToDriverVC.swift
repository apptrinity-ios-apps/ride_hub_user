//
//  NoteToDriverVC.swift
//  NewRideHub
//
//  Created by INDOBYTES on 22/10/19.
//  Copyright © 2019 INDOBYTES. All rights reserved.
//

import UIKit

class NoteToDriverVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var selectedRow = Int()
    var note = ""
    var themes = Themes()
 
    @IBOutlet weak var noteTextField: UITextField!
    @IBOutlet weak var driverNoteCollectionView: UICollectionView!
    
    @IBOutlet weak var submitBtn: UIButton!
    @IBAction func closeAction(_ sender: Any) {
       dismiss(animated: true, completion: nil)
    }
    
    
    var noteArr = ["GateCode","i'm waiting","i'm at the corner of","i'm infront of","Door number","You'll be picking up","GateCode","i'm waiting","i'm at the corner of","i'm infront of","Door number","You'll be picking up","GateCode","i'm waiting","i'm at the corner of","i'm infront of","Door number","You'll be picking up","GateCode","i'm waiting","i'm at the corner of","i'm infront of","Door number","You'll be picking up","GateCode","i'm waiting","i'm at the corner of","i'm infront of","Door number","You'll be picking up","GateCode","i'm waiting","i'm at the corner of","i'm infront of","Door number","You'll be picking up"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
          submitBtn.layer.cornerRadius = submitBtn.frame.size.height/2
        noteTextField.layer.cornerRadius = noteTextField.frame.size.height/2
        noteTextField.layer.borderWidth = 1
        self.noteTextField.layer.borderColor = UIColor(red: 6/255, green: 152, blue: 212/255, alpha: 1.0).cgColor
        noteTextField.attributedPlaceholder = NSAttributedString(string: "Pick up",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: CGFloat(67/255.0), green: CGFloat(67/255.0), blue: CGFloat(67/255.0), alpha: CGFloat(1.0) )])
        // For Right side Padding
        noteTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: noteTextField.frame.height))
        noteTextField.leftViewMode = .always
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(NoteToDriverVC.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(NoteToDriverVC.cancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        noteTextField.inputAccessoryView = toolBar
        
        driverNoteCollectionView.delegate = self
        driverNoteCollectionView.dataSource = self

        self.driverNoteCollectionView.register(UINib(nibName: "DriverNoteCollectionCell", bundle: nil), forCellWithReuseIdentifier: "DriverNoteCollectionCell")
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func donePicker() {
        noteTextField.resignFirstResponder()  
    }
    
    @objc func cancelPicker() {
        noteTextField.text = ""
        noteTextField.resignFirstResponder()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return noteArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DriverNoteCollectionCell", for: indexPath) as! DriverNoteCollectionCell
        cell.contentView.layer.cornerRadius = cell.contentView.frame.height/2
        cell.noteLabel.layer.backgroundColor = UIColor(red: 205/255, green: 205/255, blue: 205/255, alpha: 1.0).cgColor
        cell.noteLabel.text = noteArr[indexPath.row]

        return cell
        
    }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {        
        note = noteArr[indexPath.row]
        selectedRow = indexPath.row
        noteTextField.text = note
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: self.driverNoteCollectionView.frame.width/2 - 3, height: 55)
        
        let label = UILabel(frame: CGRect.zero)
        label.text = noteArr[indexPath.item]
        label.sizeToFit()
        return CGSize(width: label.frame.width + 12, height: 55)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    
        return 0
    }
    @IBAction func noteSubmitAction(_ sender: Any) {
        
        if(noteTextField.text == ""){
            themes.showAlert(title: "Alert", message: "Please enter your note", sender: self)
        }else{
            driverNote = noteTextField.text!
            dismiss(animated: true, completion: nil)
            
            
        }
        
    }
    
    
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("TextField did begin editing method called")
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("TextField did end editing method called\(textField.text!)")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("TextField should begin editing method called")
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        print("TextField should clear method called")
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("While entering the characters this method gets called")
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    
}
