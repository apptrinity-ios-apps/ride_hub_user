//
//  PopUpViewController.swift
//  RideHub
//
//  Created by INDOBYTES on 23/12/19.
//  Copyright © 2019 indobytes. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController {
    
    var themes = Themes()
    var urlService = URLservices.sharedInstance
    
    @IBOutlet var popUpView: UIView!
    @IBOutlet var headingLBL: UILabel!
    @IBOutlet var SubTextBL: UILabel!
    @IBOutlet var ok_Btn: UIButton!
    
    var id = String()
    var userId = String()
    var headingMesg = ""
    var subMesg = ""
    var fromScreen = ""
    var rideId = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shadowView(view: ok_Btn)
        self.shadowView(view: popUpView)
        
        self.headingLBL.text = headingMesg
        self.SubTextBL.text = subMesg
        
        self.popUpView.layer.cornerRadius = 10
        
        self.ok_Btn.layer.cornerRadius = self.ok_Btn.frame.height / 2
        
        self.showAnimate()

        // Do any additional setup after loading the view.
    }
    
    func shadowView(view:UIView){
        view.layer.cornerRadius = view.frame.size.height/2
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowRadius = 3
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.gray.cgColor
    }
        
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        // self.view.alpha = 3.0;
        UIView.animate(withDuration: 0.25, animations: {
            //   self.view.alpha = 1.8
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            // self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func CloseAction(_ sender: Any) {
        
        self.removeAnimate()
        
    }
    
    
    @IBAction func OKAction(_ sender: Any) {
        
        self.removeAnimate()
        
        if fromScreen == "Payment"{
            
            let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "RatingVC") as! RatingVC
            rideVc.rideID = rideId
            self.navigationController?.pushViewController(rideVc, animated: true)
            
        } else {
            self.removeAnimate()
        }

    }
    
 
}
