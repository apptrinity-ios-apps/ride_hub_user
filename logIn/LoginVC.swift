//
//  LoginVC.swift
//  RideHubUser-Swift
//
//  Created by nagaraj  kumar on 11/09/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit
import CoreLocation
class LoginVC: UIViewController,UITextFieldDelegate,CLLocationManagerDelegate {

    var themes = Themes()
    var urlService = URLservices.sharedInstance
    var loginrecord = LoginRecord()
    
    
   
    
  @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var signinbtn: UIButton!
    @IBOutlet weak var emailTFT: UITextField!
    
    @IBOutlet weak var passwordTFT: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.userNameView.layer.cornerRadius = self.userNameView.frame.height/2
        self.passwordView.layer.cornerRadius = self.passwordView.frame.height/2
        self.userNameView.alpha = 0.4
        self.passwordView.alpha = 0.4
        self.userNameView.isUserInteractionEnabled = true
        emailTFT.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: emailTFT.frame.height))
        emailTFT.leftViewMode = .always
        //For Right side padding
        emailTFT.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: emailTFT.frame.height))
        emailTFT.rightViewMode = .always
        passwordTFT.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: passwordTFT.frame.height))
        passwordTFT.leftViewMode = .always
        //For Right side padding
        passwordTFT.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 5, height: passwordTFT.frame.height))
        passwordTFT.rightViewMode = .always
        self.passwordTFT.layer.cornerRadius = 17
        //self.passwordTFT.backgroundColor = UIColor.clear
        passwordTFT.attributedPlaceholder = NSAttributedString(string: "Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        self.emailTFT.layer.cornerRadius = 17
        //self.emailTFT.backgroundColor = UIColor.clear
        emailTFT.attributedPlaceholder = NSAttributedString(string: "Email",attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        
        
// fbBtn.layer.cornerRadius = 18;
        signinbtn.layer.cornerRadius = signinbtn.frame.height/2
        signinbtn.layer.borderWidth = 0.5;
        signinbtn.layer.borderColor = UIColor.white.cgColor
 
        emailTFT.delegate = self
        passwordTFT.delegate = self
        
        let appDelagate = UIApplication.shared.delegate as? AppDelegate
        appDelagate?.connectToXmpp()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func signIn() {
        let networkRechability = urlService.connectedToNetwork()
        if(networkRechability){
           themes.showActivityIndicator(uiView: self.view)
          //  3a91055c93d30ab99c27e0ccb6598f7d5934c7c1787e66d39c72cb435a24eb62   // Static
            let token = themes.getDeviceToken()
            let parameters = [ "email":emailTFT.text!, "password":passwordTFT.text!,
                               "deviceToken":token,
                //"deviceToken":"a498f1e1b936c6823de5b7c7b650352da79d30c86935ef6a75ee2f50f854cfc7",
                               "gcm_id":"" ] as [String : Any]
            print("log in parameters is \(parameters)")
            urlService.serviceCallPostMethodWithParams(url:applogin, params: parameters as Dictionary<String, Any>) { response in
                print(response)
                let isDead = self.themes.checkNullValue(response["is_dead"]) as! String
                if(isDead == "Yes"){
                    self.themes.hideActivityIndicator(uiView: self.view)
                    self.themes.showAlert(title: "Message", message: "Your Session has been Logged out...\nKindly Login again", sender: self)
                    let rideVc = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.navigationController?.pushViewController(rideVc, animated: true)
                }else{
                self.themes.hideActivityIndicator(uiView: self.view)
                let success = response["status"] as! String
                if(success == "1"){
                  UserDefaults.standard.set(true, forKey: "checkLogIn")
                    self.loginrecord = LoginRecord()
                    self.loginrecord.categoryStr = response["category"] as! String
                    //self.loginrecord.subCategoryStr = response["subcategory"]as! String
                    self.loginrecord.userID = response["user_id"]as! String
                    self.loginrecord.userImage = response["user_image"]as! String
                    self.loginrecord.userEmail = response["email"]as! String
                    self.loginrecord.userName = response["user_name"]as! String
                    self.loginrecord.mobileNumber = response["phone_number"]as! String
                    self.loginrecord.amountWallet = response["wallet_amount"]as! String
                    self.loginrecord.currency = response["currency"]as! String
                    self.loginrecord.countryCode = (response["country_code"] as? String)!
                    
                    let image = (response["user_image"] as? String)!
                    
                    self.themes.saveuserDP(image)
                    self.themes.savePassword(password: self.passwordTFT.text!)
                    self.themes.saveXmppUserCredentials(response["sec_key"]as? String)
                    self.themes.saveUserID(self.loginrecord.userID)
                    self.themes.saveuserDP(self.loginrecord.userImage)
                    self.themes.saveCategoryString(self.loginrecord.categoryStr)
                    self.themes.saveSubCategoryString(self.loginrecord.subCategoryStr)
                    self.themes.saveuserEmail(self.loginrecord.userEmail)
                    self.themes.saveUserName(self.loginrecord.userName)
                    self.themes.saveMobileNumber(self.loginrecord.mobileNumber)
                    self.themes.saveWallet(self.loginrecord.amountWallet)
                    self.themes.saveCurrency(self.loginrecord.currency)
                    self.themes.saveCountryCode(self.loginrecord.countryCode)
                    
                    let appDelagate = UIApplication.shared.delegate as? AppDelegate
                    appDelagate?.GetAppInfo()
                    
                    if CLLocationManager.locationServicesEnabled() {
                        switch(CLLocationManager.authorizationStatus()) {
                        case .notDetermined, .restricted, .denied:
                            print("No access")
                            
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "LocationServicesVC") as! LocationServicesVC
                            vc.fromHint = 1
                            self.navigationController?.pushViewController(vc, animated: true)
                        case .authorizedAlways, .authorizedWhenInUse:
                            //                    UserDefaults.standard.set("2", forKey: "checkfreeorGratis")
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    } else {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "LocationServicesVC") as! LocationServicesVC
                        vc.fromHint = 1
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
                else{
                    
                    let result = response["message"] as! String
                    self.themes.showAlert(title: "Oops ☹️", message: result, sender: self)
                }
            }
            }
        }else{
            themes.showAlert(title: "Oops ☹️", message: "No Internet", sender: self)
        }
    }
    
    
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
     
        if textField == emailTFT{
          
            if(!(isValidEmail(testStr: emailTFT.text!))){
              
                themes.showAlert(title: "Alert", message: "Please Enter Valid Email", sender: self)
                
                textField.resignFirstResponder()
                //emailTFT.becomeFirstResponder()
            }
            else{
                
                passwordTFT.becomeFirstResponder()
            }
            
            
        }
        else{
            passwordTFT.resignFirstResponder()
        }
        
        
        
        return true
    }
    func isValidEmail(testStr:String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
 
    @IBAction func forgotPasswordAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
        
    }
    
    func ValidatedTextField()-> Bool {
        
        if (emailTFT.text == ""){
         
            self.themes.showAlert(title: "Alert", message: "Email is Mandatory", sender: self)
           // self.emailTFT.becomeFirstResponder()
            return false
            
        }
        else if (passwordTFT.text == "") {
            self.themes.showAlert(title: "Alert", message: "Password is Mandatory", sender: self)
           // self.emailTFT.becomeFirstResponder()
            return false
        }
        
        return true
       
    }
    
    
    
   
    @IBAction func SignInBtnAction(_ sender: Any) {
        
        if (ValidatedTextField()){
            signIn()
        }
        else{
            
            
        }
        
    }
    
   
    
    
    
    @IBAction func signUpBtnAction(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
