//  LoginRecord.swift
//  RideHubUser-Swift
//  Created by nagaraj  kumar on 12/10/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//
import UIKit
class LoginRecord: NSObject {
    var userID = ""
    var userImage = ""
    var categoryStr = ""
    var subCategoryStr = ""
    var userEmail = ""
    var userName = ""
    var mobileNumber = ""
    var amountWallet = ""
    var currency = ""
    var countryCode = ""
    var xmppKey = ""
}
