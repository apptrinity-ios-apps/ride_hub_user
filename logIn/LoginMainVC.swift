//
//  LoginMainVC.swift
//  RideHubUser-Swift
//
//  Created by nagaraj  kumar on 10/09/18.
//  Copyright © 2018 Indobytes. All rights reserved.
//

import UIKit

class LoginMainVC: UIViewController {

    
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    let delegate = UIApplication.shared.delegate as! AppDelegate
    
    var themes = Themes()
    override func viewDidLoad() {
        super.viewDidLoad()
        signInBtn.setTitle("SIGNIN", for: .normal)
         signUpBtn.setTitle("REGISTER", for: .normal)
        
        signInBtn.layer.shadowColor = UIColor.black.cgColor
        signInBtn.layer.shadowOpacity = 0.5;
        signInBtn.layer.shadowRadius = 2;
        signInBtn.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        
        //Register_btn.layer.cornerRadius = 5;
        signUpBtn.layer.shadowColor = UIColor.black.cgColor
        signUpBtn.layer.shadowOpacity = 0.5;
        signUpBtn.layer.shadowRadius = 2;
        signUpBtn.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
         
        
        signInBtn.layer.borderWidth = 1;
        signInBtn.layer.borderColor = UIColor.white.cgColor
        signInBtn.layer.cornerRadius=20;
        
        signUpBtn.layer.borderWidth = 1;
        signUpBtn.layer.borderColor = UIColor.white.cgColor
        signUpBtn.layer.cornerRadius=20;
        
        NotificationCenter.default.addObserver(self,selector: #selector(noInternet),name: NSNotification.Name(rawValue: "NoInternet"),object: nil)
       //  NotificationCenter.default.addObserver(self, selector: #selector(self.noInternet(notification:)), name: Notification.Name("NoInternet"), object: nil)
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
       
        
        
    }
    
    @objc func noInternet(){
        
     themes.showAlert(title: "Oops", message: "NoInternet", sender: self)
        
    }
    

    @IBAction func signInAction(_ sender: Any) {
        
        let LoginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(LoginVC, animated: true)
        
    }
    
    @IBAction func signUpAction(_ sender: Any) {
 
        let LoginVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(LoginVC, animated: true)
        
    }
    
}
